// Code generated for linux/riscv64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -o rmd160.go rmd160.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build linux && riscv64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_RMD160_BLOCK_LENGTH = 64
const m_RMD160_DIGEST_LENGTH = 20

type Tsize_t = uint64

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuint8_t = uint8

type TRMD160_CTX = struct {
	Fstate  [5]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_RMD160(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var _ /* ctx at bp+88 */ TRMD160_CTX
	var _ /* hash_bin_got at bp+20 */ [20]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [20]Tuint8_t
	var _ /* hash_str_got at bp+40 */ [41]uint8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_RMD160_DIGEST_LENGTH))
	x_RMD160Data(tls, data, libc.Xstrlen(tls, data), bp+40)
	x_RMD160Init(tls, bp+88)
	x_RMD160Update(tls, bp+88, data, libc.Xstrlen(tls, data))
	x_RMD160End(tls, bp+88, bp+40)
	x_RMD160Init(tls, bp+88)
	x_RMD160Update(tls, bp+88, data, libc.Xstrlen(tls, data))
	x_RMD160Final(tls, bp+20, bp+88)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_RMD160(tls, __ccgo_ts+22, __ccgo_ts+63)
	_test_RMD160(tls, __ccgo_ts+64, __ccgo_ts+105)
	_test_RMD160(tls, __ccgo_ts+109, __ccgo_ts+150)
	return 0
}

func main() {
	libc.Start(x_main)
}

/* rotate x left n bits.  */

var _PADDING2 = [64]Tuint8_t{
	0: uint8(0x80),
}

func x_RMD160Init(tls *libc.TLS, ctx uintptr) {
	(*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xEFCDAB89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98BADCFE)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 4*4)) = uint32(0xC3D2E1F0)
}

func x_RMD160Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need, off Tsize_t
	_, _, _ = have, need, off
	have = (*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount / uint64(8) % uint64(m_RMD160_BLOCK_LENGTH)
	need = uint64(m_RMD160_BLOCK_LENGTH) - have
	*(*Tuint64_t)(unsafe.Pointer(ctx + 24)) += uint64(8) * len1
	off = uint64(0)
	if len1 >= need {
		if have != 0 {
			libc.Xmemcpy(tls, ctx+32+uintptr(have), input, need)
			x_RMD160Transform(tls, ctx, ctx+32)
			off = need
			have = uint64(0)
		}
		/* now the buffer is empty */
		for off+uint64(m_RMD160_BLOCK_LENGTH) <= len1 {
			x_RMD160Transform(tls, ctx, input+uintptr(off))
			off += uint64(m_RMD160_BLOCK_LENGTH)
		}
	}
	if off < len1 {
		libc.Xmemcpy(tls, ctx+32+uintptr(have), input+uintptr(off), len1-off)
	}
}

func x_RMD160Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* size at bp+0 */ [8]Tuint8_t
	_ = padlen
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount)
	/*
	 * pad to RMD160_BLOCK_LENGTH byte blocks, at least one byte from
	 * PADDING plus 8 bytes for the size
	 */
	padlen = uint64(m_RMD160_BLOCK_LENGTH) - (*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount/uint64(8)%uint64(m_RMD160_BLOCK_LENGTH)
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_RMD160_BLOCK_LENGTH)
	}
	x_RMD160Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING2)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	x_RMD160Update(tls, ctx, bp, uint64(8))
}

func x_RMD160Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	x_RMD160Pad(tls, ctx)
	i = 0
	for {
		if !(i < int32(5)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.Xmemset(tls, ctx, 0, uint64(96))
}

func x_RMD160Transform(tls *libc.TLS, state uintptr, block uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, aa, b, bb, c, cc, d, dd, e, ee, t Tuint32_t
	var _ /* x at bp+0 */ [16]Tuint32_t
	_, _, _, _, _, _, _, _, _, _, _ = a, aa, b, bb, c, cc, d, dd, e, ee, t
	libc.Xmemcpy(tls, bp, block, uint64(m_RMD160_BLOCK_LENGTH))
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* Round 1 */
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)<<int32(11) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)<<int32(14) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)<<int32(15) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)<<int32(12) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)<<int32(5) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)<<int32(8) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)<<int32(7) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)<<int32(9) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)<<int32(11) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)<<int32(13) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)<<int32(14) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)<<int32(15) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)<<int32(6) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)<<int32(7) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)<<int32(9) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)<<int32(8) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #15 */
	/* Round 2 */
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5A827999))<<int32(7) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5A827999))<<int32(6) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5A827999))<<int32(8) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5A827999))<<int32(13) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5A827999))<<int32(11) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5A827999))<<int32(9) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5A827999))<<int32(7) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5A827999))<<int32(15) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5A827999))<<int32(7) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5A827999))<<int32(12) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5A827999))<<int32(15) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5A827999))<<int32(9) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5A827999))<<int32(11) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5A827999))<<int32(7) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5A827999))<<int32(13) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5A827999))<<int32(12) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #31 */
	/* Round 3 */
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6ED9EBA1))<<int32(11) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6ED9EBA1))<<int32(13) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6ED9EBA1))<<int32(6) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6ED9EBA1))<<int32(7) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6ED9EBA1))<<int32(14) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6ED9EBA1))<<int32(9) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6ED9EBA1))<<int32(13) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6ED9EBA1))<<int32(15) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6ED9EBA1))<<int32(14) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6ED9EBA1))<<int32(8) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6ED9EBA1))<<int32(13) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6ED9EBA1))<<int32(6) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6ED9EBA1))<<int32(5) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6ED9EBA1))<<int32(12) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6ED9EBA1))<<int32(7) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6ED9EBA1))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #47 */
	/* Round 4 */
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x8F1BBCDC))<<int32(11) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x8F1BBCDC))<<int32(12) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x8F1BBCDC))<<int32(14) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x8F1BBCDC))<<int32(15) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x8F1BBCDC))<<int32(14) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x8F1BBCDC))<<int32(15) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x8F1BBCDC))<<int32(9) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x8F1BBCDC))<<int32(8) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x8F1BBCDC))<<int32(9) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x8F1BBCDC))<<int32(14) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x8F1BBCDC))<<int32(5) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x8F1BBCDC))<<int32(6) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x8F1BBCDC))<<int32(8) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x8F1BBCDC))<<int32(6) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x8F1BBCDC))<<int32(5) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x8F1BBCDC))<<int32(12) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #63 */
	/* Round 5 */
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0xA953FD4E))<<int32(9) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0xA953FD4E))<<int32(15) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0xA953FD4E))<<int32(5) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0xA953FD4E))<<int32(11) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0xA953FD4E))<<int32(6) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0xA953FD4E))<<int32(8) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0xA953FD4E))<<int32(13) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0xA953FD4E))<<int32(12) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0xA953FD4E))<<int32(5) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0xA953FD4E))<<int32(12) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0xA953FD4E))<<int32(13) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0xA953FD4E))<<int32(14) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0xA953FD4E))<<int32(11) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0xA953FD4E))<<int32(8) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0xA953FD4E))<<int32(5) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0xA953FD4E))<<int32(6) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #79 */
	aa = a
	bb = b
	cc = c
	dd = d
	ee = e
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* Parallel round 1 */
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x50A28BE6))<<int32(8) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x50A28BE6))<<int32(9) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x50A28BE6))<<int32(9) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x50A28BE6))<<int32(11) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x50A28BE6))<<int32(13) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x50A28BE6))<<int32(15) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x50A28BE6))<<int32(15) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x50A28BE6))<<int32(5) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x50A28BE6))<<int32(7) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x50A28BE6))<<int32(7) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x50A28BE6))<<int32(8) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x50A28BE6))<<int32(11) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x50A28BE6))<<int32(14) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x50A28BE6))<<int32(14) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x50A28BE6))<<int32(12) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x50A28BE6))<<int32(6) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #15 */
	/* Parallel round 2 */
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5C4DD124))<<int32(9) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5C4DD124))<<int32(13) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5C4DD124))<<int32(15) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5C4DD124))<<int32(7) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5C4DD124))<<int32(12) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5C4DD124))<<int32(8) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5C4DD124))<<int32(9) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5C4DD124))<<int32(11) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5C4DD124))<<int32(7) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5C4DD124))<<int32(7) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5C4DD124))<<int32(12) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5C4DD124))<<int32(7) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5C4DD124))<<int32(6) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5C4DD124))<<int32(15) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5C4DD124))<<int32(13) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5C4DD124))<<int32(11) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #31 */
	/* Parallel round 3 */
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6D703EF3))<<int32(9) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6D703EF3))<<int32(7) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6D703EF3))<<int32(15) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6D703EF3))<<int32(11) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6D703EF3))<<int32(8) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6D703EF3))<<int32(6) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6D703EF3))<<int32(6) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6D703EF3))<<int32(14) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6D703EF3))<<int32(12) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6D703EF3))<<int32(13) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6D703EF3))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6D703EF3))<<int32(14) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6D703EF3))<<int32(13) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6D703EF3))<<int32(13) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6D703EF3))<<int32(7) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6D703EF3))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #47 */
	/* Parallel round 4 */
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x7A6D76E9))<<int32(15) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x7A6D76E9))<<int32(5) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x7A6D76E9))<<int32(8) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x7A6D76E9))<<int32(11) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x7A6D76E9))<<int32(14) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x7A6D76E9))<<int32(14) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x7A6D76E9))<<int32(6) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x7A6D76E9))<<int32(14) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x7A6D76E9))<<int32(6) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x7A6D76E9))<<int32(9) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x7A6D76E9))<<int32(12) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x7A6D76E9))<<int32(9) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x7A6D76E9))<<int32(12) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x7A6D76E9))<<int32(5) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x7A6D76E9))<<int32(15) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x7A6D76E9))<<int32(8) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #63 */
	/* Parallel round 5 */
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)<<int32(8) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)<<int32(5) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)<<int32(12) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)<<int32(9) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)<<int32(12) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)<<int32(5) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)<<int32(14) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)<<int32(6) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)<<int32(8) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)<<int32(13) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)<<int32(6) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)<<int32(5) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)<<int32(15) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)<<int32(13) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)<<int32(11) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)<<int32(11) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #79 */
	t = *(*Tuint32_t)(unsafe.Pointer(state + 1*4)) + cc + d
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 2*4)) + dd + e
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 3*4)) + ee + a
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 4*4)) + aa + b
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) = *(*Tuint32_t)(unsafe.Pointer(state)) + bb + c
	*(*Tuint32_t)(unsafe.Pointer(state)) = t
}

// C documentation
//
//	/* ARGSUSED */
func x_RMD160End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [20]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_RMD160_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_RMD160Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_RMD160_DIGEST_LENGTH)) {
			break
		}
		*(*uint8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex3[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*uint8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex3[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*uint8)(unsafe.Pointer(buf + uintptr(i+i))) = uint8('\000')
	libc.Xmemset(tls, bp, 0, uint64(20))
	return buf
}

var _hex3 = [17]uint8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_RMD160Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TRMD160_CTX
	x_RMD160Init(tls, bp)
	x_RMD160Update(tls, bp, data, len1)
	return x_RMD160End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "ref <%s> != got <%s>\n\x009c1185a5c5e9fc54612808977ee8f548b2258d31\x00\x008eb208f7e05d987a9b044a8e98c6b087f15a0bfc\x00abc\x00e9cbd2ea8015a084ce9cf83a3c65b51f8fa10a39\x0012345\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
