// Code generated for linux/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -mlong-double-64 -o md2.go md2.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build linux && amd64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_MD2_DIGEST_LENGTH = 16
const m_MD2_DIGEST_STRING_LENGTH = 33

type Tsize_t = uint64

type Tuint32_t = uint32

type Tuint8_t = uint8

type TMD2_CTX = struct {
	Fi Tuint32_t
	FC [16]uint8
	FX [48]uint8
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_MD2(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(144)
	defer tls.Free(144)
	var _ /* ctx at bp+68 */ TMD2_CTX
	var _ /* hash_bin_got at bp+16 */ [16]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [16]Tuint8_t
	var _ /* hash_str_got at bp+32 */ [33]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_MD2_DIGEST_LENGTH))
	x_MD2Data(tls, data, libc.Xstrlen(tls, data), bp+32)
	x_MD2Init(tls, bp+68)
	x_MD2Update(tls, bp+68, data, uint32(libc.Xstrlen(tls, data)))
	x_MD2End(tls, bp+68, bp+32)
	x_MD2Init(tls, bp+68)
	x_MD2Update(tls, bp+68, data, uint32(libc.Xstrlen(tls, data)))
	x_MD2Final(tls, bp+16, bp+68)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_MD2(tls, __ccgo_ts, __ccgo_ts+33)
	_test_MD2(tls, __ccgo_ts+34, __ccgo_ts+67)
	_test_MD2(tls, __ccgo_ts+71, __ccgo_ts+104)
	return 0
}

func main() {
	libc.Start(x_main)
}

// C documentation
//
//	/* cut-n-pasted from rfc1319 */
var _S = [256]uint8{
	0:   uint8(41),
	1:   uint8(46),
	2:   uint8(67),
	3:   uint8(201),
	4:   uint8(162),
	5:   uint8(216),
	6:   uint8(124),
	7:   uint8(1),
	8:   uint8(61),
	9:   uint8(54),
	10:  uint8(84),
	11:  uint8(161),
	12:  uint8(236),
	13:  uint8(240),
	14:  uint8(6),
	15:  uint8(19),
	16:  uint8(98),
	17:  uint8(167),
	18:  uint8(5),
	19:  uint8(243),
	20:  uint8(192),
	21:  uint8(199),
	22:  uint8(115),
	23:  uint8(140),
	24:  uint8(152),
	25:  uint8(147),
	26:  uint8(43),
	27:  uint8(217),
	28:  uint8(188),
	29:  uint8(76),
	30:  uint8(130),
	31:  uint8(202),
	32:  uint8(30),
	33:  uint8(155),
	34:  uint8(87),
	35:  uint8(60),
	36:  uint8(253),
	37:  uint8(212),
	38:  uint8(224),
	39:  uint8(22),
	40:  uint8(103),
	41:  uint8(66),
	42:  uint8(111),
	43:  uint8(24),
	44:  uint8(138),
	45:  uint8(23),
	46:  uint8(229),
	47:  uint8(18),
	48:  uint8(190),
	49:  uint8(78),
	50:  uint8(196),
	51:  uint8(214),
	52:  uint8(218),
	53:  uint8(158),
	54:  uint8(222),
	55:  uint8(73),
	56:  uint8(160),
	57:  uint8(251),
	58:  uint8(245),
	59:  uint8(142),
	60:  uint8(187),
	61:  uint8(47),
	62:  uint8(238),
	63:  uint8(122),
	64:  uint8(169),
	65:  uint8(104),
	66:  uint8(121),
	67:  uint8(145),
	68:  uint8(21),
	69:  uint8(178),
	70:  uint8(7),
	71:  uint8(63),
	72:  uint8(148),
	73:  uint8(194),
	74:  uint8(16),
	75:  uint8(137),
	76:  uint8(11),
	77:  uint8(34),
	78:  uint8(95),
	79:  uint8(33),
	80:  uint8(128),
	81:  uint8(127),
	82:  uint8(93),
	83:  uint8(154),
	84:  uint8(90),
	85:  uint8(144),
	86:  uint8(50),
	87:  uint8(39),
	88:  uint8(53),
	89:  uint8(62),
	90:  uint8(204),
	91:  uint8(231),
	92:  uint8(191),
	93:  uint8(247),
	94:  uint8(151),
	95:  uint8(3),
	96:  uint8(255),
	97:  uint8(25),
	98:  uint8(48),
	99:  uint8(179),
	100: uint8(72),
	101: uint8(165),
	102: uint8(181),
	103: uint8(209),
	104: uint8(215),
	105: uint8(94),
	106: uint8(146),
	107: uint8(42),
	108: uint8(172),
	109: uint8(86),
	110: uint8(170),
	111: uint8(198),
	112: uint8(79),
	113: uint8(184),
	114: uint8(56),
	115: uint8(210),
	116: uint8(150),
	117: uint8(164),
	118: uint8(125),
	119: uint8(182),
	120: uint8(118),
	121: uint8(252),
	122: uint8(107),
	123: uint8(226),
	124: uint8(156),
	125: uint8(116),
	126: uint8(4),
	127: uint8(241),
	128: uint8(69),
	129: uint8(157),
	130: uint8(112),
	131: uint8(89),
	132: uint8(100),
	133: uint8(113),
	134: uint8(135),
	135: uint8(32),
	136: uint8(134),
	137: uint8(91),
	138: uint8(207),
	139: uint8(101),
	140: uint8(230),
	141: uint8(45),
	142: uint8(168),
	143: uint8(2),
	144: uint8(27),
	145: uint8(96),
	146: uint8(37),
	147: uint8(173),
	148: uint8(174),
	149: uint8(176),
	150: uint8(185),
	151: uint8(246),
	152: uint8(28),
	153: uint8(70),
	154: uint8(97),
	155: uint8(105),
	156: uint8(52),
	157: uint8(64),
	158: uint8(126),
	159: uint8(15),
	160: uint8(85),
	161: uint8(71),
	162: uint8(163),
	163: uint8(35),
	164: uint8(221),
	165: uint8(81),
	166: uint8(175),
	167: uint8(58),
	168: uint8(195),
	169: uint8(92),
	170: uint8(249),
	171: uint8(206),
	172: uint8(186),
	173: uint8(197),
	174: uint8(234),
	175: uint8(38),
	176: uint8(44),
	177: uint8(83),
	178: uint8(13),
	179: uint8(110),
	180: uint8(133),
	181: uint8(40),
	182: uint8(132),
	183: uint8(9),
	184: uint8(211),
	185: uint8(223),
	186: uint8(205),
	187: uint8(244),
	188: uint8(65),
	189: uint8(129),
	190: uint8(77),
	191: uint8(82),
	192: uint8(106),
	193: uint8(220),
	194: uint8(55),
	195: uint8(200),
	196: uint8(108),
	197: uint8(193),
	198: uint8(171),
	199: uint8(250),
	200: uint8(36),
	201: uint8(225),
	202: uint8(123),
	203: uint8(8),
	204: uint8(12),
	205: uint8(189),
	206: uint8(177),
	207: uint8(74),
	208: uint8(120),
	209: uint8(136),
	210: uint8(149),
	211: uint8(139),
	212: uint8(227),
	213: uint8(99),
	214: uint8(232),
	215: uint8(109),
	216: uint8(233),
	217: uint8(203),
	218: uint8(213),
	219: uint8(254),
	220: uint8(59),
	222: uint8(29),
	223: uint8(57),
	224: uint8(242),
	225: uint8(239),
	226: uint8(183),
	227: uint8(14),
	228: uint8(102),
	229: uint8(88),
	230: uint8(208),
	231: uint8(228),
	232: uint8(166),
	233: uint8(119),
	234: uint8(114),
	235: uint8(248),
	236: uint8(235),
	237: uint8(117),
	238: uint8(75),
	239: uint8(10),
	240: uint8(49),
	241: uint8(68),
	242: uint8(80),
	243: uint8(180),
	244: uint8(143),
	245: uint8(237),
	246: uint8(31),
	247: uint8(26),
	248: uint8(219),
	249: uint8(153),
	250: uint8(141),
	251: uint8(51),
	252: uint8(159),
	253: uint8(17),
	254: uint8(131),
	255: uint8(20),
}

// C documentation
//
//	/* cut-n-pasted from rfc1319 */
var _pad = [17]uintptr{
	0:  __ccgo_ts + 33,
	1:  __ccgo_ts + 110,
	2:  __ccgo_ts + 112,
	3:  __ccgo_ts + 115,
	4:  __ccgo_ts + 119,
	5:  __ccgo_ts + 124,
	6:  __ccgo_ts + 130,
	7:  __ccgo_ts + 137,
	8:  __ccgo_ts + 145,
	9:  __ccgo_ts + 154,
	10: __ccgo_ts + 164,
	11: __ccgo_ts + 175,
	12: __ccgo_ts + 187,
	13: __ccgo_ts + 200,
	14: __ccgo_ts + 214,
	15: __ccgo_ts + 229,
	16: __ccgo_ts + 245,
}

func x_MD2Init(tls *libc.TLS, context uintptr) {
	(*TMD2_CTX)(unsafe.Pointer(context)).Fi = uint32(16)
	libc.Xmemset(tls, context+4, 0, uint64(16))
	libc.Xmemset(tls, context+20, 0, uint64(48))
}

func x_MD2Update(tls *libc.TLS, context uintptr, input uintptr, inputLen uint32) {
	var idx, piece uint32
	var p2 uintptr
	_, _, _ = idx, piece, p2
	idx = uint32(0)
	for {
		if !(idx < inputLen) {
			break
		}
		piece = uint32(32) - (*TMD2_CTX)(unsafe.Pointer(context)).Fi
		if inputLen-idx < piece {
			piece = inputLen - idx
		}
		libc.Xmemcpy(tls, context+20+uintptr((*TMD2_CTX)(unsafe.Pointer(context)).Fi), input+uintptr(idx), uint64(piece))
		p2 = context
		*(*Tuint32_t)(unsafe.Pointer(p2)) += piece
		if *(*Tuint32_t)(unsafe.Pointer(p2)) == uint32(32) {
			x_MD2Transform(tls, context)
		} /* resets i */
		goto _1
	_1:
		;
		idx += piece
	}
}

func x_MD2Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var padlen uint32
	_ = padlen
	/* padlen should be 1..16 */
	padlen = uint32(32) - (*TMD2_CTX)(unsafe.Pointer(context)).Fi
	/* add padding */
	x_MD2Update(tls, context, _pad[padlen], padlen)
	/* add checksum */
	x_MD2Update(tls, context, context+4, libc.Uint32FromInt64(16))
	/* copy out final digest */
	libc.Xmemcpy(tls, digest, context+20, libc.Uint64FromInt32(16))
	/* reset the context */
	x_MD2Init(tls, context)
}

// C documentation
//
//	/*static*/
func x_MD2Transform(tls *libc.TLS, context uintptr) {
	var j, k, l, t, v4 Tuint32_t
	var v6 uint8
	var p2 uintptr
	_, _, _, _, _, _, _ = j, k, l, t, v4, v6, p2
	/* set block "3" and update "checksum" */
	l = uint32(*(*uint8)(unsafe.Pointer(context + 4 + 15)))
	j = libc.Uint32FromInt32(0)
	for {
		if !(j < uint32(16)) {
			break
		}
		*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(32)+j))) = libc.Uint8FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(j)))) ^ libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(16)+j)))))
		p2 = context + 4 + uintptr(j)
		*(*uint8)(unsafe.Pointer(p2)) = uint8(int32(*(*uint8)(unsafe.Pointer(p2))) ^ libc.Int32FromUint8(_S[uint32(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(16)+j))))^l]))
		l = uint32(*(*uint8)(unsafe.Pointer(p2)))
		goto _1
	_1:
		;
		j++
	}
	/* mangle input block */
	v4 = libc.Uint32FromInt32(0)
	j = v4
	t = v4
	for {
		if !(j < uint32(18)) {
			break
		}
		k = uint32(0)
		for {
			if !(k < uint32(48)) {
				break
			}
			v6 = libc.Uint8FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(k)))) ^ libc.Int32FromUint8(_S[t]))
			*(*uint8)(unsafe.Pointer(context + 20 + uintptr(k))) = v6
			t = uint32(v6)
			goto _5
		_5:
			;
			k++
		}
		goto _3
	_3:
		;
		t = (t + j) % uint32(256)
		j++
	}
	/* reset input pointer */
	(*TMD2_CTX)(unsafe.Pointer(context)).Fi = uint32(16)
}

// C documentation
//
//	/* ARGSUSED */
func x_MD2End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, uint64(m_MD2_DIGEST_STRING_LENGTH))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_MD2Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD2_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.Xmemset(tls, bp, 0, uint64(16))
	return buf
}

var _hex = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_MD2Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var _ /* ctx at bp+0 */ TMD2_CTX
	x_MD2Init(tls, bp)
	x_MD2Update(tls, bp, data, uint32(len1))
	return x_MD2End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "8350e5a3e24c153df2275c9f80692773\x00\x00da853b0d3f88d99b30283a69e6ded6bb\x00abc\x002fe3cb9e21922819e79a2781af74e36d\x0012345\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
