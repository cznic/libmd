// Code generated for linux/s390x by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -mlong-double-64 -o md4.go md4.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build linux && s390x

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_MD4_BLOCK_LENGTH = 64
const m_MD4_DIGEST_LENGTH = 16

type Tsize_t = uint64

type Tuint32_t = uint32

type Tuint64_t = uint64

type Tuint8_t = uint8

type TMD4_CTX = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_MD4(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var _ /* ctx at bp+72 */ TMD4_CTX
	var _ /* hash_bin_got at bp+16 */ [16]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [16]Tuint8_t
	var _ /* hash_str_got at bp+32 */ [33]uint8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_MD4_DIGEST_LENGTH))
	x_MD4Data(tls, data, libc.Xstrlen(tls, data), bp+32)
	x_MD4Init(tls, bp+72)
	x_MD4Update(tls, bp+72, data, libc.Xstrlen(tls, data))
	x_MD4End(tls, bp+72, bp+32)
	x_MD4Init(tls, bp+72)
	x_MD4Update(tls, bp+72, data, libc.Xstrlen(tls, data))
	x_MD4Final(tls, bp+16, bp+72)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_MD4(tls, __ccgo_ts, __ccgo_ts+33)
	_test_MD4(tls, __ccgo_ts+34, __ccgo_ts+67)
	_test_MD4(tls, __ccgo_ts+71, __ccgo_ts+104)
	return 0
}

func main() {
	libc.Start(x_main)
}

var _PADDING = [64]Tuint8_t{
	0: uint8(0x80),
}

// C documentation
//
//	/*
//	 * Start MD4 accumulation.
//	 * Set bit count to 0 and buffer to mysterious initialization constants.
//	 */
func x_MD4Init(tls *libc.TLS, ctx uintptr) {
	(*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xefcdab89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98badcfe)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
}

// C documentation
//
//	/*
//	 * Update context to reflect the concatenation of another buffer full
//	 * of bytes.
//	 */
func x_MD4Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need Tsize_t
	_, _ = have, need
	/* Check how many bytes we already have and how many more we need. */
	have = (*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	need = uint64(m_MD4_BLOCK_LENGTH) - have
	/* Update bitcount */
	*(*Tuint64_t)(unsafe.Pointer(ctx + 16)) += len1 << int32(3)
	if len1 >= need {
		if have != uint64(0) {
			libc.Xmemcpy(tls, ctx+24+uintptr(have), input, need)
			x_MD4Transform(tls, ctx, ctx+24)
			input += uintptr(need)
			len1 -= need
			have = uint64(0)
		}
		/* Process data in MD4_BLOCK_LENGTH-byte chunks. */
		for len1 >= uint64(m_MD4_BLOCK_LENGTH) {
			x_MD4Transform(tls, ctx, input)
			input += uintptr(m_MD4_BLOCK_LENGTH)
			len1 -= uint64(m_MD4_BLOCK_LENGTH)
		}
	}
	/* Handle any remaining bytes of data. */
	if len1 != uint64(0) {
		libc.Xmemcpy(tls, ctx+24+uintptr(have), input, len1)
	}
}

// C documentation
//
//	/*
//	 * Pad pad to 64-byte boundary with the bit pattern
//	 * 1 0* (64-bit count of bits processed, MSB-first)
//	 */
func x_MD4Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* count at bp+0 */ [8]Tuint8_t
	_ = padlen
	/* Convert count to 8 bytes in little endian order. */
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount)
	/* Pad out to 56 mod 64. */
	padlen = uint64(m_MD4_BLOCK_LENGTH) - (*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount>>libc.Int32FromInt32(3)&libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_MD4_BLOCK_LENGTH)
	}
	x_MD4Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	x_MD4Update(tls, ctx, bp, uint64(8))
}

// C documentation
//
//	/*
//	 * Final wrapup--call MD4Pad, fill in digest and zero out ctx.
//	 */
func x_MD4Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	x_MD4Pad(tls, ctx)
	if digest != libc.UintptrFromInt32(0) {
		i = 0
		for {
			if !(i < int32(4)) {
				break
			}
			*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
			*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
			*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
			*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
			goto _1
		_1:
			;
			i++
		}
		libc.Xmemset(tls, ctx, 0, uint64(88))
	}
}

/* The three core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */

/* This is the central step in the MD4 algorithm. */

// C documentation
//
//	/*
//	 * The core of the MD4 algorithm, this alters an existing MD4 hash to
//	 * reflect the addition of 16 longwords of new data.  MD4Update blocks
//	 * the data and converts bytes into longwords for this routine.
//	 */
func x_MD4Transform(tls *libc.TLS, state uintptr, block uintptr) {
	var a, b, c, d Tuint32_t
	var in [16]Tuint32_t
	_, _, _, _, _ = a, b, c, d, in
	a = uint32(0)
	for {
		if !(a < libc.Uint32FromInt32(libc.Int32FromInt32(m_MD4_BLOCK_LENGTH)/libc.Int32FromInt32(4))) {
			break
		}
		in[a] = uint32(*(*Tuint8_t)(unsafe.Pointer(block + uintptr(a*uint32(4)+uint32(0))))) | uint32(*(*Tuint8_t)(unsafe.Pointer(block + uintptr(a*uint32(4)+uint32(1)))))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(block + uintptr(a*uint32(4)+uint32(2)))))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(block + uintptr(a*uint32(4)+uint32(3)))))<<libc.Int32FromInt32(24)
		goto _1
	_1:
		;
		a++
	}
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	a += d ^ b&(c^d) + in[0]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + in[int32(1)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + in[int32(2)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + in[int32(3)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + in[int32(4)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + in[int32(5)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + in[int32(6)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + in[int32(7)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + in[int32(8)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + in[int32(9)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + in[int32(10)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + in[int32(11)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + in[int32(12)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + in[int32(13)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + in[int32(14)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + in[int32(15)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += b&c | b&d | c&d + in[0] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + in[int32(4)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + in[int32(8)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + in[int32(12)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + in[int32(1)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + in[int32(5)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + in[int32(9)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + in[int32(13)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + in[int32(2)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + in[int32(6)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + in[int32(10)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + in[int32(14)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + in[int32(3)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + in[int32(7)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + in[int32(11)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + in[int32(15)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b ^ c ^ d + in[0] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + in[int32(8)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + in[int32(4)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + in[int32(12)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + in[int32(2)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + in[int32(10)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + in[int32(6)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + in[int32(14)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + in[int32(1)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + in[int32(9)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + in[int32(5)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + in[int32(13)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + in[int32(3)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + in[int32(11)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + in[int32(7)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + in[int32(15)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
}

// C documentation
//
//	/* ARGSUSED */
func x_MD4End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_MD4Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD4_DIGEST_LENGTH)) {
			break
		}
		*(*uint8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex1[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*uint8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex1[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*uint8)(unsafe.Pointer(buf + uintptr(i+i))) = uint8('\000')
	libc.Xmemset(tls, bp, 0, uint64(16))
	return buf
}

var _hex1 = [17]uint8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_MD4Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TMD4_CTX
	x_MD4Init(tls, bp)
	x_MD4Update(tls, bp, data, len1)
	return x_MD4End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "31d6cfe0d16ae931b73c59d7e0c089c0\x00\x00a448017aaf21d8525fc10ae87aa6729d\x00abc\x0023580e2a459f7ea40f9efa148b63cafb\x0012345\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
