// Code generated for darwin/amd64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -mlong-double-64 -o sha2.go sha2.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build darwin && amd64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_SHA224_DIGEST_LENGTH = 28
const m_SHA256_BLOCK_LENGTH = 64
const m_SHA256_DIGEST_LENGTH = 32
const m_SHA384_DIGEST_LENGTH = 48
const m_SHA512_256_DIGEST_LENGTH = 32
const m_SHA512_BLOCK_LENGTH = 128
const m_SHA512_DIGEST_LENGTH = 64

type T__predefined_size_t = uint64

type Tsize_t = uint64

type Tuint64_t = uint64

type Tuint32_t = uint32

type Tuint8_t = uint8

type TSHA2_CTX = struct {
	Fstate struct {
		Fst64        [0][8]Tuint64_t
		Fst32        [8]Tuint32_t
		F__ccgo_pad2 [32]byte
	}
	Fbitcount [2]Tuint64_t
	Fbuffer   [128]Tuint8_t
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_SHA224(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(336)
	defer tls.Free(336)
	var _ /* ctx at bp+120 */ TSHA2_CTX
	var _ /* hash_bin_got at bp+28 */ [28]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [28]Tuint8_t
	var _ /* hash_str_got at bp+56 */ [57]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA224_DIGEST_LENGTH))
	x_SHA224Data(tls, data, libc.Xstrlen(tls, data), bp+56)
	x_SHA224Init(tls, bp+120)
	x_SHA256Update(tls, bp+120, data, libc.Xstrlen(tls, data))
	x_SHA224End(tls, bp+120, bp+56)
	x_SHA224Init(tls, bp+120)
	x_SHA256Update(tls, bp+120, data, libc.Xstrlen(tls, data))
	x_SHA224Final(tls, bp+28, bp+120)
}

func _test_SHA256(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(352)
	defer tls.Free(352)
	var _ /* ctx at bp+136 */ TSHA2_CTX
	var _ /* hash_bin_got at bp+32 */ [32]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [32]Tuint8_t
	var _ /* hash_str_got at bp+64 */ [65]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA256_DIGEST_LENGTH))
	x_SHA256Data(tls, data, libc.Xstrlen(tls, data), bp+64)
	x_SHA256Init(tls, bp+136)
	x_SHA256Update(tls, bp+136, data, libc.Xstrlen(tls, data))
	x_SHA256End(tls, bp+136, bp+64)
	x_SHA256Init(tls, bp+136)
	x_SHA256Update(tls, bp+136, data, libc.Xstrlen(tls, data))
	x_SHA256Final(tls, bp+32, bp+136)
}

func _test_SHA384(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(416)
	defer tls.Free(416)
	var _ /* ctx at bp+200 */ TSHA2_CTX
	var _ /* hash_bin_got at bp+48 */ [48]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [48]Tuint8_t
	var _ /* hash_str_got at bp+96 */ [97]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA384_DIGEST_LENGTH))
	x_SHA384Data(tls, data, libc.Xstrlen(tls, data), bp+96)
	x_SHA384Init(tls, bp+200)
	x_SHA512Update(tls, bp+200, data, libc.Xstrlen(tls, data))
	x_SHA384End(tls, bp+200, bp+96)
	x_SHA384Init(tls, bp+200)
	x_SHA512Update(tls, bp+200, data, libc.Xstrlen(tls, data))
	x_SHA384Final(tls, bp+48, bp+200)
}

func _test_SHA512(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(480)
	defer tls.Free(480)
	var _ /* ctx at bp+264 */ TSHA2_CTX
	var _ /* hash_bin_got at bp+64 */ [64]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [64]Tuint8_t
	var _ /* hash_str_got at bp+128 */ [129]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA512_DIGEST_LENGTH))
	x_SHA512Data(tls, data, libc.Xstrlen(tls, data), bp+128)
	x_SHA512Init(tls, bp+264)
	x_SHA512Update(tls, bp+264, data, libc.Xstrlen(tls, data))
	x_SHA512End(tls, bp+264, bp+128)
	x_SHA512Init(tls, bp+264)
	x_SHA512Update(tls, bp+264, data, libc.Xstrlen(tls, data))
	x_SHA512Final(tls, bp+64, bp+264)
}

func _test_SHA512_256(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(352)
	defer tls.Free(352)
	var _ /* ctx at bp+136 */ TSHA2_CTX
	var _ /* hash_bin_got at bp+32 */ [32]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [32]Tuint8_t
	var _ /* hash_str_got at bp+64 */ [65]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA512_256_DIGEST_LENGTH))
	x_SHA512_256Data(tls, data, libc.Xstrlen(tls, data), bp+64)
	x_SHA512_256Init(tls, bp+136)
	x_SHA512Update(tls, bp+136, data, libc.Xstrlen(tls, data))
	x_SHA512_256End(tls, bp+136, bp+64)
	x_SHA512_256Init(tls, bp+136)
	x_SHA512Update(tls, bp+136, data, libc.Xstrlen(tls, data))
	x_SHA512_256Final(tls, bp+32, bp+136)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_SHA224(tls, __ccgo_ts+22, __ccgo_ts+79)
	_test_SHA224(tls, __ccgo_ts+80, __ccgo_ts+137)
	_test_SHA224(tls, __ccgo_ts+141, __ccgo_ts+198)
	_test_SHA256(tls, __ccgo_ts+204, __ccgo_ts+79)
	_test_SHA256(tls, __ccgo_ts+269, __ccgo_ts+137)
	_test_SHA256(tls, __ccgo_ts+334, __ccgo_ts+198)
	_test_SHA384(tls, __ccgo_ts+399, __ccgo_ts+79)
	_test_SHA384(tls, __ccgo_ts+496, __ccgo_ts+137)
	_test_SHA384(tls, __ccgo_ts+593, __ccgo_ts+198)
	_test_SHA512(tls, __ccgo_ts+690, __ccgo_ts+79)
	_test_SHA512(tls, __ccgo_ts+819, __ccgo_ts+137)
	_test_SHA512(tls, __ccgo_ts+948, __ccgo_ts+198)
	_test_SHA512_256(tls, __ccgo_ts+1077, __ccgo_ts+79)
	_test_SHA512_256(tls, __ccgo_ts+1142, __ccgo_ts+137)
	_test_SHA512_256(tls, __ccgo_ts+1207, __ccgo_ts+198)
	return 0
}

func main() {
	libc.Start(x_main)
}

/*
 * UNROLLED TRANSFORM LOOP NOTE:
 * You can define SHA2_UNROLL_TRANSFORM to use the unrolled transform
 * loop version for the hash transform rounds (defined using macros
 * later in this file).  Either define on the command line, for example:
 *
 *   cc -DSHA2_UNROLL_TRANSFORM -o sha2 sha2.c sha2prog.c
 *
 * or define below:
 *
 *   #define SHA2_UNROLL_TRANSFORM
 *
 */

/*** SHA-224/256/384/512 Various Length Definitions ***********************/
/* NOTE: Most of these are in sha2.h */

/*** ENDIAN SPECIFIC COPY MACROS **************************************/

/*
 * Macro for incrementally adding the unsigned 64-bit integer n to the
 * unsigned 128-bit integer (represented using a two-element array of
 * 64-bit words):
 */

/*** THE SIX LOGICAL FUNCTIONS ****************************************/
/*
 * Bit shifting and rotation (used by the six SHA-XYZ logical functions:
 *
 *   NOTE:  The naming of R and S appears backwards here (R is a SHIFT and
 *   S is a ROTATION) because the SHA-224/256/384/512 description document
 *   (see http://csrc.nist.gov/cryptval/shs/sha256-384-512.pdf) uses this
 *   same "backwards" definition.
 */
/* Shift-right (used in SHA-224, SHA-256, SHA-384, and SHA-512): */
/* 32-bit Rotate-right (used in SHA-224 and SHA-256): */
/* 64-bit Rotate-right (used in SHA-384 and SHA-512): */

/* Two of six logical functions used in SHA-224, SHA-256, SHA-384, and SHA-512: */

/* Four of six logical functions used in SHA-224 and SHA-256: */

/* Four of six logical functions used in SHA-384 and SHA-512: */

// C documentation
//
//	/*** SHA-XYZ INITIAL HASH VALUES AND CONSTANTS ************************/
//	/* Hash constant words K for SHA-224 and SHA-256: */
var _K256 = [64]Tuint32_t{
	0:  uint32(0x428a2f98),
	1:  uint32(0x71374491),
	2:  uint32(0xb5c0fbcf),
	3:  uint32(0xe9b5dba5),
	4:  uint32(0x3956c25b),
	5:  uint32(0x59f111f1),
	6:  uint32(0x923f82a4),
	7:  uint32(0xab1c5ed5),
	8:  uint32(0xd807aa98),
	9:  uint32(0x12835b01),
	10: uint32(0x243185be),
	11: uint32(0x550c7dc3),
	12: uint32(0x72be5d74),
	13: uint32(0x80deb1fe),
	14: uint32(0x9bdc06a7),
	15: uint32(0xc19bf174),
	16: uint32(0xe49b69c1),
	17: uint32(0xefbe4786),
	18: uint32(0x0fc19dc6),
	19: uint32(0x240ca1cc),
	20: uint32(0x2de92c6f),
	21: uint32(0x4a7484aa),
	22: uint32(0x5cb0a9dc),
	23: uint32(0x76f988da),
	24: uint32(0x983e5152),
	25: uint32(0xa831c66d),
	26: uint32(0xb00327c8),
	27: uint32(0xbf597fc7),
	28: uint32(0xc6e00bf3),
	29: uint32(0xd5a79147),
	30: uint32(0x06ca6351),
	31: uint32(0x14292967),
	32: uint32(0x27b70a85),
	33: uint32(0x2e1b2138),
	34: uint32(0x4d2c6dfc),
	35: uint32(0x53380d13),
	36: uint32(0x650a7354),
	37: uint32(0x766a0abb),
	38: uint32(0x81c2c92e),
	39: uint32(0x92722c85),
	40: uint32(0xa2bfe8a1),
	41: uint32(0xa81a664b),
	42: uint32(0xc24b8b70),
	43: uint32(0xc76c51a3),
	44: uint32(0xd192e819),
	45: uint32(0xd6990624),
	46: uint32(0xf40e3585),
	47: uint32(0x106aa070),
	48: uint32(0x19a4c116),
	49: uint32(0x1e376c08),
	50: uint32(0x2748774c),
	51: uint32(0x34b0bcb5),
	52: uint32(0x391c0cb3),
	53: uint32(0x4ed8aa4a),
	54: uint32(0x5b9cca4f),
	55: uint32(0x682e6ff3),
	56: uint32(0x748f82ee),
	57: uint32(0x78a5636f),
	58: uint32(0x84c87814),
	59: uint32(0x8cc70208),
	60: uint32(0x90befffa),
	61: uint32(0xa4506ceb),
	62: uint32(0xbef9a3f7),
	63: uint32(0xc67178f2),
}

// C documentation
//
//	/* Initial hash value H for SHA-256: */
var _sha256_initial_hash_value = [8]Tuint32_t{
	0: uint32(0x6a09e667),
	1: uint32(0xbb67ae85),
	2: uint32(0x3c6ef372),
	3: uint32(0xa54ff53a),
	4: uint32(0x510e527f),
	5: uint32(0x9b05688c),
	6: uint32(0x1f83d9ab),
	7: uint32(0x5be0cd19),
}

// C documentation
//
//	/* Hash constant words K for SHA-384 and SHA-512: */
var _K512 = [80]Tuint64_t{
	0:  uint64(0x428a2f98d728ae22),
	1:  uint64(0x7137449123ef65cd),
	2:  uint64(0xb5c0fbcfec4d3b2f),
	3:  uint64(0xe9b5dba58189dbbc),
	4:  uint64(0x3956c25bf348b538),
	5:  uint64(0x59f111f1b605d019),
	6:  uint64(0x923f82a4af194f9b),
	7:  uint64(0xab1c5ed5da6d8118),
	8:  uint64(0xd807aa98a3030242),
	9:  uint64(0x12835b0145706fbe),
	10: uint64(0x243185be4ee4b28c),
	11: uint64(0x550c7dc3d5ffb4e2),
	12: uint64(0x72be5d74f27b896f),
	13: uint64(0x80deb1fe3b1696b1),
	14: uint64(0x9bdc06a725c71235),
	15: uint64(0xc19bf174cf692694),
	16: uint64(0xe49b69c19ef14ad2),
	17: uint64(0xefbe4786384f25e3),
	18: uint64(0x0fc19dc68b8cd5b5),
	19: uint64(0x240ca1cc77ac9c65),
	20: uint64(0x2de92c6f592b0275),
	21: uint64(0x4a7484aa6ea6e483),
	22: uint64(0x5cb0a9dcbd41fbd4),
	23: uint64(0x76f988da831153b5),
	24: uint64(0x983e5152ee66dfab),
	25: uint64(0xa831c66d2db43210),
	26: uint64(0xb00327c898fb213f),
	27: uint64(0xbf597fc7beef0ee4),
	28: uint64(0xc6e00bf33da88fc2),
	29: uint64(0xd5a79147930aa725),
	30: uint64(0x06ca6351e003826f),
	31: uint64(0x142929670a0e6e70),
	32: uint64(0x27b70a8546d22ffc),
	33: uint64(0x2e1b21385c26c926),
	34: uint64(0x4d2c6dfc5ac42aed),
	35: uint64(0x53380d139d95b3df),
	36: uint64(0x650a73548baf63de),
	37: uint64(0x766a0abb3c77b2a8),
	38: uint64(0x81c2c92e47edaee6),
	39: uint64(0x92722c851482353b),
	40: uint64(0xa2bfe8a14cf10364),
	41: uint64(0xa81a664bbc423001),
	42: uint64(0xc24b8b70d0f89791),
	43: uint64(0xc76c51a30654be30),
	44: uint64(0xd192e819d6ef5218),
	45: uint64(0xd69906245565a910),
	46: uint64(0xf40e35855771202a),
	47: uint64(0x106aa07032bbd1b8),
	48: uint64(0x19a4c116b8d2d0c8),
	49: uint64(0x1e376c085141ab53),
	50: uint64(0x2748774cdf8eeb99),
	51: uint64(0x34b0bcb5e19b48a8),
	52: uint64(0x391c0cb3c5c95a63),
	53: uint64(0x4ed8aa4ae3418acb),
	54: uint64(0x5b9cca4f7763e373),
	55: uint64(0x682e6ff3d6b2b8a3),
	56: uint64(0x748f82ee5defb2fc),
	57: uint64(0x78a5636f43172f60),
	58: uint64(0x84c87814a1f0ab72),
	59: uint64(0x8cc702081a6439ec),
	60: uint64(0x90befffa23631e28),
	61: uint64(0xa4506cebde82bde9),
	62: uint64(0xbef9a3f7b2c67915),
	63: uint64(0xc67178f2e372532b),
	64: uint64(0xca273eceea26619c),
	65: uint64(0xd186b8c721c0c207),
	66: uint64(0xeada7dd6cde0eb1e),
	67: uint64(0xf57d4f7fee6ed178),
	68: uint64(0x06f067aa72176fba),
	69: uint64(0x0a637dc5a2c898a6),
	70: uint64(0x113f9804bef90dae),
	71: uint64(0x1b710b35131c471b),
	72: uint64(0x28db77f523047d84),
	73: uint64(0x32caab7b40c72493),
	74: uint64(0x3c9ebe0a15c9bebc),
	75: uint64(0x431d67c49c100d4c),
	76: uint64(0x4cc5d4becb3e42b6),
	77: uint64(0x597f299cfc657e2a),
	78: uint64(0x5fcb6fab3ad6faec),
	79: uint64(0x6c44198c4a475817),
}

// C documentation
//
//	/* Initial hash value H for SHA-512 */
var _sha512_initial_hash_value = [8]Tuint64_t{
	0: uint64(0x6a09e667f3bcc908),
	1: uint64(0xbb67ae8584caa73b),
	2: uint64(0x3c6ef372fe94f82b),
	3: uint64(0xa54ff53a5f1d36f1),
	4: uint64(0x510e527fade682d1),
	5: uint64(0x9b05688c2b3e6c1f),
	6: uint64(0x1f83d9abfb41bd6b),
	7: uint64(0x5be0cd19137e2179),
}

// C documentation
//
//	/* Initial hash value H for SHA-224: */
var _sha224_initial_hash_value = [8]Tuint32_t{
	0: uint32(0xc1059ed8),
	1: uint32(0x367cd507),
	2: uint32(0x3070dd17),
	3: uint32(0xf70e5939),
	4: uint32(0xffc00b31),
	5: uint32(0x68581511),
	6: uint32(0x64f98fa7),
	7: uint32(0xbefa4fa4),
}

// C documentation
//
//	/* Initial hash value H for SHA-384 */
var _sha384_initial_hash_value = [8]Tuint64_t{
	0: uint64(0xcbbb9d5dc1059ed8),
	1: uint64(0x629a292a367cd507),
	2: uint64(0x9159015a3070dd17),
	3: uint64(0x152fecd8f70e5939),
	4: uint64(0x67332667ffc00b31),
	5: uint64(0x8eb44a8768581511),
	6: uint64(0xdb0c2e0d64f98fa7),
	7: uint64(0x47b5481dbefa4fa4),
}

// C documentation
//
//	/* Initial hash value H for SHA-512-256 */
var _sha512_256_initial_hash_value = [8]Tuint64_t{
	0: uint64(0x22312194fc2bf72c),
	1: uint64(0x9f555fa3c84c64c2),
	2: uint64(0x2393b86b6f53b151),
	3: uint64(0x963877195940eabd),
	4: uint64(0x96283ee2a88effe3),
	5: uint64(0xbe5e1e2553863992),
	6: uint64(0x2b0199fc2c85b8aa),
	7: uint64(0x0eb72ddc81c52ca2),
}

// C documentation
//
//	/*** SHA-224: *********************************************************/
func x_SHA224Init(tls *libc.TLS, context uintptr) {
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha224_initial_hash_value)), uint64(32), ^T__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^T__predefined_size_t(0))
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = uint64(0)
}

func x_SHA224Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	x_SHA256Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(7)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^T__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-256: *********************************************************/
func x_SHA256Init(tls *libc.TLS, context uintptr) {
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha256_initial_hash_value)), uint64(32), ^T__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^T__predefined_size_t(0))
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = uint64(0)
}

/* Unrolled SHA-256 round macros: */

func x_SHA256Transform(tls *libc.TLS, state uintptr, data uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var T1, a, b, c, d, e, f, g, h, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9 Tuint32_t
	var j int32
	var p1, p2, p3, p4, p5, p6, p7, p8 uintptr
	var _ /* W256 at bp+0 */ [16]Tuint32_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = T1, a, b, c, d, e, f, g, h, j, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9, p1, p2, p3, p4, p5, p6, p7, p8
	/* Initialize registers with the prev. intermediate value */
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	f = *(*Tuint32_t)(unsafe.Pointer(state + 5*4))
	g = *(*Tuint32_t)(unsafe.Pointer(state + 6*4))
	h = *(*Tuint32_t)(unsafe.Pointer(state + 7*4))
	j = 0
	for cond := true; cond; cond = j < int32(16) {
		/* Rounds 0 to 15 (unrolled): */
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = h + (e>>libc.Int32FromInt32(6) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (e>>libc.Int32FromInt32(11) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (e>>libc.Int32FromInt32(25) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (e&f ^ ^e&g) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(2) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (a>>libc.Int32FromInt32(13) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (a>>libc.Int32FromInt32(22) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (a&b ^ a&c ^ b&c)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = g + (d>>libc.Int32FromInt32(6) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (d>>libc.Int32FromInt32(11) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (d>>libc.Int32FromInt32(25) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (d&e ^ ^d&f) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(2) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (h>>libc.Int32FromInt32(13) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (h>>libc.Int32FromInt32(22) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (h&a ^ h&b ^ a&b)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = f + (c>>libc.Int32FromInt32(6) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (c>>libc.Int32FromInt32(11) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (c>>libc.Int32FromInt32(25) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (c&d ^ ^c&e) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(2) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (g>>libc.Int32FromInt32(13) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (g>>libc.Int32FromInt32(22) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (g&h ^ g&a ^ h&a)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = e + (b>>libc.Int32FromInt32(6) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (b>>libc.Int32FromInt32(11) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (b>>libc.Int32FromInt32(25) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (b&c ^ ^b&d) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(2) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (f>>libc.Int32FromInt32(13) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (f>>libc.Int32FromInt32(22) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (f&g ^ f&h ^ g&h)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = d + (a>>libc.Int32FromInt32(6) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (a>>libc.Int32FromInt32(11) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (a>>libc.Int32FromInt32(25) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (a&b ^ ^a&c) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(2) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (e>>libc.Int32FromInt32(13) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (e>>libc.Int32FromInt32(22) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (e&f ^ e&g ^ f&g)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = c + (h>>libc.Int32FromInt32(6) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (h>>libc.Int32FromInt32(11) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (h>>libc.Int32FromInt32(25) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (h&a ^ ^h&b) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(2) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (d>>libc.Int32FromInt32(13) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (d>>libc.Int32FromInt32(22) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (d&e ^ d&f ^ e&f)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = b + (g>>libc.Int32FromInt32(6) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (g>>libc.Int32FromInt32(11) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (g>>libc.Int32FromInt32(25) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (g&h ^ ^g&a) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(2) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (c>>libc.Int32FromInt32(13) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (c>>libc.Int32FromInt32(22) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (c&d ^ c&e ^ d&e)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = a + (f>>libc.Int32FromInt32(6) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (f>>libc.Int32FromInt32(11) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (f>>libc.Int32FromInt32(25) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (f&g ^ ^f&h) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(2) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (b>>libc.Int32FromInt32(13) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (b>>libc.Int32FromInt32(22) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Now for the remaining rounds up to 63: */
	for cond := true; cond; cond = j < int32(64) {
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p1 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p1)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = h + (e>>libc.Int32FromInt32(6) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (e>>libc.Int32FromInt32(11) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (e>>libc.Int32FromInt32(25) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (e&f ^ ^e&g) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p1))
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(2) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (a>>libc.Int32FromInt32(13) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (a>>libc.Int32FromInt32(22) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (a&b ^ a&c ^ b&c)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p2 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p2)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = g + (d>>libc.Int32FromInt32(6) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (d>>libc.Int32FromInt32(11) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (d>>libc.Int32FromInt32(25) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (d&e ^ ^d&f) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p2))
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(2) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (h>>libc.Int32FromInt32(13) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (h>>libc.Int32FromInt32(22) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (h&a ^ h&b ^ a&b)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p3 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p3)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = f + (c>>libc.Int32FromInt32(6) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (c>>libc.Int32FromInt32(11) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (c>>libc.Int32FromInt32(25) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (c&d ^ ^c&e) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p3))
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(2) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (g>>libc.Int32FromInt32(13) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (g>>libc.Int32FromInt32(22) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (g&h ^ g&a ^ h&a)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p4 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p4)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = e + (b>>libc.Int32FromInt32(6) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (b>>libc.Int32FromInt32(11) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (b>>libc.Int32FromInt32(25) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (b&c ^ ^b&d) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p4))
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(2) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (f>>libc.Int32FromInt32(13) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (f>>libc.Int32FromInt32(22) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (f&g ^ f&h ^ g&h)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p5 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p5)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = d + (a>>libc.Int32FromInt32(6) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (a>>libc.Int32FromInt32(11) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (a>>libc.Int32FromInt32(25) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (a&b ^ ^a&c) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p5))
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(2) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (e>>libc.Int32FromInt32(13) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (e>>libc.Int32FromInt32(22) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (e&f ^ e&g ^ f&g)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p6 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p6)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = c + (h>>libc.Int32FromInt32(6) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (h>>libc.Int32FromInt32(11) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (h>>libc.Int32FromInt32(25) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (h&a ^ ^h&b) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p6))
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(2) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (d>>libc.Int32FromInt32(13) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (d>>libc.Int32FromInt32(22) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (d&e ^ d&f ^ e&f)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p7 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p7)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = b + (g>>libc.Int32FromInt32(6) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (g>>libc.Int32FromInt32(11) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (g>>libc.Int32FromInt32(25) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (g&h ^ ^g&a) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p7))
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(2) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (c>>libc.Int32FromInt32(13) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (c>>libc.Int32FromInt32(22) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (c&d ^ c&e ^ d&e)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p8 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p8)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = a + (f>>libc.Int32FromInt32(6) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (f>>libc.Int32FromInt32(11) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (f>>libc.Int32FromInt32(25) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (f&g ^ ^f&h) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p8))
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(2) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (b>>libc.Int32FromInt32(13) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (b>>libc.Int32FromInt32(22) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Compute the current intermediate hash value */
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) += e
	*(*Tuint32_t)(unsafe.Pointer(state + 5*4)) += f
	*(*Tuint32_t)(unsafe.Pointer(state + 6*4)) += g
	*(*Tuint32_t)(unsafe.Pointer(state + 7*4)) += h
	/* Clean up */
	v16 = libc.Uint32FromInt32(0)
	T1 = v16
	v15 = v16
	h = v15
	v14 = v15
	g = v14
	v13 = v14
	f = v13
	v12 = v13
	e = v12
	v11 = v12
	d = v11
	v10 = v11
	c = v10
	v9 = v10
	b = v9
	a = v9
}

func x_SHA256Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var freespace, usedspace, v1, v2 Tsize_t
	_, _, _, _ = freespace, usedspace, v1, v2
	/* Calling with no data is valid (we do nothing) */
	if len1 == uint64(0) {
		return
	}
	usedspace = *(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA256_BLOCK_LENGTH)
	if usedspace > uint64(0) {
		/* Calculate how much free space is available in the buffer */
		freespace = uint64(m_SHA256_BLOCK_LENGTH) - usedspace
		if len1 >= freespace {
			/* Fill the buffer completely and process it */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, freespace, ^T__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += freespace << int32(3)
			len1 -= freespace
			data += uintptr(freespace)
			x_SHA256Transform(tls, context, context+80)
		} else {
			/* The buffer is not yet full */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, len1, ^T__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << int32(3)
			/* Clean up: */
			v1 = libc.Uint64FromInt32(0)
			freespace = v1
			usedspace = v1
			return
		}
	}
	for len1 >= uint64(m_SHA256_BLOCK_LENGTH) {
		/* Process as many complete blocks as we can */
		x_SHA256Transform(tls, context, data)
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH) << libc.Int32FromInt32(3))
		len1 -= uint64(m_SHA256_BLOCK_LENGTH)
		data += uintptr(m_SHA256_BLOCK_LENGTH)
	}
	if len1 > uint64(0) {
		/* There's left-overs, so save 'em */
		libc.X__builtin___memcpy_chk(tls, context+80, data, len1, ^T__predefined_size_t(0))
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << int32(3)
	}
	/* Clean up: */
	v2 = libc.Uint64FromInt32(0)
	freespace = v2
	usedspace = v2
}

func x_SHA256Pad(tls *libc.TLS, context uintptr) {
	var usedspace, v1 uint32
	_, _ = usedspace, v1
	usedspace = uint32(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA256_BLOCK_LENGTH))
	if usedspace > uint32(0) {
		/* Begin padding with a 1 bit: */
		v1 = usedspace
		usedspace++
		*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(v1))) = uint8(0x80)
		if usedspace <= libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) {
			/* Set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8))-usedspace), ^T__predefined_size_t(0))
		} else {
			if usedspace < uint32(m_SHA256_BLOCK_LENGTH) {
				libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(uint32(m_SHA256_BLOCK_LENGTH)-usedspace), ^T__predefined_size_t(0))
			}
			/* Do second-to-last transform: */
			x_SHA256Transform(tls, context, context+80)
			/* Prepare for last transform: */
			libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)), ^T__predefined_size_t(0))
		}
	} else {
		/* Set-up for the last transform: */
		libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)), ^T__predefined_size_t(0))
		/* Begin padding with a 1 bit: */
		*(*Tuint8_t)(unsafe.Pointer(context + 80)) = uint8(0x80)
	}
	/* Store the length of input data (in bits) in big endian format: */
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)))
	/* Final transform: */
	x_SHA256Transform(tls, context, context+80)
	/* Clean up: */
	usedspace = uint32(0)
}

func x_SHA256Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	x_SHA256Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(8)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^T__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-512: *********************************************************/
func x_SHA512Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha512_initial_hash_value)), uint64(64), ^T__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^T__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

/* Unrolled SHA-512 round macros: */

func x_SHA512Transform(tls *libc.TLS, state uintptr, data uintptr) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var T1, a, b, c, d, e, f, g, h, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9 Tuint64_t
	var j int32
	var p1, p2, p3, p4, p5, p6, p7, p8 uintptr
	var _ /* W512 at bp+0 */ [16]Tuint64_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = T1, a, b, c, d, e, f, g, h, j, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9, p1, p2, p3, p4, p5, p6, p7, p8
	/* Initialize registers with the prev. intermediate value */
	a = *(*Tuint64_t)(unsafe.Pointer(state))
	b = *(*Tuint64_t)(unsafe.Pointer(state + 1*8))
	c = *(*Tuint64_t)(unsafe.Pointer(state + 2*8))
	d = *(*Tuint64_t)(unsafe.Pointer(state + 3*8))
	e = *(*Tuint64_t)(unsafe.Pointer(state + 4*8))
	f = *(*Tuint64_t)(unsafe.Pointer(state + 5*8))
	g = *(*Tuint64_t)(unsafe.Pointer(state + 6*8))
	h = *(*Tuint64_t)(unsafe.Pointer(state + 7*8))
	j = 0
	for cond := true; cond; cond = j < int32(16) {
		/* Rounds 0 to 15 (unrolled): */
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = h + (e>>libc.Int32FromInt32(14) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (e>>libc.Int32FromInt32(18) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (e>>libc.Int32FromInt32(41) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (e&f ^ ^e&g) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(28) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (a>>libc.Int32FromInt32(34) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (a>>libc.Int32FromInt32(39) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (a&b ^ a&c ^ b&c)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = g + (d>>libc.Int32FromInt32(14) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (d>>libc.Int32FromInt32(18) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (d>>libc.Int32FromInt32(41) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (d&e ^ ^d&f) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(28) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (h>>libc.Int32FromInt32(34) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (h>>libc.Int32FromInt32(39) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (h&a ^ h&b ^ a&b)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = f + (c>>libc.Int32FromInt32(14) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (c>>libc.Int32FromInt32(18) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (c>>libc.Int32FromInt32(41) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (c&d ^ ^c&e) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(28) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (g>>libc.Int32FromInt32(34) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (g>>libc.Int32FromInt32(39) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (g&h ^ g&a ^ h&a)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = e + (b>>libc.Int32FromInt32(14) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (b>>libc.Int32FromInt32(18) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (b>>libc.Int32FromInt32(41) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (b&c ^ ^b&d) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(28) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (f>>libc.Int32FromInt32(34) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (f>>libc.Int32FromInt32(39) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (f&g ^ f&h ^ g&h)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = d + (a>>libc.Int32FromInt32(14) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (a>>libc.Int32FromInt32(18) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (a>>libc.Int32FromInt32(41) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (a&b ^ ^a&c) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(28) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (e>>libc.Int32FromInt32(34) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (e>>libc.Int32FromInt32(39) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (e&f ^ e&g ^ f&g)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = c + (h>>libc.Int32FromInt32(14) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (h>>libc.Int32FromInt32(18) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (h>>libc.Int32FromInt32(41) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (h&a ^ ^h&b) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(28) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (d>>libc.Int32FromInt32(34) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (d>>libc.Int32FromInt32(39) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (d&e ^ d&f ^ e&f)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = b + (g>>libc.Int32FromInt32(14) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (g>>libc.Int32FromInt32(18) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (g>>libc.Int32FromInt32(41) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (g&h ^ ^g&a) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(28) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (c>>libc.Int32FromInt32(34) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (c>>libc.Int32FromInt32(39) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (c&d ^ c&e ^ d&e)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = a + (f>>libc.Int32FromInt32(14) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (f>>libc.Int32FromInt32(18) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (f>>libc.Int32FromInt32(41) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (f&g ^ ^f&h) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(28) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (b>>libc.Int32FromInt32(34) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (b>>libc.Int32FromInt32(39) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Now for the remaining rounds up to 79: */
	for cond := true; cond; cond = j < int32(80) {
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p1 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p1)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = h + (e>>libc.Int32FromInt32(14) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (e>>libc.Int32FromInt32(18) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (e>>libc.Int32FromInt32(41) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (e&f ^ ^e&g) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p1))
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(28) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (a>>libc.Int32FromInt32(34) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (a>>libc.Int32FromInt32(39) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (a&b ^ a&c ^ b&c)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p2 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p2)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = g + (d>>libc.Int32FromInt32(14) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (d>>libc.Int32FromInt32(18) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (d>>libc.Int32FromInt32(41) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (d&e ^ ^d&f) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p2))
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(28) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (h>>libc.Int32FromInt32(34) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (h>>libc.Int32FromInt32(39) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (h&a ^ h&b ^ a&b)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p3 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p3)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = f + (c>>libc.Int32FromInt32(14) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (c>>libc.Int32FromInt32(18) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (c>>libc.Int32FromInt32(41) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (c&d ^ ^c&e) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p3))
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(28) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (g>>libc.Int32FromInt32(34) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (g>>libc.Int32FromInt32(39) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (g&h ^ g&a ^ h&a)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p4 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p4)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = e + (b>>libc.Int32FromInt32(14) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (b>>libc.Int32FromInt32(18) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (b>>libc.Int32FromInt32(41) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (b&c ^ ^b&d) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p4))
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(28) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (f>>libc.Int32FromInt32(34) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (f>>libc.Int32FromInt32(39) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (f&g ^ f&h ^ g&h)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p5 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p5)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = d + (a>>libc.Int32FromInt32(14) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (a>>libc.Int32FromInt32(18) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (a>>libc.Int32FromInt32(41) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (a&b ^ ^a&c) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p5))
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(28) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (e>>libc.Int32FromInt32(34) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (e>>libc.Int32FromInt32(39) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (e&f ^ e&g ^ f&g)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p6 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p6)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = c + (h>>libc.Int32FromInt32(14) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (h>>libc.Int32FromInt32(18) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (h>>libc.Int32FromInt32(41) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (h&a ^ ^h&b) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p6))
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(28) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (d>>libc.Int32FromInt32(34) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (d>>libc.Int32FromInt32(39) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (d&e ^ d&f ^ e&f)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p7 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p7)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = b + (g>>libc.Int32FromInt32(14) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (g>>libc.Int32FromInt32(18) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (g>>libc.Int32FromInt32(41) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (g&h ^ ^g&a) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p7))
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(28) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (c>>libc.Int32FromInt32(34) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (c>>libc.Int32FromInt32(39) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (c&d ^ c&e ^ d&e)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p8 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p8)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = a + (f>>libc.Int32FromInt32(14) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (f>>libc.Int32FromInt32(18) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (f>>libc.Int32FromInt32(41) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (f&g ^ ^f&h) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p8))
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(28) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (b>>libc.Int32FromInt32(34) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (b>>libc.Int32FromInt32(39) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Compute the current intermediate hash value */
	*(*Tuint64_t)(unsafe.Pointer(state)) += a
	*(*Tuint64_t)(unsafe.Pointer(state + 1*8)) += b
	*(*Tuint64_t)(unsafe.Pointer(state + 2*8)) += c
	*(*Tuint64_t)(unsafe.Pointer(state + 3*8)) += d
	*(*Tuint64_t)(unsafe.Pointer(state + 4*8)) += e
	*(*Tuint64_t)(unsafe.Pointer(state + 5*8)) += f
	*(*Tuint64_t)(unsafe.Pointer(state + 6*8)) += g
	*(*Tuint64_t)(unsafe.Pointer(state + 7*8)) += h
	/* Clean up */
	v16 = libc.Uint64FromInt32(0)
	T1 = v16
	v15 = v16
	h = v15
	v14 = v15
	g = v14
	v13 = v14
	f = v13
	v12 = v13
	e = v12
	v11 = v12
	d = v11
	v10 = v11
	c = v10
	v9 = v10
	b = v9
	a = v9
}

func x_SHA512Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var freespace, usedspace, v1, v2 Tsize_t
	_, _, _, _ = freespace, usedspace, v1, v2
	/* Calling with no data is valid (we do nothing) */
	if len1 == uint64(0) {
		return
	}
	usedspace = *(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA512_BLOCK_LENGTH)
	if usedspace > uint64(0) {
		/* Calculate how much free space is available in the buffer */
		freespace = uint64(m_SHA512_BLOCK_LENGTH) - usedspace
		if len1 >= freespace {
			/* Fill the buffer completely and process it */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, freespace, ^T__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += freespace << libc.Int32FromInt32(3)
			if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < freespace<<libc.Int32FromInt32(3) {
				*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
			}
			len1 -= freespace
			data += uintptr(freespace)
			x_SHA512Transform(tls, context, context+80)
		} else {
			/* The buffer is not yet full */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, len1, ^T__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << libc.Int32FromInt32(3)
			if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < len1<<libc.Int32FromInt32(3) {
				*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
			}
			/* Clean up: */
			v1 = libc.Uint64FromInt32(0)
			freespace = v1
			usedspace = v1
			return
		}
	}
	for len1 >= uint64(m_SHA512_BLOCK_LENGTH) {
		/* Process as many complete blocks as we can */
		x_SHA512Transform(tls, context, data)
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH) << libc.Int32FromInt32(3))
		if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)<<libc.Int32FromInt32(3)) {
			*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
		}
		len1 -= uint64(m_SHA512_BLOCK_LENGTH)
		data += uintptr(m_SHA512_BLOCK_LENGTH)
	}
	if len1 > uint64(0) {
		/* There's left-overs, so save 'em */
		libc.X__builtin___memcpy_chk(tls, context+80, data, len1, ^T__predefined_size_t(0))
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << libc.Int32FromInt32(3)
		if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < len1<<libc.Int32FromInt32(3) {
			*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
		}
	}
	/* Clean up: */
	v2 = libc.Uint64FromInt32(0)
	freespace = v2
	usedspace = v2
}

func x_SHA512Pad(tls *libc.TLS, context uintptr) {
	var usedspace, v1 uint32
	_, _ = usedspace, v1
	usedspace = uint32(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA512_BLOCK_LENGTH))
	if usedspace > uint32(0) {
		/* Begin padding with a 1 bit: */
		v1 = usedspace
		usedspace++
		*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(v1))) = uint8(0x80)
		if usedspace <= libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) {
			/* Set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16))-usedspace), ^T__predefined_size_t(0))
		} else {
			if usedspace < uint32(m_SHA512_BLOCK_LENGTH) {
				libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(uint32(m_SHA512_BLOCK_LENGTH)-usedspace), ^T__predefined_size_t(0))
			}
			/* Do second-to-last transform: */
			x_SHA512Transform(tls, context, context+80)
			/* And set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(2)), ^T__predefined_size_t(0))
		}
	} else {
		/* Prepare for final transform: */
		libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)), ^T__predefined_size_t(0))
		/* Begin padding with a 1 bit: */
		*(*Tuint8_t)(unsafe.Pointer(context + 80)) = uint8(0x80)
	}
	/* Store the length of input data (in bits) in big endian format: */
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)))
	/* Final transform: */
	x_SHA512Transform(tls, context, context+80)
	/* Clean up: */
	usedspace = uint32(0)
}

func x_SHA512Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	x_SHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(8)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^T__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-384: *********************************************************/
func x_SHA384Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha384_initial_hash_value)), uint64(64), ^T__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^T__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

func x_SHA384Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	x_SHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(6)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	/* Zero out state data */
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^T__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-512/256: *********************************************************/
func x_SHA512_256Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha512_256_initial_hash_value)), uint64(64), ^T__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^T__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

func x_SHA512_256Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	x_SHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(4)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	/* Zero out state data */
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^T__predefined_size_t(0))
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA224End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [28]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA224_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA224Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA224_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex5[libc.Int32FromUint8((*(*[28]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex5[libc.Int32FromUint8((*(*[28]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(28), ^T__predefined_size_t(0))
	return buf
}

var _hex5 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA224Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	x_SHA224Init(tls, bp)
	x_SHA256Update(tls, bp, data, len1)
	return x_SHA224End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA256End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [32]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA256Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA256_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex6[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex6[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(32), ^T__predefined_size_t(0))
	return buf
}

var _hex6 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA256Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	x_SHA256Init(tls, bp)
	x_SHA256Update(tls, bp, data, len1)
	return x_SHA256End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA384End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [48]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA384_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA384Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA384_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex7[libc.Int32FromUint8((*(*[48]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex7[libc.Int32FromUint8((*(*[48]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(48), ^T__predefined_size_t(0))
	return buf
}

var _hex7 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA384Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	x_SHA384Init(tls, bp)
	x_SHA512Update(tls, bp, data, len1)
	return x_SHA384End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA512End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [64]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA512Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA512_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex8[libc.Int32FromUint8((*(*[64]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex8[libc.Int32FromUint8((*(*[64]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(64), ^T__predefined_size_t(0))
	return buf
}

var _hex8 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA512Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	x_SHA512Init(tls, bp)
	x_SHA512Update(tls, bp, data, len1)
	return x_SHA512End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA512_256End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [32]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_256_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA512_256Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA512_256_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex9[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex9[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(32), ^T__predefined_size_t(0))
	return buf
}

var _hex9 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA512_256Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	x_SHA512_256Init(tls, bp)
	x_SHA512Update(tls, bp, data, len1)
	return x_SHA512_256End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "ref <%s> != got <%s>\n\x00d14a028c2a3a2bc9476102bb288234c415a2b01f828ea62ac5b3e42f\x00\x0023097d223405d8228642a477bda255b32aadbce4bda0b3f7e36c9da7\x00abc\x00a7470858e79c282bc2f6adfd831b132672dfd1224c1e78cbf5bcd057\x0012345\x00e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855\x00ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad\x005994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5\x0038b060a751ac96384cd9327eb1b1e36a21fdb71114be07434c0cc7bf63f6e1da274edebfe76f65fbd51ad2f14898b95b\x00cb00753f45a35e8bb5a03d699ac65007272c32ab0eded1631a8b605a43ff5bed8086072ba1e7cc2358baeca134c825a7\x000fa76955abfa9dafd83facca8343a92aa09497f98101086611b0bfa95dbc0dcc661d62e9568a5a032ba81960f3e55d4a\x00cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e\x00ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f\x003627909a29c31381a071ec27f7c9ca97726182aed29a7ddd2e54353322cfb30abb9e3a6df2ac2c20fe23436311d678564d0c8d305930575f60e2d3d048184d79\x00c672b8d1ef56ed28ab87c3622c5114069bdd3ad7b8f9737498d0c01ecef0967a\x0053048e2681941ef99b2e29b76b4c7dabe4c2d0c634fc6d46e0e2f13107e7af23\x00ee039e3bed452ceb91427fcef9f0e01b6af73272c8a103e5bb853c9e3170edef\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
