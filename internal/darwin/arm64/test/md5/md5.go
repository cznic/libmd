// Code generated for darwin/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -o md5.go md5.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build darwin && arm64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_MD5_BLOCK_LENGTH = 64
const m_MD5_DIGEST_LENGTH = 16

type T__predefined_size_t = uint64

type Tsize_t = uint64

type Tuint64_t = uint64

type Tuint32_t = uint32

type Tuint8_t = uint8

type TMD5_CTX = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

type Tmd5_state_t = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_MD5(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(160)
	defer tls.Free(160)
	var _ /* ctx at bp+72 */ TMD5_CTX
	var _ /* hash_bin_got at bp+16 */ [16]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [16]Tuint8_t
	var _ /* hash_str_got at bp+32 */ [33]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_MD5_DIGEST_LENGTH))
	x_MD5Data(tls, data, libc.Xstrlen(tls, data), bp+32)
	x_MD5Init(tls, bp+72)
	x_MD5Update(tls, bp+72, data, libc.Xstrlen(tls, data))
	x_MD5End(tls, bp+72, bp+32)
	x_MD5Init(tls, bp+72)
	x_MD5Update(tls, bp+72, data, libc.Xstrlen(tls, data))
	x_MD5Final(tls, bp+16, bp+72)
}

func _test_MD5_aladdin(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var _ /* hash_bin_got at bp+16 */ [16]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [16]Tuint8_t
	var _ /* pms at bp+32 */ Tmd5_state_t
	_hex2bin(tls, bp, hash_str_ref, uint64(m_MD5_DIGEST_LENGTH))
	x_MD5Init(tls, bp+32)
	x_MD5Update(tls, bp+32, data, libc.Xstrlen(tls, data))
	x_MD5Final(tls, bp+16, bp+32)
}

func _test_MD5_all(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	_test_MD5(tls, hash_str_ref, data)
	_test_MD5_aladdin(tls, hash_str_ref, data)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_MD5_all(tls, __ccgo_ts+22, __ccgo_ts+55)
	_test_MD5_all(tls, __ccgo_ts+56, __ccgo_ts+89)
	_test_MD5_all(tls, __ccgo_ts+93, __ccgo_ts+126)
	return 0
}

func main() {
	libc.Start(x_main)
}

/* Avoid polluting the namespace. Even though this makes this usage
 * implementation-specific, defining it unconditionally should not be
 * a problem, and better than possibly breaking unexpecting code. */

var _PADDING1 = [64]Tuint8_t{
	0: uint8(0x80),
}

// C documentation
//
//	/*
//	 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
//	 * initialization constants.
//	 */
func x_MD5Init(tls *libc.TLS, ctx uintptr) {
	(*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xefcdab89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98badcfe)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
}

// C documentation
//
//	/*
//	 * Update context to reflect the concatenation of another buffer full
//	 * of bytes.
//	 */
func x_MD5Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need Tsize_t
	_, _ = have, need
	/* Check how many bytes we already have and how many more we need. */
	have = (*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	need = uint64(m_MD5_BLOCK_LENGTH) - have
	/* Update bitcount */
	*(*Tuint64_t)(unsafe.Pointer(ctx + 16)) += len1 << int32(3)
	if len1 >= need {
		if have != uint64(0) {
			libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, need, ^T__predefined_size_t(0))
			x_MD5Transform(tls, ctx, ctx+24)
			input += uintptr(need)
			len1 -= need
			have = uint64(0)
		}
		/* Process data in MD5_BLOCK_LENGTH-byte chunks. */
		for len1 >= uint64(m_MD5_BLOCK_LENGTH) {
			x_MD5Transform(tls, ctx, input)
			input += uintptr(m_MD5_BLOCK_LENGTH)
			len1 -= uint64(m_MD5_BLOCK_LENGTH)
		}
	}
	/* Handle any remaining bytes of data. */
	if len1 != uint64(0) {
		libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, len1, ^T__predefined_size_t(0))
	}
}

// C documentation
//
//	/*
//	 * Pad pad to 64-byte boundary with the bit pattern
//	 * 1 0* (64-bit count of bits processed, MSB-first)
//	 */
func x_MD5Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* count at bp+0 */ [8]Tuint8_t
	_ = padlen
	/* Convert count to 8 bytes in little endian order. */
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount)
	/* Pad out to 56 mod 64. */
	padlen = uint64(m_MD5_BLOCK_LENGTH) - (*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount>>libc.Int32FromInt32(3)&libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_MD5_BLOCK_LENGTH)
	}
	x_MD5Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING1)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	x_MD5Update(tls, ctx, bp, uint64(8))
}

// C documentation
//
//	/*
//	 * Final wrapup--call MD5Pad, fill in digest and zero out ctx.
//	 */
func x_MD5Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	x_MD5Pad(tls, ctx)
	i = 0
	for {
		if !(i < int32(4)) {
			break
		}
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, ctx, 0, uint64(88), ^T__predefined_size_t(0))
}

/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */

/* This is the central step in the MD5 algorithm. */

// C documentation
//
//	/*
//	 * The core of the MD5 algorithm, this alters an existing MD5 hash to
//	 * reflect the addition of 16 longwords of new data.  MD5Update blocks
//	 * the data and converts bytes into longwords for this routine.
//	 */
func x_MD5Transform(tls *libc.TLS, state uintptr, block uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, b, c, d Tuint32_t
	var _ /* in at bp+0 */ [16]Tuint32_t
	_, _, _, _ = a, b, c, d
	libc.X__builtin___memcpy_chk(tls, bp, block, uint64(64), ^T__predefined_size_t(0))
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xd76aa478)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xe8c7b756)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x242070db)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xc1bdceee)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xf57c0faf)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0x4787c62a)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xa8304613)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0xfd469501)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x698098d8)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x8b44f7af)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xffff5bb1)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x895cd7be)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x6b901122)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0xfd987193)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xa679438e)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x49b40821)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xf61e2562)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xc040b340)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x265e5a51)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xe9b6c7aa)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xd62f105d)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0x02441453)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0xd8a1e681)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xe7d3fbc8)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x21e1cde6)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xc33707d6)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xf4d50d87)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x455a14ed)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0xa9e3e905)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0xfcefa3f8)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x676f02d9)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x8d2a4c8a)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xfffa3942)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x8771f681)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x6d9d6122)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xfde5380c)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xa4beea44)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0x4bdecfa9)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0xf6bb4b60)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xbebfbc70)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x289b7ec6)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xeaa127fa)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xd4ef3085)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0x04881d05)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0xd9d4d039)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0xe6db99e5)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x1fa27cf8)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0xc4ac5665)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xf4292244)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x432aff97)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xab9423a7)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xfc93a039)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x655b59c3)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0x8f0ccc92)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xffeff47d)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0x85845dd1)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x6fa87e4f)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0xfe2ce6e0)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xa3014314)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x4e0811a1)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xf7537e82)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0xbd3af235)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x2ad7d2bb)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0xeb86d391)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
}

/* Avoid polluting the namespace. Even though this makes this usage
 * implementation-specific, defining it unconditionally should not be
 * a problem, and better than possibly breaking unexpecting code. */

// C documentation
//
//	/* ARGSUSED */
func x_MD5End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_MD5Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD5_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex2[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex2[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(16), ^T__predefined_size_t(0))
	return buf
}

var _hex2 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_MD5Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TMD5_CTX
	x_MD5Init(tls, bp)
	x_MD5Update(tls, bp, data, len1)
	return x_MD5End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "ref <%s> != got <%s>\n\x00d41d8cd98f00b204e9800998ecf8427e\x00\x00900150983cd24fb0d6963f7d28e17f72\x00abc\x00827ccb0eea8a706c4c34a16891f84e7b\x0012345\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
