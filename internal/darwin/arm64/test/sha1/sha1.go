// Code generated for darwin/arm64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -DNDEBUG -o sha1.go sha1.o.go ../src/.libs/libmd.a', DO NOT EDIT.

//go:build darwin && arm64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_SHA1_BLOCK_LENGTH = 64
const m_SHA1_DIGEST_LENGTH = 20

type T__predefined_size_t = uint64

type Tsize_t = uint64

type Tuint64_t = uint64

type Tuint32_t = uint32

type Tuint8_t = uint8

type TSHA1_CTX = struct {
	Fstate  [5]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

func _hexchar2bin(tls *libc.TLS, c int32) (r int32) {
	if c >= int32('0') && c <= int32('9') {
		return c - int32('0')
	} else {
		if c >= int32('a') && c <= int32('f') {
			return c - int32('a') + int32(10)
		} else {
			if c >= int32('A') && c <= int32('F') {
				return c - int32('A') + int32(10)
			}
		}
	}
	return r
}

func _hex2bin(tls *libc.TLS, bin uintptr, str uintptr, bin_len Tsize_t) {
	var i Tsize_t
	_ = i
	i = uint64(0)
	for {
		if !(i < bin_len) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(bin + uintptr(i))) = libc.Uint8FromInt32(_hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2))))))<<int32(4) | _hexchar2bin(tls, int32(*(*int8)(unsafe.Pointer(str + uintptr(i*uint64(2)+uint64(1)))))))
		goto _1
	_1:
		;
		i++
	}
}

func _test_SHA1(tls *libc.TLS, hash_str_ref uintptr, data uintptr) {
	bp := tls.Alloc(192)
	defer tls.Free(192)
	var _ /* ctx at bp+88 */ TSHA1_CTX
	var _ /* hash_bin_got at bp+20 */ [20]Tuint8_t
	var _ /* hash_bin_ref at bp+0 */ [20]Tuint8_t
	var _ /* hash_str_got at bp+40 */ [41]int8
	_hex2bin(tls, bp, hash_str_ref, uint64(m_SHA1_DIGEST_LENGTH))
	x_SHA1Data(tls, data, libc.Xstrlen(tls, data), bp+40)
	x_SHA1Init(tls, bp+88)
	x_SHA1Update(tls, bp+88, data, libc.Xstrlen(tls, data))
	x_SHA1End(tls, bp+88, bp+40)
	x_SHA1Init(tls, bp+88)
	x_SHA1Update(tls, bp+88, data, libc.Xstrlen(tls, data))
	x_SHA1Final(tls, bp+20, bp+88)
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	_test_SHA1(tls, __ccgo_ts+22, __ccgo_ts+63)
	_test_SHA1(tls, __ccgo_ts+64, __ccgo_ts+105)
	_test_SHA1(tls, __ccgo_ts+109, __ccgo_ts+150)
	return 0
}

func main() {
	libc.Start(x_main)
}

// C documentation
//
//	/*
//	 * Hash a single 512-bit block. This is the core of the algorithm.
//	 */
func x_SHA1Transform(tls *libc.TLS, state uintptr, buffer uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, b, c, d, e, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v6, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v7, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v8, v80, v81, v82, v83, v84, v9 Tuint32_t
	var block uintptr
	var _ /* workspace at bp+0 */ [64]Tuint8_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a, b, block, c, d, e, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v6, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v7, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v8, v80, v81, v82, v83, v84, v9
	block = bp
	libc.X__builtin___memcpy_chk(tls, block, buffer, uint64(m_SHA1_BLOCK_LENGTH), ^T__predefined_size_t(0))
	/* Copy context->state[] to working vars */
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* 4 rounds of 20 operations each. Loop unrolled. */
	v1 = (*(*Tuint32_t)(unsafe.Pointer(block))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block)) = v1
	e += b&(c^d) ^ d + v1 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v2 = (*(*Tuint32_t)(unsafe.Pointer(block + 1*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 1*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 1*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 1*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 1*4)) = v2
	d += a&(b^c) ^ c + v2 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v3 = (*(*Tuint32_t)(unsafe.Pointer(block + 2*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 2*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 2*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 2*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 2*4)) = v3
	c += e&(a^b) ^ b + v3 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v4 = (*(*Tuint32_t)(unsafe.Pointer(block + 3*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 3*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 3*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 3*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 3*4)) = v4
	b += d&(e^a) ^ a + v4 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v5 = (*(*Tuint32_t)(unsafe.Pointer(block + 4*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 4*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 4*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 4*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 4*4)) = v5
	a += c&(d^e) ^ e + v5 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v6 = (*(*Tuint32_t)(unsafe.Pointer(block + 5*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 5*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 5*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 5*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 5*4)) = v6
	e += b&(c^d) ^ d + v6 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v7 = (*(*Tuint32_t)(unsafe.Pointer(block + 6*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 6*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 6*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 6*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 6*4)) = v7
	d += a&(b^c) ^ c + v7 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v8 = (*(*Tuint32_t)(unsafe.Pointer(block + 7*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 7*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 7*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 7*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 7*4)) = v8
	c += e&(a^b) ^ b + v8 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v9 = (*(*Tuint32_t)(unsafe.Pointer(block + 8*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 8*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 8*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 8*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 8*4)) = v9
	b += d&(e^a) ^ a + v9 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v10 = (*(*Tuint32_t)(unsafe.Pointer(block + 9*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 9*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 9*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 9*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 9*4)) = v10
	a += c&(d^e) ^ e + v10 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v11 = (*(*Tuint32_t)(unsafe.Pointer(block + 10*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 10*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 10*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 10*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 10*4)) = v11
	e += b&(c^d) ^ d + v11 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v12 = (*(*Tuint32_t)(unsafe.Pointer(block + 11*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 11*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 11*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 11*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 11*4)) = v12
	d += a&(b^c) ^ c + v12 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v13 = (*(*Tuint32_t)(unsafe.Pointer(block + 12*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 12*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 12*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 12*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 12*4)) = v13
	c += e&(a^b) ^ b + v13 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v14 = (*(*Tuint32_t)(unsafe.Pointer(block + 13*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 13*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 13*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 13*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 13*4)) = v14
	b += d&(e^a) ^ a + v14 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v15 = (*(*Tuint32_t)(unsafe.Pointer(block + 14*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 14*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 14*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 14*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 14*4)) = v15
	a += c&(d^e) ^ e + v15 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v16 = (*(*Tuint32_t)(unsafe.Pointer(block + 15*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 15*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 15*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 15*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 15*4)) = v16
	e += b&(c^d) ^ d + v16 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v17 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)) = v17
	d += a&(b^c) ^ c + v17 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v18 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)) = v18
	c += e&(a^b) ^ b + v18 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v19 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)) = v19
	b += d&(e^a) ^ a + v19 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v20 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)) = v20
	a += c&(d^e) ^ e + v20 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v21 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)) = v21
	e += b ^ c ^ d + v21 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v22 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)) = v22
	d += a ^ b ^ c + v22 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v23 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)) = v23
	c += e ^ a ^ b + v23 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v24 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)) = v24
	b += d ^ e ^ a + v24 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v25 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)) = v25
	a += c ^ d ^ e + v25 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v26 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)) = v26
	e += b ^ c ^ d + v26 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v27 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)) = v27
	d += a ^ b ^ c + v27 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v28 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)) = v28
	c += e ^ a ^ b + v28 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v29 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)) = v29
	b += d ^ e ^ a + v29 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v30 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)) = v30
	a += c ^ d ^ e + v30 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v31 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)) = v31
	e += b ^ c ^ d + v31 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v32 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)) = v32
	d += a ^ b ^ c + v32 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v33 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)) = v33
	c += e ^ a ^ b + v33 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v34 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)) = v34
	b += d ^ e ^ a + v34 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v35 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)) = v35
	a += c ^ d ^ e + v35 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v36 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)) = v36
	e += b ^ c ^ d + v36 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v37 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)) = v37
	d += a ^ b ^ c + v37 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v38 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)) = v38
	c += e ^ a ^ b + v38 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v39 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)) = v39
	b += d ^ e ^ a + v39 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v40 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)) = v40
	a += c ^ d ^ e + v40 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v41 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)) = v41
	e += (b|c)&d | b&c + v41 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v42 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)) = v42
	d += (a|b)&c | a&b + v42 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v43 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)) = v43
	c += (e|a)&b | e&a + v43 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v44 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)) = v44
	b += (d|e)&a | d&e + v44 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v45 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)) = v45
	a += (c|d)&e | c&d + v45 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v46 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)) = v46
	e += (b|c)&d | b&c + v46 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v47 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)) = v47
	d += (a|b)&c | a&b + v47 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v48 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)) = v48
	c += (e|a)&b | e&a + v48 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v49 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)) = v49
	b += (d|e)&a | d&e + v49 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v50 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)) = v50
	a += (c|d)&e | c&d + v50 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v51 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)) = v51
	e += (b|c)&d | b&c + v51 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v52 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)) = v52
	d += (a|b)&c | a&b + v52 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v53 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)) = v53
	c += (e|a)&b | e&a + v53 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v54 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)) = v54
	b += (d|e)&a | d&e + v54 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v55 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)) = v55
	a += (c|d)&e | c&d + v55 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v56 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)) = v56
	e += (b|c)&d | b&c + v56 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v57 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)) = v57
	d += (a|b)&c | a&b + v57 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v58 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)) = v58
	c += (e|a)&b | e&a + v58 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v59 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)) = v59
	b += (d|e)&a | d&e + v59 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v60 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)) = v60
	a += (c|d)&e | c&d + v60 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v61 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)) = v61
	e += b ^ c ^ d + v61 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v62 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)) = v62
	d += a ^ b ^ c + v62 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v63 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)) = v63
	c += e ^ a ^ b + v63 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v64 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)) = v64
	b += d ^ e ^ a + v64 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v65 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)) = v65
	a += c ^ d ^ e + v65 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v66 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)) = v66
	e += b ^ c ^ d + v66 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v67 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)) = v67
	d += a ^ b ^ c + v67 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v68 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)) = v68
	c += e ^ a ^ b + v68 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v69 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)) = v69
	b += d ^ e ^ a + v69 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v70 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)) = v70
	a += c ^ d ^ e + v70 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v71 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)) = v71
	e += b ^ c ^ d + v71 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v72 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)) = v72
	d += a ^ b ^ c + v72 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v73 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)) = v73
	c += e ^ a ^ b + v73 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v74 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)) = v74
	b += d ^ e ^ a + v74 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v75 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)) = v75
	a += c ^ d ^ e + v75 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v76 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)) = v76
	e += b ^ c ^ d + v76 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v77 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)) = v77
	d += a ^ b ^ c + v77 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v78 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)) = v78
	c += e ^ a ^ b + v78 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v79 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)) = v79
	b += d ^ e ^ a + v79 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v80 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)) = v80
	a += c ^ d ^ e + v80 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	/* Add the working vars back into context.state[] */
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) += e
	/* Wipe variables */
	v84 = libc.Uint32FromInt32(0)
	e = v84
	v83 = v84
	d = v83
	v82 = v83
	c = v82
	v81 = v82
	b = v81
	a = v81
}

// C documentation
//
//	/*
//	 * SHA1Init - Initialize new context
//	 */
func x_SHA1Init(tls *libc.TLS, context uintptr) {
	/* SHA1 initialization constants */
	(*TSHA1_CTX)(unsafe.Pointer(context)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(context)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(context + 1*4)) = uint32(0xEFCDAB89)
	*(*Tuint32_t)(unsafe.Pointer(context + 2*4)) = uint32(0x98BADCFE)
	*(*Tuint32_t)(unsafe.Pointer(context + 3*4)) = uint32(0x10325476)
	*(*Tuint32_t)(unsafe.Pointer(context + 4*4)) = uint32(0xC3D2E1F0)
}

// C documentation
//
//	/*
//	 * Run your data through this.
//	 */
func x_SHA1Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var i, j, v1 Tsize_t
	_, _, _ = i, j, v1
	j = (*TSHA1_CTX)(unsafe.Pointer(context)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(63)
	*(*Tuint64_t)(unsafe.Pointer(context + 24)) += len1 << libc.Int32FromInt32(3)
	if j+len1 > uint64(63) {
		v1 = libc.Uint64FromInt32(64) - j
		i = v1
		libc.X__builtin___memcpy_chk(tls, context+32+uintptr(j), data, v1, ^T__predefined_size_t(0))
		x_SHA1Transform(tls, context, context+32)
		for {
			if !(i+uint64(63) < len1) {
				break
			}
			x_SHA1Transform(tls, context, data+uintptr(i))
			goto _2
		_2:
			;
			i += uint64(64)
		}
		j = uint64(0)
	} else {
		i = uint64(0)
	}
	libc.X__builtin___memcpy_chk(tls, context+32+uintptr(j), data+uintptr(i), len1-i, ^T__predefined_size_t(0))
}

// C documentation
//
//	/*
//	 * Add padding and return the message digest.
//	 */
func x_SHA1Pad(tls *libc.TLS, context uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i uint32
	var _ /* finalcount at bp+0 */ [8]Tuint8_t
	_ = i
	i = uint32(0)
	for {
		if !(i < uint32(8)) {
			break
		}
		(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[i] = uint8((*TSHA1_CTX)(unsafe.Pointer(context)).Fcount >> ((libc.Uint32FromInt32(7) - i&libc.Uint32FromInt32(7)) * libc.Uint32FromInt32(8)) & libc.Uint64FromInt32(255)) /* Endian independent */
		goto _1
	_1:
		;
		i++
	}
	x_SHA1Update(tls, context, __ccgo_ts+308, uint64(1))
	for (*TSHA1_CTX)(unsafe.Pointer(context)).Fcount&uint64(504) != uint64(448) {
		x_SHA1Update(tls, context, __ccgo_ts+310, uint64(1))
	}
	x_SHA1Update(tls, context, bp, uint64(8)) /* Should cause a SHA1Transform() */
}

func x_SHA1Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i uint32
	_ = i
	x_SHA1Pad(tls, context)
	i = uint32(0)
	for {
		if !(i < uint32(m_SHA1_DIGEST_LENGTH)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i>>int32(2))*4)) >> ((libc.Uint32FromInt32(3) - i&libc.Uint32FromInt32(3)) * libc.Uint32FromInt32(8)) & libc.Uint32FromInt32(255))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(96), ^T__predefined_size_t(0))
}

// C documentation
//
//	/* ARGSUSED */
func x_SHA1End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [20]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA1_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	x_SHA1Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA1_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex4[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex4[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(20), ^T__predefined_size_t(0))
	return buf
}

var _hex4 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func x_SHA1Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TSHA1_CTX
	x_SHA1Init(tls, bp)
	x_SHA1Update(tls, bp, data, len1)
	return x_SHA1End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "ref <%s> != got <%s>\n\x00da39a3ee5e6b4b0d3255bfef95601890afd80709\x00\x00a9993e364706816aba3e25717850c26c9cd0d89d\x00abc\x008cb2237d0679ca88db6464eac60da96345513964\x0012345\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
