// Copyright 2023 The libmd-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libmd // import "modernc.org/libmd"

import (
	"context"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"testing"
	"time"

	_ "modernc.org/cc/v4"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/fileutil/ccgo"
)

var (
	goos   = runtime.GOOS
	goarch = runtime.GOARCH
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func Test(t *testing.T) {
	tempDir := t.TempDir()
	var files, skip, compileok, buildok, fail, pass int
	filepath.Walk(filepath.Join("internal", goos, goarch, "test"), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			t.Fatal(err)
		}

		if info.IsDir() || filepath.Ext(path) != ".go" {
			return nil
		}

		files++
		bin := filepath.Join(tempDir, "test")
		if goos == "windows" {
			bin += ".exe"
		}
		os.Remove(bin)

		out, err := run(5*time.Minute, "", "go", "build", "-o", bin, path)
		if err != nil {
			t.Errorf("out=%s FAIL err=%v", out, err)
			return nil
		}

		compileok++
		buildok++
		if out, err = run(5*time.Minute, "", bin, "-v"); err != nil {
			fail++
			t.Errorf("out=%s FAIL err=%v", out, err)
			return nil
		}

		t.Logf("%s: PASS", path)
		pass++
		return nil
	})

	t.Logf("files=%v skip=%v compileok=%v buildok=%v fail=%v pass=%v", files, skip, compileok, buildok, fail, pass)
	// all_test.go:68: files=6 skip=0 compileok=6 buildok=6 fail=0 pass=6
}

func run(limit time.Duration, inDir, bin string, args ...string) (out []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), limit)

	defer cancel()

	cmd := exec.CommandContext(ctx, bin, args...)
	cmd.Dir = inDir
	cmd.WaitDelay = 10 * time.Second
	return cmd.CombinedOutput()
}
