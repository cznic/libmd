// Code generated for darwin/amd64 by 'generator --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -extended-errors -ignore-unsupported-alignment -ignore-link-errors -o libmd.go --package-name libmd src/.libs/libmd.a', DO NOT EDIT.

//go:build darwin && amd64

package libmd

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

const m_BIG_ENDIAN = "__DARWIN_BIG_ENDIAN"
const m_BYTE_ORDER = "__DARWIN_BYTE_ORDER"
const m_FD_SETSIZE = "__DARWIN_FD_SETSIZE"
const m_HAVE_CONFIG_H = 1
const m_HAVE_DLFCN_H = 1
const m_HAVE_INTTYPES_H = 1
const m_HAVE_STDINT_H = 1
const m_HAVE_STDIO_H = 1
const m_HAVE_STDLIB_H = 1
const m_HAVE_STRINGS_H = 1
const m_HAVE_STRING_H = 1
const m_HAVE_SYS_STAT_H = 1
const m_HAVE_SYS_TYPES_H = 1
const m_HAVE_UNISTD_H = 1
const m_HAVE_WCHAR_H = 1
const m_INTMAX_MAX = "__INTMAX_MAX__"
const m_INTPTR_MAX = "__INTPTR_MAX__"
const m_INT_FAST16_MAX = "__INT_LEAST16_MAX"
const m_INT_FAST16_MIN = "__INT_LEAST16_MIN"
const m_INT_FAST32_MAX = "__INT_LEAST32_MAX"
const m_INT_FAST32_MIN = "__INT_LEAST32_MIN"
const m_INT_FAST64_MAX = "__INT_LEAST64_MAX"
const m_INT_FAST64_MIN = "__INT_LEAST64_MIN"
const m_INT_FAST8_MAX = "__INT_LEAST8_MAX"
const m_INT_FAST8_MIN = "__INT_LEAST8_MIN"
const m_INT_LEAST16_MAX = "__INT_LEAST16_MAX"
const m_INT_LEAST16_MIN = "__INT_LEAST16_MIN"
const m_INT_LEAST32_MAX = "__INT_LEAST32_MAX"
const m_INT_LEAST32_MIN = "__INT_LEAST32_MIN"
const m_INT_LEAST64_MAX = "__INT_LEAST64_MAX"
const m_INT_LEAST64_MIN = "__INT_LEAST64_MIN"
const m_INT_LEAST8_MAX = "__INT_LEAST8_MAX"
const m_INT_LEAST8_MIN = "__INT_LEAST8_MIN"
const m_LITTLE_ENDIAN = "__DARWIN_LITTLE_ENDIAN"
const m_LT_OBJDIR = ".libs/"
const m_MAC_OS_VERSION_11_0 = "__MAC_11_0"
const m_MAC_OS_VERSION_11_1 = "__MAC_11_1"
const m_MAC_OS_VERSION_11_3 = "__MAC_11_3"
const m_MAC_OS_VERSION_11_4 = "__MAC_11_4"
const m_MAC_OS_VERSION_11_5 = "__MAC_11_5"
const m_MAC_OS_VERSION_11_6 = "__MAC_11_6"
const m_MAC_OS_VERSION_12_0 = "__MAC_12_0"
const m_MAC_OS_VERSION_12_1 = "__MAC_12_1"
const m_MAC_OS_VERSION_12_2 = "__MAC_12_2"
const m_MAC_OS_VERSION_12_3 = "__MAC_12_3"
const m_MAC_OS_VERSION_12_4 = "__MAC_12_4"
const m_MAC_OS_VERSION_12_5 = "__MAC_12_5"
const m_MAC_OS_VERSION_12_6 = "__MAC_12_6"
const m_MAC_OS_VERSION_12_7 = "__MAC_12_7"
const m_MAC_OS_VERSION_13_0 = "__MAC_13_0"
const m_MAC_OS_VERSION_13_1 = "__MAC_13_1"
const m_MAC_OS_VERSION_13_2 = "__MAC_13_2"
const m_MAC_OS_VERSION_13_3 = "__MAC_13_3"
const m_MAC_OS_VERSION_13_4 = "__MAC_13_4"
const m_MAC_OS_VERSION_13_5 = "__MAC_13_5"
const m_MAC_OS_VERSION_13_6 = "__MAC_13_6"
const m_MAC_OS_VERSION_14_0 = "__MAC_14_0"
const m_MAC_OS_VERSION_14_1 = "__MAC_14_1"
const m_MAC_OS_VERSION_14_2 = "__MAC_14_2"
const m_MAC_OS_VERSION_14_3 = "__MAC_14_3"
const m_MAC_OS_VERSION_14_4 = "__MAC_14_4"
const m_MAC_OS_VERSION_14_5 = "__MAC_14_5"
const m_MAC_OS_VERSION_15_0 = "__MAC_15_0"
const m_MAC_OS_VERSION_15_1 = "__MAC_15_1"
const m_MAC_OS_VERSION_15_2 = "__MAC_15_2"
const m_MAC_OS_X_VERSION_10_0 = "__MAC_10_0"
const m_MAC_OS_X_VERSION_10_1 = "__MAC_10_1"
const m_MAC_OS_X_VERSION_10_10 = "__MAC_10_10"
const m_MAC_OS_X_VERSION_10_10_2 = "__MAC_10_10_2"
const m_MAC_OS_X_VERSION_10_10_3 = "__MAC_10_10_3"
const m_MAC_OS_X_VERSION_10_11 = "__MAC_10_11"
const m_MAC_OS_X_VERSION_10_11_2 = "__MAC_10_11_2"
const m_MAC_OS_X_VERSION_10_11_3 = "__MAC_10_11_3"
const m_MAC_OS_X_VERSION_10_11_4 = "__MAC_10_11_4"
const m_MAC_OS_X_VERSION_10_12 = "__MAC_10_12"
const m_MAC_OS_X_VERSION_10_12_1 = "__MAC_10_12_1"
const m_MAC_OS_X_VERSION_10_12_2 = "__MAC_10_12_2"
const m_MAC_OS_X_VERSION_10_12_4 = "__MAC_10_12_4"
const m_MAC_OS_X_VERSION_10_13 = "__MAC_10_13"
const m_MAC_OS_X_VERSION_10_13_1 = "__MAC_10_13_1"
const m_MAC_OS_X_VERSION_10_13_2 = "__MAC_10_13_2"
const m_MAC_OS_X_VERSION_10_13_4 = "__MAC_10_13_4"
const m_MAC_OS_X_VERSION_10_14 = "__MAC_10_14"
const m_MAC_OS_X_VERSION_10_14_1 = "__MAC_10_14_1"
const m_MAC_OS_X_VERSION_10_14_4 = "__MAC_10_14_4"
const m_MAC_OS_X_VERSION_10_14_5 = "__MAC_10_14_5"
const m_MAC_OS_X_VERSION_10_14_6 = "__MAC_10_14_6"
const m_MAC_OS_X_VERSION_10_15 = "__MAC_10_15"
const m_MAC_OS_X_VERSION_10_15_1 = "__MAC_10_15_1"
const m_MAC_OS_X_VERSION_10_15_4 = "__MAC_10_15_4"
const m_MAC_OS_X_VERSION_10_16 = "__MAC_10_16"
const m_MAC_OS_X_VERSION_10_2 = "__MAC_10_2"
const m_MAC_OS_X_VERSION_10_3 = "__MAC_10_3"
const m_MAC_OS_X_VERSION_10_4 = "__MAC_10_4"
const m_MAC_OS_X_VERSION_10_5 = "__MAC_10_5"
const m_MAC_OS_X_VERSION_10_6 = "__MAC_10_6"
const m_MAC_OS_X_VERSION_10_7 = "__MAC_10_7"
const m_MAC_OS_X_VERSION_10_8 = "__MAC_10_8"
const m_MAC_OS_X_VERSION_10_9 = "__MAC_10_9"
const m_MD2_BLOCK_LENGTH = 16
const m_MD2_DIGEST_LENGTH = 16
const m_MD2_DIGEST_STRING_LENGTH = 33
const m_NBBY = "__DARWIN_NBBY"
const m_NDEBUG = 1
const m_NFDBITS = "__DARWIN_NFDBITS"
const m_NULL = "__DARWIN_NULL"
const m_PACKAGE = "libmd"
const m_PACKAGE_BUGREPORT = "libbsd@lists.freedesktop.org"
const m_PACKAGE_NAME = "libmd"
const m_PACKAGE_STRING = "libmd 1.1.0"
const m_PACKAGE_TARNAME = "libmd"
const m_PACKAGE_URL = ""
const m_PACKAGE_VERSION = "1.1.0"
const m_PDP_ENDIAN = "__DARWIN_PDP_ENDIAN"
const m_PTRDIFF_MAX = "__PTRDIFF_MAX__"
const m_SIZE_MAX = "__SIZE_MAX__"
const m_STDC_HEADERS = 1
const m_UINTMAX_MAX = "__UINTMAX_MAX__"
const m_UINTPTR_MAX = "__UINTPTR_MAX__"
const m_UINT_FAST16_MAX = "__UINT_LEAST16_MAX"
const m_UINT_FAST32_MAX = "__UINT_LEAST32_MAX"
const m_UINT_FAST64_MAX = "__UINT_LEAST64_MAX"
const m_UINT_FAST8_MAX = "__UINT_LEAST8_MAX"
const m_UINT_LEAST16_MAX = "__UINT_LEAST16_MAX"
const m_UINT_LEAST32_MAX = "__UINT_LEAST32_MAX"
const m_UINT_LEAST64_MAX = "__UINT_LEAST64_MAX"
const m_UINT_LEAST8_MAX = "__UINT_LEAST8_MAX"
const m_VERSION = "1.1.0"
const m_WCHAR_MAX = "__WCHAR_MAX__"
const m__ALL_SOURCE = 1
const m__DARWIN_C_SOURCE = 1
const m__DARWIN_FEATURE_64_BIT_INODE = 1
const m__DARWIN_FEATURE_ONLY_UNIX_CONFORMANCE = 1
const m__DARWIN_FEATURE_UNIX_CONFORMANCE = 3
const m__FORTIFY_SOURCE = 2
const m__GNU_SOURCE = 1
const m__HPUX_ALT_XOPEN_SOCKET_API = 1
const m__LP64 = 1
const m__NETBSD_SOURCE = 1
const m__OPENBSD_SOURCE = 1
const m__POSIX_PTHREAD_SEMANTICS = 1
const m__QUAD_HIGHWORD = 1
const m__QUAD_LOWWORD = 0
const m__TANDEM_SOURCE = 1
const m___API_TO_BE_DEPRECATED = 100000
const m___API_TO_BE_DEPRECATED_DRIVERKIT = 100000
const m___API_TO_BE_DEPRECATED_IOS = 100000
const m___API_TO_BE_DEPRECATED_MACCATALYST = 100000
const m___API_TO_BE_DEPRECATED_MACOS = 100000
const m___API_TO_BE_DEPRECATED_TVOS = 100000
const m___API_TO_BE_DEPRECATED_VISIONOS = 100000
const m___API_TO_BE_DEPRECATED_WATCHOS = 100000
const m___APPLE_CC__ = 6000
const m___APPLE__ = 1
const m___ATOMIC_ACQUIRE = 2
const m___ATOMIC_ACQ_REL = 4
const m___ATOMIC_CONSUME = 1
const m___ATOMIC_RELAXED = 0
const m___ATOMIC_RELEASE = 3
const m___ATOMIC_SEQ_CST = 5
const m___BIGGEST_ALIGNMENT__ = 16
const m___BITINT_MAXWIDTH__ = 8388608
const m___BLOCKS__ = 1
const m___BOOL_WIDTH__ = 8
const m___BRIDGEOS_2_0 = 20000
const m___BRIDGEOS_3_0 = 30000
const m___BRIDGEOS_3_1 = 30100
const m___BRIDGEOS_3_4 = 30400
const m___BRIDGEOS_4_0 = 40000
const m___BRIDGEOS_4_1 = 40100
const m___BRIDGEOS_5_0 = 50000
const m___BRIDGEOS_5_1 = 50100
const m___BRIDGEOS_5_3 = 50300
const m___BRIDGEOS_6_0 = 60000
const m___BRIDGEOS_6_2 = 60200
const m___BRIDGEOS_6_4 = 60400
const m___BRIDGEOS_6_5 = 60500
const m___BRIDGEOS_6_6 = 60600
const m___BRIDGEOS_7_0 = 70000
const m___BRIDGEOS_7_1 = 70100
const m___BRIDGEOS_7_2 = 70200
const m___BRIDGEOS_7_3 = 70300
const m___BRIDGEOS_7_4 = 70400
const m___BRIDGEOS_7_6 = 70600
const m___BRIDGEOS_8_0 = 80000
const m___BRIDGEOS_8_1 = 80100
const m___BRIDGEOS_8_2 = 80200
const m___BRIDGEOS_8_3 = 80300
const m___BRIDGEOS_8_4 = 80400
const m___BRIDGEOS_8_5 = 80500
const m___BRIDGEOS_9_0 = 90000
const m___BRIDGEOS_9_1 = 90100
const m___BRIDGEOS_9_2 = 90200
const m___BYTE_ORDER__ = "__ORDER_LITTLE_ENDIAN__"
const m___CCGO__ = 1
const m___CHAR_BIT__ = 8
const m___CLANG_ATOMIC_BOOL_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___CLANG_ATOMIC_CHAR_LOCK_FREE = 2
const m___CLANG_ATOMIC_INT_LOCK_FREE = 2
const m___CLANG_ATOMIC_LLONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_LONG_LOCK_FREE = 2
const m___CLANG_ATOMIC_POINTER_LOCK_FREE = 2
const m___CLANG_ATOMIC_SHORT_LOCK_FREE = 2
const m___CLANG_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___CONSTANT_CFSTRINGS__ = 1
const m___DARWIN_64_BIT_INO_T = 1
const m___DARWIN_BIG_ENDIAN = 4321
const m___DARWIN_BYTE_ORDER = "__DARWIN_LITTLE_ENDIAN"
const m___DARWIN_C_ANSI = 010000
const m___DARWIN_C_FULL = 900000
const m___DARWIN_C_LEVEL = "__DARWIN_C_FULL"
const m___DARWIN_FD_SETSIZE = 1024
const m___DARWIN_LITTLE_ENDIAN = 1234
const m___DARWIN_NBBY = 8
const m___DARWIN_NON_CANCELABLE = 0
const m___DARWIN_NO_LONG_LONG = 0
const m___DARWIN_ONLY_64_BIT_INO_T = 0
const m___DARWIN_ONLY_UNIX_CONFORMANCE = 1
const m___DARWIN_ONLY_VERS_1050 = 0
const m___DARWIN_PDP_ENDIAN = 3412
const m___DARWIN_SUF_1050 = "$1050"
const m___DARWIN_SUF_64_BIT_INO_T = "$INODE64"
const m___DARWIN_SUF_EXTSN = "$DARWIN_EXTSN"
const m___DARWIN_UNIX03 = 1
const m___DARWIN_VERS_1050 = 1
const m___DARWIN_WCHAR_MAX = "__WCHAR_MAX__"
const m___DBL_DECIMAL_DIG__ = 17
const m___DBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___DBL_DIG__ = 15
const m___DBL_EPSILON__ = 2.2204460492503131e-16
const m___DBL_HAS_DENORM__ = 1
const m___DBL_HAS_INFINITY__ = 1
const m___DBL_HAS_QUIET_NAN__ = 1
const m___DBL_MANT_DIG__ = 53
const m___DBL_MAX_10_EXP__ = 308
const m___DBL_MAX_EXP__ = 1024
const m___DBL_MAX__ = 1.7976931348623157e+308
const m___DBL_MIN__ = 2.2250738585072014e-308
const m___DECIMAL_DIG__ = "__LDBL_DECIMAL_DIG__"
const m___DRIVERKIT_19_0 = 190000
const m___DRIVERKIT_20_0 = 200000
const m___DRIVERKIT_21_0 = 210000
const m___DRIVERKIT_22_0 = 220000
const m___DRIVERKIT_22_4 = 220400
const m___DRIVERKIT_22_5 = 220500
const m___DRIVERKIT_22_6 = 220600
const m___DRIVERKIT_23_0 = 230000
const m___DRIVERKIT_23_1 = 230100
const m___DRIVERKIT_23_2 = 230200
const m___DRIVERKIT_23_3 = 230300
const m___DRIVERKIT_23_4 = 230400
const m___DRIVERKIT_23_5 = 230500
const m___DRIVERKIT_24_0 = 240000
const m___DRIVERKIT_24_1 = 240100
const m___DRIVERKIT_24_2 = 240200
const m___DYNAMIC__ = 1
const m___ENABLE_LEGACY_MAC_AVAILABILITY = 1
const m___ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__ = 150000
const m___ENVIRONMENT_OS_VERSION_MIN_REQUIRED__ = 150000
const m___EXTENSIONS__ = 1
const m___FINITE_MATH_ONLY__ = 0
const m___FLT16_DECIMAL_DIG__ = 5
const m___FLT16_DENORM_MIN__ = 5.9604644775390625e-8
const m___FLT16_DIG__ = 3
const m___FLT16_EPSILON__ = 9.765625e-4
const m___FLT16_HAS_DENORM__ = 1
const m___FLT16_HAS_INFINITY__ = 1
const m___FLT16_HAS_QUIET_NAN__ = 1
const m___FLT16_MANT_DIG__ = 11
const m___FLT16_MAX_10_EXP__ = 4
const m___FLT16_MAX_EXP__ = 16
const m___FLT16_MAX__ = 6.5504e+4
const m___FLT16_MIN__ = 6.103515625e-5
const m___FLT_DECIMAL_DIG__ = 9
const m___FLT_DENORM_MIN__ = 1.40129846e-45
const m___FLT_DIG__ = 6
const m___FLT_EPSILON__ = 1.19209290e-7
const m___FLT_HAS_DENORM__ = 1
const m___FLT_HAS_INFINITY__ = 1
const m___FLT_HAS_QUIET_NAN__ = 1
const m___FLT_MANT_DIG__ = 24
const m___FLT_MAX_10_EXP__ = 38
const m___FLT_MAX_EXP__ = 128
const m___FLT_MAX__ = 3.40282347e+38
const m___FLT_MIN__ = 1.17549435e-38
const m___FLT_RADIX__ = 2
const m___FPCLASS_NEGINF = 0x0004
const m___FPCLASS_NEGNORMAL = 0x0008
const m___FPCLASS_NEGSUBNORMAL = 0x0010
const m___FPCLASS_NEGZERO = 0x0020
const m___FPCLASS_POSINF = 0x0200
const m___FPCLASS_POSNORMAL = 0x0100
const m___FPCLASS_POSSUBNORMAL = 0x0080
const m___FPCLASS_POSZERO = 0x0040
const m___FPCLASS_QNAN = 0x0002
const m___FPCLASS_SNAN = 0x0001
const m___FUNCTION__ = "__func__"
const m___FXSR__ = 1
const m___GCC_ASM_FLAG_OUTPUTS__ = 1
const m___GCC_ATOMIC_BOOL_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR16_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR32_T_LOCK_FREE = 2
const m___GCC_ATOMIC_CHAR_LOCK_FREE = 2
const m___GCC_ATOMIC_INT_LOCK_FREE = 2
const m___GCC_ATOMIC_LLONG_LOCK_FREE = 2
const m___GCC_ATOMIC_LONG_LOCK_FREE = 2
const m___GCC_ATOMIC_POINTER_LOCK_FREE = 2
const m___GCC_ATOMIC_SHORT_LOCK_FREE = 2
const m___GCC_ATOMIC_TEST_AND_SET_TRUEVAL = 1
const m___GCC_ATOMIC_WCHAR_T_LOCK_FREE = 2
const m___GCC_HAVE_DWARF2_CFI_ASM = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_16 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 = 1
const m___GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 = 1
const m___GNUC_MINOR__ = 2
const m___GNUC_PATCHLEVEL__ = 1
const m___GNUC_STDC_INLINE__ = 1
const m___GNUC__ = 4
const m___GXX_ABI_VERSION = 1002
const m___INT16_FMTd__ = "hd"
const m___INT16_FMTi__ = "hi"
const m___INT16_MAX__ = 32767
const m___INT16_TYPE__ = "short"
const m___INT32_FMTd__ = "d"
const m___INT32_FMTi__ = "i"
const m___INT32_MAX__ = 2147483647
const m___INT32_TYPE__ = "int"
const m___INT64_C_SUFFIX__ = "LL"
const m___INT64_FMTd__ = "lld"
const m___INT64_FMTi__ = "lli"
const m___INT64_MAX__ = 9223372036854775807
const m___INT8_FMTd__ = "hhd"
const m___INT8_FMTi__ = "hhi"
const m___INT8_MAX__ = 127
const m___INTMAX_C_SUFFIX__ = "L"
const m___INTMAX_FMTd__ = "ld"
const m___INTMAX_FMTi__ = "li"
const m___INTMAX_MAX__ = 9223372036854775807
const m___INTMAX_WIDTH__ = 64
const m___INTPTR_FMTd__ = "ld"
const m___INTPTR_FMTi__ = "li"
const m___INTPTR_MAX__ = 9223372036854775807
const m___INTPTR_WIDTH__ = 64
const m___INT_FAST16_FMTd__ = "hd"
const m___INT_FAST16_FMTi__ = "hi"
const m___INT_FAST16_MAX__ = 32767
const m___INT_FAST16_TYPE__ = "short"
const m___INT_FAST16_WIDTH__ = 16
const m___INT_FAST32_FMTd__ = "d"
const m___INT_FAST32_FMTi__ = "i"
const m___INT_FAST32_MAX__ = 2147483647
const m___INT_FAST32_TYPE__ = "int"
const m___INT_FAST32_WIDTH__ = 32
const m___INT_FAST64_FMTd__ = "lld"
const m___INT_FAST64_FMTi__ = "lli"
const m___INT_FAST64_MAX__ = 9223372036854775807
const m___INT_FAST64_WIDTH__ = 64
const m___INT_FAST8_FMTd__ = "hhd"
const m___INT_FAST8_FMTi__ = "hhi"
const m___INT_FAST8_MAX__ = 127
const m___INT_FAST8_WIDTH__ = 8
const m___INT_LEAST16_FMTd__ = "hd"
const m___INT_LEAST16_FMTi__ = "hi"
const m___INT_LEAST16_MAX__ = 32767
const m___INT_LEAST16_TYPE__ = "short"
const m___INT_LEAST16_WIDTH__ = 16
const m___INT_LEAST32_FMTd__ = "d"
const m___INT_LEAST32_FMTi__ = "i"
const m___INT_LEAST32_MAX__ = 2147483647
const m___INT_LEAST32_TYPE__ = "int"
const m___INT_LEAST32_WIDTH__ = 32
const m___INT_LEAST64_FMTd__ = "lld"
const m___INT_LEAST64_FMTi__ = "lli"
const m___INT_LEAST64_MAX = "INT64_MAX"
const m___INT_LEAST64_MAX__ = 9223372036854775807
const m___INT_LEAST64_MIN = "INT64_MIN"
const m___INT_LEAST64_WIDTH__ = 64
const m___INT_LEAST8_FMTd__ = "hhd"
const m___INT_LEAST8_FMTi__ = "hhi"
const m___INT_LEAST8_MAX__ = 127
const m___INT_LEAST8_WIDTH__ = 8
const m___INT_MAX__ = 2147483647
const m___INT_WIDTH__ = 32
const m___IPHONE_10_0 = 100000
const m___IPHONE_10_1 = 100100
const m___IPHONE_10_2 = 100200
const m___IPHONE_10_3 = 100300
const m___IPHONE_11_0 = 110000
const m___IPHONE_11_1 = 110100
const m___IPHONE_11_2 = 110200
const m___IPHONE_11_3 = 110300
const m___IPHONE_11_4 = 110400
const m___IPHONE_12_0 = 120000
const m___IPHONE_12_1 = 120100
const m___IPHONE_12_2 = 120200
const m___IPHONE_12_3 = 120300
const m___IPHONE_12_4 = 120400
const m___IPHONE_13_0 = 130000
const m___IPHONE_13_1 = 130100
const m___IPHONE_13_2 = 130200
const m___IPHONE_13_3 = 130300
const m___IPHONE_13_4 = 130400
const m___IPHONE_13_5 = 130500
const m___IPHONE_13_6 = 130600
const m___IPHONE_13_7 = 130700
const m___IPHONE_14_0 = 140000
const m___IPHONE_14_1 = 140100
const m___IPHONE_14_2 = 140200
const m___IPHONE_14_3 = 140300
const m___IPHONE_14_4 = 140400
const m___IPHONE_14_5 = 140500
const m___IPHONE_14_6 = 140600
const m___IPHONE_14_7 = 140700
const m___IPHONE_14_8 = 140800
const m___IPHONE_15_0 = 150000
const m___IPHONE_15_1 = 150100
const m___IPHONE_15_2 = 150200
const m___IPHONE_15_3 = 150300
const m___IPHONE_15_4 = 150400
const m___IPHONE_15_5 = 150500
const m___IPHONE_15_6 = 150600
const m___IPHONE_15_7 = 150700
const m___IPHONE_15_8 = 150800
const m___IPHONE_16_0 = 160000
const m___IPHONE_16_1 = 160100
const m___IPHONE_16_2 = 160200
const m___IPHONE_16_3 = 160300
const m___IPHONE_16_4 = 160400
const m___IPHONE_16_5 = 160500
const m___IPHONE_16_6 = 160600
const m___IPHONE_16_7 = 160700
const m___IPHONE_17_0 = 170000
const m___IPHONE_17_1 = 170100
const m___IPHONE_17_2 = 170200
const m___IPHONE_17_3 = 170300
const m___IPHONE_17_4 = 170400
const m___IPHONE_17_5 = 170500
const m___IPHONE_18_0 = 180000
const m___IPHONE_18_1 = 180100
const m___IPHONE_18_2 = 180200
const m___IPHONE_2_0 = 20000
const m___IPHONE_2_1 = 20100
const m___IPHONE_2_2 = 20200
const m___IPHONE_3_0 = 30000
const m___IPHONE_3_1 = 30100
const m___IPHONE_3_2 = 30200
const m___IPHONE_4_0 = 40000
const m___IPHONE_4_1 = 40100
const m___IPHONE_4_2 = 40200
const m___IPHONE_4_3 = 40300
const m___IPHONE_5_0 = 50000
const m___IPHONE_5_1 = 50100
const m___IPHONE_6_0 = 60000
const m___IPHONE_6_1 = 60100
const m___IPHONE_7_0 = 70000
const m___IPHONE_7_1 = 70100
const m___IPHONE_8_0 = 80000
const m___IPHONE_8_1 = 80100
const m___IPHONE_8_2 = 80200
const m___IPHONE_8_3 = 80300
const m___IPHONE_8_4 = 80400
const m___IPHONE_9_0 = 90000
const m___IPHONE_9_1 = 90100
const m___IPHONE_9_2 = 90200
const m___IPHONE_9_3 = 90300
const m___LAHF_SAHF__ = 1
const m___LDBL_DECIMAL_DIG__ = 17
const m___LDBL_DENORM_MIN__ = 4.9406564584124654e-324
const m___LDBL_DIG__ = 15
const m___LDBL_EPSILON__ = 2.2204460492503131e-16
const m___LDBL_HAS_DENORM__ = 1
const m___LDBL_HAS_INFINITY__ = 1
const m___LDBL_HAS_QUIET_NAN__ = 1
const m___LDBL_MANT_DIG__ = 53
const m___LDBL_MAX_10_EXP__ = 308
const m___LDBL_MAX_EXP__ = 1024
const m___LDBL_MAX__ = 1.7976931348623157e+308
const m___LDBL_MIN__ = 2.2250738585072014e-308
const m___LITTLE_ENDIAN__ = 1
const m___LLONG_WIDTH__ = 64
const m___LONG_LONG_MAX__ = 9223372036854775807
const m___LONG_MAX__ = 9223372036854775807
const m___LONG_WIDTH__ = 64
const m___LP64__ = 1
const m___MACH__ = 1
const m___MAC_10_0 = 1000
const m___MAC_10_1 = 1010
const m___MAC_10_10 = 101000
const m___MAC_10_10_2 = 101002
const m___MAC_10_10_3 = 101003
const m___MAC_10_11 = 101100
const m___MAC_10_11_2 = 101102
const m___MAC_10_11_3 = 101103
const m___MAC_10_11_4 = 101104
const m___MAC_10_12 = 101200
const m___MAC_10_12_1 = 101201
const m___MAC_10_12_2 = 101202
const m___MAC_10_12_4 = 101204
const m___MAC_10_13 = 101300
const m___MAC_10_13_1 = 101301
const m___MAC_10_13_2 = 101302
const m___MAC_10_13_4 = 101304
const m___MAC_10_14 = 101400
const m___MAC_10_14_1 = 101401
const m___MAC_10_14_4 = 101404
const m___MAC_10_14_5 = 101405
const m___MAC_10_14_6 = 101406
const m___MAC_10_15 = 101500
const m___MAC_10_15_1 = 101501
const m___MAC_10_15_4 = 101504
const m___MAC_10_16 = 101600
const m___MAC_10_2 = 1020
const m___MAC_10_3 = 1030
const m___MAC_10_4 = 1040
const m___MAC_10_5 = 1050
const m___MAC_10_6 = 1060
const m___MAC_10_7 = 1070
const m___MAC_10_8 = 1080
const m___MAC_10_9 = 1090
const m___MAC_11_0 = 110000
const m___MAC_11_1 = 110100
const m___MAC_11_3 = 110300
const m___MAC_11_4 = 110400
const m___MAC_11_5 = 110500
const m___MAC_11_6 = 110600
const m___MAC_12_0 = 120000
const m___MAC_12_1 = 120100
const m___MAC_12_2 = 120200
const m___MAC_12_3 = 120300
const m___MAC_12_4 = 120400
const m___MAC_12_5 = 120500
const m___MAC_12_6 = 120600
const m___MAC_12_7 = 120700
const m___MAC_13_0 = 130000
const m___MAC_13_1 = 130100
const m___MAC_13_2 = 130200
const m___MAC_13_3 = 130300
const m___MAC_13_4 = 130400
const m___MAC_13_5 = 130500
const m___MAC_13_6 = 130600
const m___MAC_14_0 = 140000
const m___MAC_14_1 = 140100
const m___MAC_14_2 = 140200
const m___MAC_14_3 = 140300
const m___MAC_14_4 = 140400
const m___MAC_14_5 = 140500
const m___MAC_15_0 = 150000
const m___MAC_15_1 = 150100
const m___MAC_15_2 = 150200
const m___MAC_OS_X_VERSION_MAX_ALLOWED = "__MAC_15_2"
const m___MAC_OS_X_VERSION_MIN_REQUIRED = "__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__"
const m___MMX__ = 1
const m___NO_INLINE__ = 1
const m___NO_MATH_ERRNO__ = 1
const m___NO_MATH_INLINES = 1
const m___OBJC_BOOL_IS_BOOL = 0
const m___OPENCL_MEMORY_SCOPE_ALL_SVM_DEVICES = 3
const m___OPENCL_MEMORY_SCOPE_DEVICE = 2
const m___OPENCL_MEMORY_SCOPE_SUB_GROUP = 4
const m___OPENCL_MEMORY_SCOPE_WORK_GROUP = 1
const m___OPENCL_MEMORY_SCOPE_WORK_ITEM = 0
const m___ORDER_BIG_ENDIAN__ = 4321
const m___ORDER_LITTLE_ENDIAN__ = 1234
const m___ORDER_PDP_ENDIAN__ = 3412
const m___PIC__ = 2
const m___POINTER_WIDTH__ = 64
const m___PRAGMA_REDEFINE_EXTNAME = 1
const m___PRETTY_FUNCTION__ = "__func__"
const m___PTHREAD_ATTR_SIZE__ = 56
const m___PTHREAD_CONDATTR_SIZE__ = 8
const m___PTHREAD_COND_SIZE__ = 40
const m___PTHREAD_MUTEXATTR_SIZE__ = 8
const m___PTHREAD_MUTEX_SIZE__ = 56
const m___PTHREAD_ONCE_SIZE__ = 8
const m___PTHREAD_RWLOCKATTR_SIZE__ = 16
const m___PTHREAD_RWLOCK_SIZE__ = 192
const m___PTHREAD_SIZE__ = 8176
const m___PTRDIFF_FMTd__ = "ld"
const m___PTRDIFF_FMTi__ = "li"
const m___PTRDIFF_MAX__ = 9223372036854775807
const m___PTRDIFF_WIDTH__ = 64
const m___SCHAR_MAX__ = 127
const m___SEG_FS = 1
const m___SEG_GS = 1
const m___SHRT_MAX__ = 32767
const m___SHRT_WIDTH__ = 16
const m___SIG_ATOMIC_MAX__ = 2147483647
const m___SIG_ATOMIC_WIDTH__ = 32
const m___SIZEOF_DOUBLE__ = 8
const m___SIZEOF_FLOAT__ = 4
const m___SIZEOF_INT128__ = 16
const m___SIZEOF_INT__ = 4
const m___SIZEOF_LONG_DOUBLE__ = 8
const m___SIZEOF_LONG_LONG__ = 8
const m___SIZEOF_LONG__ = 8
const m___SIZEOF_POINTER__ = 8
const m___SIZEOF_PTRDIFF_T__ = 8
const m___SIZEOF_SHORT__ = 2
const m___SIZEOF_SIZE_T__ = 8
const m___SIZEOF_WCHAR_T__ = 4
const m___SIZEOF_WINT_T__ = 4
const m___SIZE_FMTX__ = "lX"
const m___SIZE_FMTo__ = "lo"
const m___SIZE_FMTu__ = "lu"
const m___SIZE_FMTx__ = "lx"
const m___SIZE_MAX__ = 18446744073709551615
const m___SIZE_WIDTH__ = 64
const m___SSE2_MATH__ = 1
const m___SSE2__ = 1
const m___SSE3__ = 1
const m___SSE4_1__ = 1
const m___SSE_MATH__ = 1
const m___SSE__ = 1
const m___SSP__ = 1
const m___SSSE3__ = 1
const m___STDC_HOSTED__ = 1
const m___STDC_NO_THREADS__ = 1
const m___STDC_UTF_16__ = 1
const m___STDC_UTF_32__ = 1
const m___STDC_VERSION__ = 201710
const m___STDC_WANT_IEC_60559_ATTRIBS_EXT__ = 1
const m___STDC_WANT_IEC_60559_BFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_DFP_EXT__ = 1
const m___STDC_WANT_IEC_60559_FUNCS_EXT__ = 1
const m___STDC_WANT_IEC_60559_TYPES_EXT__ = 1
const m___STDC_WANT_LIB_EXT1__ = 1
const m___STDC_WANT_LIB_EXT2__ = 1
const m___STDC_WANT_MATH_SPEC_FUNCS__ = 1
const m___STDC__ = 1
const m___TVOS_10_0 = 100000
const m___TVOS_10_0_1 = 100001
const m___TVOS_10_1 = 100100
const m___TVOS_10_2 = 100200
const m___TVOS_11_0 = 110000
const m___TVOS_11_1 = 110100
const m___TVOS_11_2 = 110200
const m___TVOS_11_3 = 110300
const m___TVOS_11_4 = 110400
const m___TVOS_12_0 = 120000
const m___TVOS_12_1 = 120100
const m___TVOS_12_2 = 120200
const m___TVOS_12_3 = 120300
const m___TVOS_12_4 = 120400
const m___TVOS_13_0 = 130000
const m___TVOS_13_2 = 130200
const m___TVOS_13_3 = 130300
const m___TVOS_13_4 = 130400
const m___TVOS_14_0 = 140000
const m___TVOS_14_1 = 140100
const m___TVOS_14_2 = 140200
const m___TVOS_14_3 = 140300
const m___TVOS_14_5 = 140500
const m___TVOS_14_6 = 140600
const m___TVOS_14_7 = 140700
const m___TVOS_15_0 = 150000
const m___TVOS_15_1 = 150100
const m___TVOS_15_2 = 150200
const m___TVOS_15_3 = 150300
const m___TVOS_15_4 = 150400
const m___TVOS_15_5 = 150500
const m___TVOS_15_6 = 150600
const m___TVOS_16_0 = 160000
const m___TVOS_16_1 = 160100
const m___TVOS_16_2 = 160200
const m___TVOS_16_3 = 160300
const m___TVOS_16_4 = 160400
const m___TVOS_16_5 = 160500
const m___TVOS_16_6 = 160600
const m___TVOS_17_0 = 170000
const m___TVOS_17_1 = 170100
const m___TVOS_17_2 = 170200
const m___TVOS_17_3 = 170300
const m___TVOS_17_4 = 170400
const m___TVOS_17_5 = 170500
const m___TVOS_18_0 = 180000
const m___TVOS_18_1 = 180100
const m___TVOS_18_2 = 180200
const m___TVOS_9_0 = 90000
const m___TVOS_9_1 = 90100
const m___TVOS_9_2 = 90200
const m___UINT16_FMTX__ = "hX"
const m___UINT16_FMTo__ = "ho"
const m___UINT16_FMTu__ = "hu"
const m___UINT16_FMTx__ = "hx"
const m___UINT16_MAX__ = 65535
const m___UINT32_C_SUFFIX__ = "U"
const m___UINT32_FMTX__ = "X"
const m___UINT32_FMTo__ = "o"
const m___UINT32_FMTu__ = "u"
const m___UINT32_FMTx__ = "x"
const m___UINT32_MAX__ = 4294967295
const m___UINT64_C_SUFFIX__ = "ULL"
const m___UINT64_FMTX__ = "llX"
const m___UINT64_FMTo__ = "llo"
const m___UINT64_FMTu__ = "llu"
const m___UINT64_FMTx__ = "llx"
const m___UINT64_MAX__ = "18446744073709551615U"
const m___UINT8_FMTX__ = "hhX"
const m___UINT8_FMTo__ = "hho"
const m___UINT8_FMTu__ = "hhu"
const m___UINT8_FMTx__ = "hhx"
const m___UINT8_MAX__ = 255
const m___UINTMAX_C_SUFFIX__ = "UL"
const m___UINTMAX_FMTX__ = "lX"
const m___UINTMAX_FMTo__ = "lo"
const m___UINTMAX_FMTu__ = "lu"
const m___UINTMAX_FMTx__ = "lx"
const m___UINTMAX_MAX__ = 18446744073709551615
const m___UINTMAX_WIDTH__ = 64
const m___UINTPTR_FMTX__ = "lX"
const m___UINTPTR_FMTo__ = "lo"
const m___UINTPTR_FMTu__ = "lu"
const m___UINTPTR_FMTx__ = "lx"
const m___UINTPTR_MAX__ = 18446744073709551615
const m___UINTPTR_WIDTH__ = 64
const m___UINT_FAST16_FMTX__ = "hX"
const m___UINT_FAST16_FMTo__ = "ho"
const m___UINT_FAST16_FMTu__ = "hu"
const m___UINT_FAST16_FMTx__ = "hx"
const m___UINT_FAST16_MAX__ = 65535
const m___UINT_FAST32_FMTX__ = "X"
const m___UINT_FAST32_FMTo__ = "o"
const m___UINT_FAST32_FMTu__ = "u"
const m___UINT_FAST32_FMTx__ = "x"
const m___UINT_FAST32_MAX__ = 4294967295
const m___UINT_FAST64_FMTX__ = "llX"
const m___UINT_FAST64_FMTo__ = "llo"
const m___UINT_FAST64_FMTu__ = "llu"
const m___UINT_FAST64_FMTx__ = "llx"
const m___UINT_FAST64_MAX__ = "18446744073709551615U"
const m___UINT_FAST8_FMTX__ = "hhX"
const m___UINT_FAST8_FMTo__ = "hho"
const m___UINT_FAST8_FMTu__ = "hhu"
const m___UINT_FAST8_FMTx__ = "hhx"
const m___UINT_FAST8_MAX__ = 255
const m___UINT_LEAST16_FMTX__ = "hX"
const m___UINT_LEAST16_FMTo__ = "ho"
const m___UINT_LEAST16_FMTu__ = "hu"
const m___UINT_LEAST16_FMTx__ = "hx"
const m___UINT_LEAST16_MAX__ = 65535
const m___UINT_LEAST32_FMTX__ = "X"
const m___UINT_LEAST32_FMTo__ = "o"
const m___UINT_LEAST32_FMTu__ = "u"
const m___UINT_LEAST32_FMTx__ = "x"
const m___UINT_LEAST32_MAX__ = 4294967295
const m___UINT_LEAST64_FMTX__ = "llX"
const m___UINT_LEAST64_FMTo__ = "llo"
const m___UINT_LEAST64_FMTu__ = "llu"
const m___UINT_LEAST64_FMTx__ = "llx"
const m___UINT_LEAST64_MAX = "UINT64_MAX"
const m___UINT_LEAST64_MAX__ = "18446744073709551615U"
const m___UINT_LEAST8_FMTX__ = "hhX"
const m___UINT_LEAST8_FMTo__ = "hho"
const m___UINT_LEAST8_FMTu__ = "hhu"
const m___UINT_LEAST8_FMTx__ = "hhx"
const m___UINT_LEAST8_MAX__ = 255
const m___USER_LABEL_PREFIX__ = "_"
const m___VERSION__ = "Apple LLVM 16.0.0 (clang-1600.0.26.6)"
const m___VISIONOS_1_0 = 10000
const m___VISIONOS_1_1 = 10100
const m___VISIONOS_1_2 = 10200
const m___VISIONOS_2_0 = 20000
const m___VISIONOS_2_1 = 20100
const m___VISIONOS_2_2 = 20200
const m___WATCHOS_10_0 = 100000
const m___WATCHOS_10_1 = 100100
const m___WATCHOS_10_2 = 100200
const m___WATCHOS_10_3 = 100300
const m___WATCHOS_10_4 = 100400
const m___WATCHOS_10_5 = 100500
const m___WATCHOS_11_0 = 110000
const m___WATCHOS_11_1 = 110100
const m___WATCHOS_11_2 = 110200
const m___WATCHOS_1_0 = 10000
const m___WATCHOS_2_0 = 20000
const m___WATCHOS_2_1 = 20100
const m___WATCHOS_2_2 = 20200
const m___WATCHOS_3_0 = 30000
const m___WATCHOS_3_1 = 30100
const m___WATCHOS_3_1_1 = 30101
const m___WATCHOS_3_2 = 30200
const m___WATCHOS_4_0 = 40000
const m___WATCHOS_4_1 = 40100
const m___WATCHOS_4_2 = 40200
const m___WATCHOS_4_3 = 40300
const m___WATCHOS_5_0 = 50000
const m___WATCHOS_5_1 = 50100
const m___WATCHOS_5_2 = 50200
const m___WATCHOS_5_3 = 50300
const m___WATCHOS_6_0 = 60000
const m___WATCHOS_6_1 = 60100
const m___WATCHOS_6_2 = 60200
const m___WATCHOS_7_0 = 70000
const m___WATCHOS_7_1 = 70100
const m___WATCHOS_7_2 = 70200
const m___WATCHOS_7_3 = 70300
const m___WATCHOS_7_4 = 70400
const m___WATCHOS_7_5 = 70500
const m___WATCHOS_7_6 = 70600
const m___WATCHOS_8_0 = 80000
const m___WATCHOS_8_1 = 80100
const m___WATCHOS_8_3 = 80300
const m___WATCHOS_8_4 = 80400
const m___WATCHOS_8_5 = 80500
const m___WATCHOS_8_6 = 80600
const m___WATCHOS_8_7 = 80700
const m___WATCHOS_8_8 = 80800
const m___WATCHOS_9_0 = 90000
const m___WATCHOS_9_1 = 90100
const m___WATCHOS_9_2 = 90200
const m___WATCHOS_9_3 = 90300
const m___WATCHOS_9_4 = 90400
const m___WATCHOS_9_5 = 90500
const m___WATCHOS_9_6 = 90600
const m___WCHAR_MAX__ = 2147483647
const m___WCHAR_TYPE__ = "int"
const m___WCHAR_WIDTH__ = 32
const m___WINT_MAX__ = 2147483647
const m___WINT_TYPE__ = "int"
const m___WINT_WIDTH__ = 32
const m___amd64 = 1
const m___amd64__ = 1
const m___apple_build_version__ = 16000026
const m___clang__ = 1
const m___clang_literal_encoding__ = "UTF-8"
const m___clang_major__ = 16
const m___clang_minor__ = 0
const m___clang_patchlevel__ = 0
const m___clang_version__ = "16.0.0 (clang-1600.0.26.6)"
const m___clang_wide_literal_encoding__ = "UTF-32"
const m___code_model_small__ = 1
const m___const = "const"
const m___core2 = 1
const m___core2__ = 1
const m___has_ptrcheck = 0
const m___has_safe_buffers = 0
const m___header_inline = "inline"
const m___llvm__ = 1
const m___nonnull = "_Nonnull"
const m___null_unspecified = "_Null_unspecified"
const m___nullable = "_Nullable"
const m___pic__ = 2
const m___restrict = "restrict"
const m___restrict_arr = "restrict"
const m___signed = "signed"
const m___tune_core2__ = 1
const m___volatile = "volatile"
const m___x86_64 = 1
const m___x86_64__ = 1

type t__builtin_va_list = uintptr

type t__predefined_size_t = uint64

type t__predefined_wchar_t = int32

type t__predefined_ptrdiff_t = int64

type t__int8_t = int8

type t__uint8_t = uint8

type t__int16_t = int16

type t__uint16_t = uint16

type t__int32_t = int32

type t__uint32_t = uint32

type t__int64_t = int64

type t__uint64_t = uint64

type t__darwin_intptr_t = int64

type t__darwin_natural_t = uint32

type t__darwin_ct_rune_t = int32

type t__mbstate_t = struct {
	F_mbstateL  [0]int64
	F__mbstate8 [128]int8
}

type t__darwin_mbstate_t = struct {
	F_mbstateL  [0]int64
	F__mbstate8 [128]int8
}

type t__darwin_ptrdiff_t = int64

type t__darwin_size_t = uint64

type t__darwin_va_list = uintptr

type t__darwin_wchar_t = int32

type t__darwin_rune_t = int32

type t__darwin_wint_t = int32

type t__darwin_clock_t = uint64

type t__darwin_socklen_t = uint32

type t__darwin_ssize_t = int64

type t__darwin_time_t = int64

type Tint8_t = int8

type Tint16_t = int16

type Tint32_t = int32

type Tint64_t = int64

type Tu_int8_t = uint8

type Tu_int16_t = uint16

type Tu_int32_t = uint32

type Tu_int64_t = uint64

type Tregister_t = int64

type Tintptr_t = int64

type Tuintptr_t = uint64

type Tuser_addr_t = uint64

type Tuser_size_t = uint64

type Tuser_ssize_t = int64

type Tuser_long_t = int64

type Tuser_ulong_t = uint64

type Tuser_time_t = int64

type Tuser_off_t = int64

type Tsyscall_arg_t = uint64

type t__darwin_blkcnt_t = int64

type t__darwin_blksize_t = int32

type t__darwin_dev_t = int32

type t__darwin_fsblkcnt_t = uint32

type t__darwin_fsfilcnt_t = uint32

type t__darwin_gid_t = uint32

type t__darwin_id_t = uint32

type t__darwin_ino64_t = uint64

type t__darwin_ino_t = uint64

type t__darwin_mach_port_name_t = uint32

type t__darwin_mach_port_t = uint32

type t__darwin_mode_t = uint16

type t__darwin_off_t = int64

type t__darwin_pid_t = int32

type t__darwin_sigset_t = uint32

type t__darwin_suseconds_t = int32

type t__darwin_uid_t = uint32

type t__darwin_useconds_t = uint32

type t__darwin_uuid_t = [16]uint8

type t__darwin_uuid_string_t = [37]int8

type t__darwin_pthread_handler_rec = struct {
	F__routine uintptr
	F__arg     uintptr
	F__next    uintptr
}

type T_opaque_pthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T_opaque_pthread_cond_t = struct {
	F__sig    int64
	F__opaque [40]int8
}

type T_opaque_pthread_condattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_mutex_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type T_opaque_pthread_mutexattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_once_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type T_opaque_pthread_rwlock_t = struct {
	F__sig    int64
	F__opaque [192]int8
}

type T_opaque_pthread_rwlockattr_t = struct {
	F__sig    int64
	F__opaque [16]int8
}

type T_opaque_pthread_t = struct {
	F__sig           int64
	F__cleanup_stack uintptr
	F__opaque        [8176]int8
}

type t__darwin_pthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type t__darwin_pthread_cond_t = struct {
	F__sig    int64
	F__opaque [40]int8
}

type t__darwin_pthread_condattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type t__darwin_pthread_key_t = uint64

type t__darwin_pthread_mutex_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type t__darwin_pthread_mutexattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type t__darwin_pthread_once_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type t__darwin_pthread_rwlock_t = struct {
	F__sig    int64
	F__opaque [192]int8
}

type t__darwin_pthread_rwlockattr_t = struct {
	F__sig    int64
	F__opaque [16]int8
}

type t__darwin_pthread_t = uintptr

type Tu_char = uint8

type Tu_short = uint16

type Tu_int = uint32

type Tu_long = uint64

type Tushort = uint16

type Tuint = uint32

type Tu_quad_t = uint64

type Tquad_t = int64

type Tqaddr_t = uintptr

type Tcaddr_t = uintptr

type Tdaddr_t = int32

type Tdev_t = int32

type Tfixpt_t = uint32

type Tblkcnt_t = int64

type Tblksize_t = int32

type Tgid_t = uint32

type Tin_addr_t = uint32

type Tin_port_t = uint16

type Tino_t = uint64

type Tino64_t = uint64

type Tkey_t = int32

type Tmode_t = uint16

type Tnlink_t = uint16

type Tid_t = uint32

type Tpid_t = int32

type Toff_t = int64

type Tsegsz_t = int32

type Tswblk_t = int32

type Tuid_t = uint32

type Tclock_t = uint64

type Tsize_t = uint64

type Tssize_t = int64

type Ttime_t = int64

type Tuseconds_t = uint32

type Tsuseconds_t = int32

type Trsize_t = uint64

type Terrno_t = int32

type Tfd_set = struct {
	Ffds_bits [32]t__int32_t
}

type Tfd_mask = int32

type Tpthread_attr_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type Tpthread_cond_t = struct {
	F__sig    int64
	F__opaque [40]int8
}

type Tpthread_condattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type Tpthread_mutex_t = struct {
	F__sig    int64
	F__opaque [56]int8
}

type Tpthread_mutexattr_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type Tpthread_once_t = struct {
	F__sig    int64
	F__opaque [8]int8
}

type Tpthread_rwlock_t = struct {
	F__sig    int64
	F__opaque [192]int8
}

type Tpthread_rwlockattr_t = struct {
	F__sig    int64
	F__opaque [16]int8
}

type Tpthread_t = uintptr

type Tpthread_key_t = uint64

type Tfsblkcnt_t = uint32

type Tfsfilcnt_t = uint32

type Tuint64_t = uint64

type Tint_least64_t = int64

type Tuint_least64_t = uint64

type Tint_fast64_t = int64

type Tuint_fast64_t = uint64

type Tuint32_t = uint32

type Tint_least32_t = int32

type Tuint_least32_t = uint32

type Tint_fast32_t = int32

type Tuint_fast32_t = uint32

type Tuint16_t = uint16

type Tint_least16_t = int16

type Tuint_least16_t = uint16

type Tint_fast16_t = int16

type Tuint_fast16_t = uint16

type Tuint8_t = uint8

type Tint_least8_t = int8

type Tuint_least8_t = uint8

type Tint_fast8_t = int8

type Tuint_fast8_t = uint8

type Tintmax_t = int64

type Tuintmax_t = uint64

type TMD2_CTX = struct {
	Fi Tuint32_t
	FC [16]uint8
	FX [48]uint8
}

type TMD2Context = TMD2_CTX

type t__darwin_nl_item = int32

type t__darwin_wctrans_t = int32

type t__darwin_wctype_t = uint32

/* Security checking functions.  */
/*
 * Copyright (c) 2017, 2023 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/*
 * Copyright (c) 2000-2018 Apple Inc. All rights reserved.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. The rights granted to you under the License
 * may not be used to create, or enable the creation or redistribution of,
 * unlawful or unlicensed copies of an Apple operating system, or to
 * circumvent, violate, or enable the circumvention or violation of, any
 * terms of an Apple operating system software license agreement.
 *
 * Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_END@
 */
/* Copyright 1995 NeXT Computer, Inc. All rights reserved. */
/*
 * Copyright (c) 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Berkeley Software Design, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)cdefs.h	8.8 (Berkeley) 1/9/95
 */

/*
 * Copyright (c) 2007-2016 by Apple Inc.. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/*
 * Copyright (c) 2007, 2008 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/*
 * Copyright (c) 2004, 2008, 2009 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/* bcopy and bzero */

/* Removed in Issue 7 */

/* void	bcopy(const void *src, void *dst, size_t len) */

/* void	bzero(void *s, size_t n) */

/* Security checking functions.  */
/*
 * Copyright (c) 2007,2017,2023 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/*
 * Copyright (c) 2000-2018 Apple Inc. All rights reserved.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. The rights granted to you under the License
 * may not be used to create, or enable the creation or redistribution of,
 * unlawful or unlicensed copies of an Apple operating system, or to
 * circumvent, violate, or enable the circumvention or violation of, any
 * terms of an Apple operating system software license agreement.
 *
 * Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_OSREFERENCE_LICENSE_HEADER_END@
 */
/* Copyright 1995 NeXT Computer, Inc. All rights reserved. */
/*
 * Copyright (c) 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * Berkeley Software Design, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *	This product includes software developed by the University of
 *	California, Berkeley and its contributors.
 * 4. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	@(#)cdefs.h	8.8 (Berkeley) 1/9/95
 */

/*
 * Copyright (c) 2007-2016 by Apple Inc.. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/*
 * Copyright (c) 2007, 2008 Apple Inc. All rights reserved.
 *
 * @APPLE_LICENSE_HEADER_START@
 *
 * This file contains Original Code and/or Modifications of Original Code
 * as defined in and that are subject to the Apple Public Source License
 * Version 2.0 (the 'License'). You may not use this file except in
 * compliance with the License. Please obtain a copy of the License at
 * http://www.opensource.apple.com/apsl/ and read it before using this
 * file.
 *
 * The Original Code and all software distributed under the License are
 * distributed on an 'AS IS' basis, WITHOUT WARRANTY OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, AND APPLE HEREBY DISCLAIMS ALL SUCH WARRANTIES,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, QUIET ENJOYMENT OR NON-INFRINGEMENT.
 * Please see the License for the specific language governing rights and
 * limitations under the License.
 *
 * @APPLE_LICENSE_HEADER_END@
 */

/* <rdar://problem/12622659> */

/* memccpy, memcpy, mempcpy, memmove, memset, strcpy, strlcpy, stpcpy,
   strncpy, stpncpy, strcat, strlcat, and strncat */

/* void *memcpy(void *dst, const void *src, size_t n) */

/* void *memmove(void *dst, const void *src, size_t len) */

/* void *memset(void *b, int c, size_t len) */

/* char *strcpy(char *dst, const char *src) */

/* char *stpcpy(char *dst, const char *src) */

/* char *stpncpy(char *dst, const char *src, size_t n) */

/* char *strncpy(char *dst, const char *src, size_t n) */

/* char *strcat(char *s1, const char *s2) */

/* char *strncat(char *s1, const char *s2, size_t n) */

// C documentation
//
//	/* cut-n-pasted from rfc1319 */
var _S = [256]uint8{
	0:   uint8(41),
	1:   uint8(46),
	2:   uint8(67),
	3:   uint8(201),
	4:   uint8(162),
	5:   uint8(216),
	6:   uint8(124),
	7:   uint8(1),
	8:   uint8(61),
	9:   uint8(54),
	10:  uint8(84),
	11:  uint8(161),
	12:  uint8(236),
	13:  uint8(240),
	14:  uint8(6),
	15:  uint8(19),
	16:  uint8(98),
	17:  uint8(167),
	18:  uint8(5),
	19:  uint8(243),
	20:  uint8(192),
	21:  uint8(199),
	22:  uint8(115),
	23:  uint8(140),
	24:  uint8(152),
	25:  uint8(147),
	26:  uint8(43),
	27:  uint8(217),
	28:  uint8(188),
	29:  uint8(76),
	30:  uint8(130),
	31:  uint8(202),
	32:  uint8(30),
	33:  uint8(155),
	34:  uint8(87),
	35:  uint8(60),
	36:  uint8(253),
	37:  uint8(212),
	38:  uint8(224),
	39:  uint8(22),
	40:  uint8(103),
	41:  uint8(66),
	42:  uint8(111),
	43:  uint8(24),
	44:  uint8(138),
	45:  uint8(23),
	46:  uint8(229),
	47:  uint8(18),
	48:  uint8(190),
	49:  uint8(78),
	50:  uint8(196),
	51:  uint8(214),
	52:  uint8(218),
	53:  uint8(158),
	54:  uint8(222),
	55:  uint8(73),
	56:  uint8(160),
	57:  uint8(251),
	58:  uint8(245),
	59:  uint8(142),
	60:  uint8(187),
	61:  uint8(47),
	62:  uint8(238),
	63:  uint8(122),
	64:  uint8(169),
	65:  uint8(104),
	66:  uint8(121),
	67:  uint8(145),
	68:  uint8(21),
	69:  uint8(178),
	70:  uint8(7),
	71:  uint8(63),
	72:  uint8(148),
	73:  uint8(194),
	74:  uint8(16),
	75:  uint8(137),
	76:  uint8(11),
	77:  uint8(34),
	78:  uint8(95),
	79:  uint8(33),
	80:  uint8(128),
	81:  uint8(127),
	82:  uint8(93),
	83:  uint8(154),
	84:  uint8(90),
	85:  uint8(144),
	86:  uint8(50),
	87:  uint8(39),
	88:  uint8(53),
	89:  uint8(62),
	90:  uint8(204),
	91:  uint8(231),
	92:  uint8(191),
	93:  uint8(247),
	94:  uint8(151),
	95:  uint8(3),
	96:  uint8(255),
	97:  uint8(25),
	98:  uint8(48),
	99:  uint8(179),
	100: uint8(72),
	101: uint8(165),
	102: uint8(181),
	103: uint8(209),
	104: uint8(215),
	105: uint8(94),
	106: uint8(146),
	107: uint8(42),
	108: uint8(172),
	109: uint8(86),
	110: uint8(170),
	111: uint8(198),
	112: uint8(79),
	113: uint8(184),
	114: uint8(56),
	115: uint8(210),
	116: uint8(150),
	117: uint8(164),
	118: uint8(125),
	119: uint8(182),
	120: uint8(118),
	121: uint8(252),
	122: uint8(107),
	123: uint8(226),
	124: uint8(156),
	125: uint8(116),
	126: uint8(4),
	127: uint8(241),
	128: uint8(69),
	129: uint8(157),
	130: uint8(112),
	131: uint8(89),
	132: uint8(100),
	133: uint8(113),
	134: uint8(135),
	135: uint8(32),
	136: uint8(134),
	137: uint8(91),
	138: uint8(207),
	139: uint8(101),
	140: uint8(230),
	141: uint8(45),
	142: uint8(168),
	143: uint8(2),
	144: uint8(27),
	145: uint8(96),
	146: uint8(37),
	147: uint8(173),
	148: uint8(174),
	149: uint8(176),
	150: uint8(185),
	151: uint8(246),
	152: uint8(28),
	153: uint8(70),
	154: uint8(97),
	155: uint8(105),
	156: uint8(52),
	157: uint8(64),
	158: uint8(126),
	159: uint8(15),
	160: uint8(85),
	161: uint8(71),
	162: uint8(163),
	163: uint8(35),
	164: uint8(221),
	165: uint8(81),
	166: uint8(175),
	167: uint8(58),
	168: uint8(195),
	169: uint8(92),
	170: uint8(249),
	171: uint8(206),
	172: uint8(186),
	173: uint8(197),
	174: uint8(234),
	175: uint8(38),
	176: uint8(44),
	177: uint8(83),
	178: uint8(13),
	179: uint8(110),
	180: uint8(133),
	181: uint8(40),
	182: uint8(132),
	183: uint8(9),
	184: uint8(211),
	185: uint8(223),
	186: uint8(205),
	187: uint8(244),
	188: uint8(65),
	189: uint8(129),
	190: uint8(77),
	191: uint8(82),
	192: uint8(106),
	193: uint8(220),
	194: uint8(55),
	195: uint8(200),
	196: uint8(108),
	197: uint8(193),
	198: uint8(171),
	199: uint8(250),
	200: uint8(36),
	201: uint8(225),
	202: uint8(123),
	203: uint8(8),
	204: uint8(12),
	205: uint8(189),
	206: uint8(177),
	207: uint8(74),
	208: uint8(120),
	209: uint8(136),
	210: uint8(149),
	211: uint8(139),
	212: uint8(227),
	213: uint8(99),
	214: uint8(232),
	215: uint8(109),
	216: uint8(233),
	217: uint8(203),
	218: uint8(213),
	219: uint8(254),
	220: uint8(59),
	222: uint8(29),
	223: uint8(57),
	224: uint8(242),
	225: uint8(239),
	226: uint8(183),
	227: uint8(14),
	228: uint8(102),
	229: uint8(88),
	230: uint8(208),
	231: uint8(228),
	232: uint8(166),
	233: uint8(119),
	234: uint8(114),
	235: uint8(248),
	236: uint8(235),
	237: uint8(117),
	238: uint8(75),
	239: uint8(10),
	240: uint8(49),
	241: uint8(68),
	242: uint8(80),
	243: uint8(180),
	244: uint8(143),
	245: uint8(237),
	246: uint8(31),
	247: uint8(26),
	248: uint8(219),
	249: uint8(153),
	250: uint8(141),
	251: uint8(51),
	252: uint8(159),
	253: uint8(17),
	254: uint8(131),
	255: uint8(20),
}

// C documentation
//
//	/* cut-n-pasted from rfc1319 */
var _pad = [17]uintptr{
	0:  __ccgo_ts,
	1:  __ccgo_ts + 1,
	2:  __ccgo_ts + 3,
	3:  __ccgo_ts + 6,
	4:  __ccgo_ts + 10,
	5:  __ccgo_ts + 15,
	6:  __ccgo_ts + 21,
	7:  __ccgo_ts + 28,
	8:  __ccgo_ts + 36,
	9:  __ccgo_ts + 45,
	10: __ccgo_ts + 55,
	11: __ccgo_ts + 66,
	12: __ccgo_ts + 78,
	13: __ccgo_ts + 91,
	14: __ccgo_ts + 105,
	15: __ccgo_ts + 120,
	16: __ccgo_ts + 136,
}

func XMD2Init(tls *libc.TLS, context uintptr) {
	(*TMD2_CTX)(unsafe.Pointer(context)).Fi = uint32(16)
	libc.X__builtin___memset_chk(tls, context+4, 0, uint64(16), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+20, 0, uint64(48), ^t__predefined_size_t(0))
}

func XMD2Update(tls *libc.TLS, context uintptr, input uintptr, inputLen uint32) {
	var idx, piece uint32
	var p2 uintptr
	_, _, _ = idx, piece, p2
	idx = uint32(0)
	for {
		if !(idx < inputLen) {
			break
		}
		piece = uint32(32) - (*TMD2_CTX)(unsafe.Pointer(context)).Fi
		if inputLen-idx < piece {
			piece = inputLen - idx
		}
		libc.X__builtin___memcpy_chk(tls, context+20+uintptr((*TMD2_CTX)(unsafe.Pointer(context)).Fi), input+uintptr(idx), uint64(piece), ^t__predefined_size_t(0))
		p2 = context
		*(*Tuint32_t)(unsafe.Pointer(p2)) += piece
		if *(*Tuint32_t)(unsafe.Pointer(p2)) == uint32(32) {
			XMD2Transform(tls, context)
		} /* resets i */
		goto _1
	_1:
		;
		idx += piece
	}
}

func XMD2Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var padlen uint32
	_ = padlen
	/* padlen should be 1..16 */
	padlen = uint32(32) - (*TMD2_CTX)(unsafe.Pointer(context)).Fi
	/* add padding */
	XMD2Update(tls, context, _pad[padlen], padlen)
	/* add checksum */
	XMD2Update(tls, context, context+4, libc.Uint32FromInt64(16))
	/* copy out final digest */
	libc.X__builtin___memcpy_chk(tls, digest, context+20, libc.Uint64FromInt32(16), ^t__predefined_size_t(0))
	/* reset the context */
	XMD2Init(tls, context)
}

// C documentation
//
//	/*static*/
func XMD2Transform(tls *libc.TLS, context uintptr) {
	var j, k, l, t, v4 Tuint32_t
	var v6 uint8
	var p2 uintptr
	_, _, _, _, _, _, _ = j, k, l, t, v4, v6, p2
	/* set block "3" and update "checksum" */
	l = uint32(*(*uint8)(unsafe.Pointer(context + 4 + 15)))
	j = libc.Uint32FromInt32(0)
	for {
		if !(j < uint32(16)) {
			break
		}
		*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(32)+j))) = libc.Uint8FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(j)))) ^ libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(16)+j)))))
		p2 = context + 4 + uintptr(j)
		*(*uint8)(unsafe.Pointer(p2)) = uint8(int32(*(*uint8)(unsafe.Pointer(p2))) ^ libc.Int32FromUint8(_S[uint32(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(uint32(16)+j))))^l]))
		l = uint32(*(*uint8)(unsafe.Pointer(p2)))
		goto _1
	_1:
		;
		j++
	}
	/* mangle input block */
	v4 = libc.Uint32FromInt32(0)
	j = v4
	t = v4
	for {
		if !(j < uint32(18)) {
			break
		}
		k = uint32(0)
		for {
			if !(k < uint32(48)) {
				break
			}
			v6 = libc.Uint8FromInt32(libc.Int32FromUint8(*(*uint8)(unsafe.Pointer(context + 20 + uintptr(k)))) ^ libc.Int32FromUint8(_S[t]))
			*(*uint8)(unsafe.Pointer(context + 20 + uintptr(k))) = v6
			t = uint32(v6)
			goto _5
		_5:
			;
			k++
		}
		goto _3
	_3:
		;
		t = (t + j) % uint32(256)
		j++
	}
	/* reset input pointer */
	(*TMD2_CTX)(unsafe.Pointer(context)).Fi = uint32(16)
}

const m_MD4_BLOCK_LENGTH = 64
const m_MD4_DIGEST_LENGTH = 16

type TMD4_CTX = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

type TMD4Context = TMD4_CTX

var _PADDING = [64]Tuint8_t{
	0: uint8(0x80),
}

// C documentation
//
//	/*
//	 * Start MD4 accumulation.
//	 * Set bit count to 0 and buffer to mysterious initialization constants.
//	 */
func XMD4Init(tls *libc.TLS, ctx uintptr) {
	(*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xefcdab89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98badcfe)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
}

// C documentation
//
//	/*
//	 * Update context to reflect the concatenation of another buffer full
//	 * of bytes.
//	 */
func XMD4Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need Tsize_t
	_, _ = have, need
	/* Check how many bytes we already have and how many more we need. */
	have = (*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	need = uint64(m_MD4_BLOCK_LENGTH) - have
	/* Update bitcount */
	*(*Tuint64_t)(unsafe.Pointer(ctx + 16)) += len1 << int32(3)
	if len1 >= need {
		if have != uint64(0) {
			libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, need, ^t__predefined_size_t(0))
			XMD4Transform(tls, ctx, ctx+24)
			input += uintptr(need)
			len1 -= need
			have = uint64(0)
		}
		/* Process data in MD4_BLOCK_LENGTH-byte chunks. */
		for len1 >= uint64(m_MD4_BLOCK_LENGTH) {
			XMD4Transform(tls, ctx, input)
			input += uintptr(m_MD4_BLOCK_LENGTH)
			len1 -= uint64(m_MD4_BLOCK_LENGTH)
		}
	}
	/* Handle any remaining bytes of data. */
	if len1 != uint64(0) {
		libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, len1, ^t__predefined_size_t(0))
	}
}

// C documentation
//
//	/*
//	 * Pad pad to 64-byte boundary with the bit pattern
//	 * 1 0* (64-bit count of bits processed, MSB-first)
//	 */
func XMD4Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* count at bp+0 */ [8]Tuint8_t
	_ = padlen
	/* Convert count to 8 bytes in little endian order. */
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount)
	/* Pad out to 56 mod 64. */
	padlen = uint64(m_MD4_BLOCK_LENGTH) - (*TMD4_CTX)(unsafe.Pointer(ctx)).Fcount>>libc.Int32FromInt32(3)&libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_MD4_BLOCK_LENGTH)
	}
	XMD4Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	XMD4Update(tls, ctx, bp, uint64(8))
}

// C documentation
//
//	/*
//	 * Final wrapup--call MD4Pad, fill in digest and zero out ctx.
//	 */
func XMD4Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	XMD4Pad(tls, ctx)
	i = 0
	for {
		if !(i < int32(4)) {
			break
		}
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, ctx, 0, uint64(88), ^t__predefined_size_t(0))
}

/* The three core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */

/* This is the central step in the MD4 algorithm. */

// C documentation
//
//	/*
//	 * The core of the MD4 algorithm, this alters an existing MD4 hash to
//	 * reflect the addition of 16 longwords of new data.  MD4Update blocks
//	 * the data and converts bytes into longwords for this routine.
//	 */
func XMD4Transform(tls *libc.TLS, state uintptr, block uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, b, c, d Tuint32_t
	var _ /* in at bp+0 */ [16]Tuint32_t
	_, _, _, _ = a, b, c, d
	libc.X__builtin___memcpy_chk(tls, bp, block, uint64(64), ^t__predefined_size_t(0))
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]
	d = d<<libc.Int32FromInt32(7) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]
	b = b<<libc.Int32FromInt32(19) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))
	a += b&c | b&d | c&d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b&c | b&d | c&d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0x5a827999)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a&b | a&c | b&c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x5a827999)
	d = d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	c += d&a | d&b | a&b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x5a827999)
	c = c<<libc.Int32FromInt32(9) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	b += c&d | c&a | d&a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x5a827999)
	b = b<<libc.Int32FromInt32(13) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0x6ed9eba1)
	a = a<<libc.Int32FromInt32(3) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(3))
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x6ed9eba1)
	d = d<<libc.Int32FromInt32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x6ed9eba1)
	c = c<<libc.Int32FromInt32(11) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x6ed9eba1)
	b = b<<libc.Int32FromInt32(15) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
}

const m_MD5_BLOCK_LENGTH = 64
const m_MD5_DIGEST_LENGTH = 16

type TMD5_CTX = struct {
	Fstate  [4]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

type TMD5Context = TMD5_CTX

/* Avoid polluting the namespace. Even though this makes this usage
 * implementation-specific, defining it unconditionally should not be
 * a problem, and better than possibly breaking unexpecting code. */

var _PADDING1 = [64]Tuint8_t{
	0: uint8(0x80),
}

// C documentation
//
//	/*
//	 * Start MD5 accumulation.  Set bit count to 0 and buffer to mysterious
//	 * initialization constants.
//	 */
func XMD5Init(tls *libc.TLS, ctx uintptr) {
	(*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xefcdab89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98badcfe)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
}

// C documentation
//
//	/*
//	 * Update context to reflect the concatenation of another buffer full
//	 * of bytes.
//	 */
func XMD5Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need Tsize_t
	_, _ = have, need
	/* Check how many bytes we already have and how many more we need. */
	have = (*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	need = uint64(m_MD5_BLOCK_LENGTH) - have
	/* Update bitcount */
	*(*Tuint64_t)(unsafe.Pointer(ctx + 16)) += len1 << int32(3)
	if len1 >= need {
		if have != uint64(0) {
			libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, need, ^t__predefined_size_t(0))
			XMD5Transform(tls, ctx, ctx+24)
			input += uintptr(need)
			len1 -= need
			have = uint64(0)
		}
		/* Process data in MD5_BLOCK_LENGTH-byte chunks. */
		for len1 >= uint64(m_MD5_BLOCK_LENGTH) {
			XMD5Transform(tls, ctx, input)
			input += uintptr(m_MD5_BLOCK_LENGTH)
			len1 -= uint64(m_MD5_BLOCK_LENGTH)
		}
	}
	/* Handle any remaining bytes of data. */
	if len1 != uint64(0) {
		libc.X__builtin___memcpy_chk(tls, ctx+24+uintptr(have), input, len1, ^t__predefined_size_t(0))
	}
}

// C documentation
//
//	/*
//	 * Pad pad to 64-byte boundary with the bit pattern
//	 * 1 0* (64-bit count of bits processed, MSB-first)
//	 */
func XMD5Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* count at bp+0 */ [8]Tuint8_t
	_ = padlen
	/* Convert count to 8 bytes in little endian order. */
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount)
	/* Pad out to 56 mod 64. */
	padlen = uint64(m_MD5_BLOCK_LENGTH) - (*TMD5_CTX)(unsafe.Pointer(ctx)).Fcount>>libc.Int32FromInt32(3)&libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_BLOCK_LENGTH)-libc.Int32FromInt32(1))
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_MD5_BLOCK_LENGTH)
	}
	XMD5Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING1)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	XMD5Update(tls, ctx, bp, uint64(8))
}

// C documentation
//
//	/*
//	 * Final wrapup--call MD5Pad, fill in digest and zero out ctx.
//	 */
func XMD5Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	XMD5Pad(tls, ctx)
	i = 0
	for {
		if !(i < int32(4)) {
			break
		}
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
		*(*uint8)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, ctx, 0, uint64(88), ^t__predefined_size_t(0))
}

/* The four core functions - F1 is optimized somewhat */

/* #define F1(x, y, z) (x & y | ~x & z) */

/* This is the central step in the MD5 algorithm. */

// C documentation
//
//	/*
//	 * The core of the MD5 algorithm, this alters an existing MD5 hash to
//	 * reflect the addition of 16 longwords of new data.  MD5Update blocks
//	 * the data and converts bytes into longwords for this routine.
//	 */
func XMD5Transform(tls *libc.TLS, state uintptr, block uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, b, c, d Tuint32_t
	var _ /* in at bp+0 */ [16]Tuint32_t
	_, _, _, _ = a, b, c, d
	libc.X__builtin___memcpy_chk(tls, bp, block, uint64(64), ^t__predefined_size_t(0))
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xd76aa478)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xe8c7b756)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x242070db)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xc1bdceee)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xf57c0faf)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0x4787c62a)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xa8304613)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0xfd469501)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x698098d8)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x8b44f7af)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xffff5bb1)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x895cd7be)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += d ^ b&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x6b901122)
	a = a<<int32(7) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7))
	a += b
	d += c ^ a&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0xfd987193)
	d = d<<int32(12) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12))
	d += a
	c += b ^ d&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xa679438e)
	c = c<<int32(17) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(17))
	c += d
	b += a ^ c&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x49b40821)
	b = b<<int32(22) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(22))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xf61e2562)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xc040b340)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x265e5a51)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xe9b6c7aa)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xd62f105d)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0x02441453)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0xd8a1e681)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xe7d3fbc8)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0x21e1cde6)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xc33707d6)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xf4d50d87)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x455a14ed)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += c ^ d&(b^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0xa9e3e905)
	a = a<<int32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5))
	a += b
	d += b ^ c&(a^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0xfcefa3f8)
	d = d<<int32(9) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9))
	d += a
	c += a ^ b&(d^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x676f02d9)
	c = c<<int32(14) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14))
	c += d
	b += d ^ a&(c^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x8d2a4c8a)
	b = b<<int32(20) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(20))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xfffa3942)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x8771f681)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0x6d9d6122)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xfde5380c)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0xa4beea44)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0x4bdecfa9)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0xf6bb4b60)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xbebfbc70)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x289b7ec6)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xeaa127fa)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0xd4ef3085)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0x04881d05)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += b ^ c ^ d + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0xd9d4d039)
	a = a<<int32(4) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(4))
	a += b
	d += a ^ b ^ c + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0xe6db99e5)
	d = d<<int32(11) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))
	d += a
	c += d ^ a ^ b + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0x1fa27cf8)
	c = c<<int32(16) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(16))
	c += d
	b += c ^ d ^ a + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0xc4ac5665)
	b = b<<int32(23) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(23))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0] + uint32(0xf4292244)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)] + uint32(0x432aff97)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)] + uint32(0xab9423a7)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)] + uint32(0xfc93a039)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)] + uint32(0x655b59c3)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)] + uint32(0x8f0ccc92)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)] + uint32(0xffeff47d)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)] + uint32(0x85845dd1)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)] + uint32(0x6fa87e4f)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)] + uint32(0xfe2ce6e0)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)] + uint32(0xa3014314)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)] + uint32(0x4e0811a1)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	a += c ^ (b | ^d) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)] + uint32(0xf7537e82)
	a = a<<int32(6) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6))
	a += b
	d += b ^ (a | ^c) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)] + uint32(0xbd3af235)
	d = d<<int32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d += a
	c += a ^ (d | ^b) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)] + uint32(0x2ad7d2bb)
	c = c<<int32(15) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15))
	c += d
	b += d ^ (c | ^a) + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)] + uint32(0xeb86d391)
	b = b<<int32(21) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(21))
	b += c
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
}

const m_H0 = 1732584193
const m_H1 = 4023233417
const m_H2 = 2562383102
const m_H3 = 271733878
const m_H4 = 3285377520
const m_K0 = 0
const m_K1 = 1518500249
const m_K2 = 1859775393
const m_K3 = 2400959708
const m_K4 = 2840853838
const m_KK0 = 1352829926
const m_KK1 = 1548603684
const m_KK2 = 1836072691
const m_KK3 = 2053994217
const m_KK4 = 0
const m_RMD160_BLOCK_LENGTH = 64
const m_RMD160_DIGEST_LENGTH = 20

type TRMD160_CTX = struct {
	Fstate  [5]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

type TRMD160Context = TRMD160_CTX

/* rotate x left n bits.  */

var _PADDING2 = [64]Tuint8_t{
	0: uint8(0x80),
}

func XRMD160Init(tls *libc.TLS, ctx uintptr) {
	(*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(ctx)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 1*4)) = uint32(0xEFCDAB89)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 2*4)) = uint32(0x98BADCFE)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 3*4)) = uint32(0x10325476)
	*(*Tuint32_t)(unsafe.Pointer(ctx + 4*4)) = uint32(0xC3D2E1F0)
}

func XRMD160Update(tls *libc.TLS, ctx uintptr, input uintptr, len1 Tsize_t) {
	var have, need, off Tsize_t
	_, _, _ = have, need, off
	have = (*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount / uint64(8) % uint64(m_RMD160_BLOCK_LENGTH)
	need = uint64(m_RMD160_BLOCK_LENGTH) - have
	*(*Tuint64_t)(unsafe.Pointer(ctx + 24)) += uint64(8) * len1
	off = uint64(0)
	if len1 >= need {
		if have != 0 {
			libc.X__builtin___memcpy_chk(tls, ctx+32+uintptr(have), input, need, ^t__predefined_size_t(0))
			XRMD160Transform(tls, ctx, ctx+32)
			off = need
			have = uint64(0)
		}
		/* now the buffer is empty */
		for off+uint64(m_RMD160_BLOCK_LENGTH) <= len1 {
			XRMD160Transform(tls, ctx, input+uintptr(off))
			off += uint64(m_RMD160_BLOCK_LENGTH)
		}
	}
	if off < len1 {
		libc.X__builtin___memcpy_chk(tls, ctx+32+uintptr(have), input+uintptr(off), len1-off, ^t__predefined_size_t(0))
	}
}

func XRMD160Pad(tls *libc.TLS, ctx uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var padlen Tsize_t
	var _ /* size at bp+0 */ [8]Tuint8_t
	_ = padlen
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(7)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(56))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(6)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(48))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(5)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(40))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(4)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(32))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(3)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(24))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(2)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(16))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[int32(1)] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount >> int32(8))
	(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[0] = uint8((*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount)
	/*
	 * pad to RMD160_BLOCK_LENGTH byte blocks, at least one byte from
	 * PADDING plus 8 bytes for the size
	 */
	padlen = uint64(m_RMD160_BLOCK_LENGTH) - (*TRMD160_CTX)(unsafe.Pointer(ctx)).Fcount/uint64(8)%uint64(m_RMD160_BLOCK_LENGTH)
	if padlen < libc.Uint64FromInt32(libc.Int32FromInt32(1)+libc.Int32FromInt32(8)) {
		padlen += uint64(m_RMD160_BLOCK_LENGTH)
	}
	XRMD160Update(tls, ctx, uintptr(unsafe.Pointer(&_PADDING2)), padlen-uint64(8)) /* padlen - 8 <= 64 */
	XRMD160Update(tls, ctx, bp, uint64(8))
}

func XRMD160Final(tls *libc.TLS, digest uintptr, ctx uintptr) {
	var i int32
	_ = i
	XRMD160Pad(tls, ctx)
	i = 0
	for {
		if !(i < int32(5)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(ctx + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, ctx, 0, uint64(96), ^t__predefined_size_t(0))
}

func XRMD160Transform(tls *libc.TLS, state uintptr, block uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, aa, b, bb, c, cc, d, dd, e, ee, t Tuint32_t
	var _ /* x at bp+0 */ [16]Tuint32_t
	_, _, _, _, _, _, _, _, _, _, _ = a, aa, b, bb, c, cc, d, dd, e, ee, t
	libc.X__builtin___memcpy_chk(tls, bp, block, uint64(m_RMD160_BLOCK_LENGTH), ^t__predefined_size_t(0))
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* Round 1 */
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)<<int32(11) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)<<int32(14) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)<<int32(15) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)<<int32(12) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)<<int32(5) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)<<int32(8) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)<<int32(7) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)<<int32(9) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)<<int32(11) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)<<int32(13) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)<<int32(14) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)<<int32(15) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)<<int32(6) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)<<int32(7) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)<<int32(9) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)<<int32(8) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #15 */
	/* Round 2 */
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5A827999))<<int32(7) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5A827999))<<int32(6) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5A827999))<<int32(8) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5A827999))<<int32(13) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5A827999))<<int32(11) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5A827999))<<int32(9) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5A827999))<<int32(7) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5A827999))<<int32(15) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5A827999))<<int32(7) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5A827999))<<int32(12) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5A827999))<<int32(15) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5A827999))<<int32(9) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5A827999))<<int32(11) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5A827999))<<int32(7) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5A827999))<<int32(13) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5A827999))<<int32(12) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5A827999))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #31 */
	/* Round 3 */
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6ED9EBA1))<<int32(11) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6ED9EBA1))<<int32(13) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6ED9EBA1))<<int32(6) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6ED9EBA1))<<int32(7) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6ED9EBA1))<<int32(14) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6ED9EBA1))<<int32(9) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6ED9EBA1))<<int32(13) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6ED9EBA1))<<int32(15) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6ED9EBA1))<<int32(14) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6ED9EBA1))<<int32(8) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6ED9EBA1))<<int32(13) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6ED9EBA1))<<int32(6) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6ED9EBA1))<<int32(5) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6ED9EBA1))<<int32(12) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6ED9EBA1))<<int32(7) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6ED9EBA1))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6ED9EBA1))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #47 */
	/* Round 4 */
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x8F1BBCDC))<<int32(11) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x8F1BBCDC))<<int32(12) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x8F1BBCDC))<<int32(14) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x8F1BBCDC))<<int32(15) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x8F1BBCDC))<<int32(14) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x8F1BBCDC))<<int32(15) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x8F1BBCDC))<<int32(9) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x8F1BBCDC))<<int32(8) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x8F1BBCDC))<<int32(9) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x8F1BBCDC))<<int32(14) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x8F1BBCDC))<<int32(5) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x8F1BBCDC))<<int32(6) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x8F1BBCDC))<<int32(8) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x8F1BBCDC))<<int32(6) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x8F1BBCDC))<<int32(5) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x8F1BBCDC))<<int32(12) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x8F1BBCDC))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #63 */
	/* Round 5 */
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0xA953FD4E))<<int32(9) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0xA953FD4E))<<int32(15) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0xA953FD4E))<<int32(5) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0xA953FD4E))<<int32(11) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0xA953FD4E))<<int32(6) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0xA953FD4E))<<int32(8) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0xA953FD4E))<<int32(13) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0xA953FD4E))<<int32(12) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0xA953FD4E))<<int32(5) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0xA953FD4E))<<int32(12) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0xA953FD4E))<<int32(13) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0xA953FD4E))<<int32(14) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0xA953FD4E))<<int32(11) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0xA953FD4E))<<int32(8) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0xA953FD4E))<<int32(5) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0xA953FD4E))<<int32(6) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0xA953FD4E))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #79 */
	aa = a
	bb = b
	cc = c
	dd = d
	ee = e
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* Parallel round 1 */
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x50A28BE6))<<int32(8) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x50A28BE6))<<int32(9) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x50A28BE6))<<int32(9) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x50A28BE6))<<int32(11) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x50A28BE6))<<int32(13) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x50A28BE6))<<int32(15) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x50A28BE6))<<int32(15) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x50A28BE6))<<int32(5) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x50A28BE6))<<int32(7) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x50A28BE6))<<int32(7) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x50A28BE6))<<int32(8) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x50A28BE6))<<int32(11) | (e+(a^(b|^c))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x50A28BE6))<<int32(14) | (d+(e^(a|^b))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x50A28BE6))<<int32(14) | (c+(d^(e|^a))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x50A28BE6))<<int32(12) | (b+(c^(d|^e))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x50A28BE6))<<int32(6) | (a+(b^(c|^d))+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x50A28BE6))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #15 */
	/* Parallel round 2 */
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5C4DD124))<<int32(9) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5C4DD124))<<int32(13) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5C4DD124))<<int32(15) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5C4DD124))<<int32(7) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5C4DD124))<<int32(12) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5C4DD124))<<int32(8) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5C4DD124))<<int32(9) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5C4DD124))<<int32(11) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5C4DD124))<<int32(7) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5C4DD124))<<int32(7) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5C4DD124))<<int32(12) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5C4DD124))<<int32(7) | (d+(e&b | a & ^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5C4DD124))<<int32(6) | (c+(d&a | e & ^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5C4DD124))<<int32(15) | (b+(c&e | d & ^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5C4DD124))<<int32(13) | (a+(b&d | c & ^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5C4DD124))<<int32(11) | (e+(a&c | b & ^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x5C4DD124))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #31 */
	/* Parallel round 3 */
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6D703EF3))<<int32(9) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6D703EF3))<<int32(7) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6D703EF3))<<int32(15) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6D703EF3))<<int32(11) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6D703EF3))<<int32(8) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6D703EF3))<<int32(6) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6D703EF3))<<int32(6) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6D703EF3))<<int32(14) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6D703EF3))<<int32(12) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6D703EF3))<<int32(13) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6D703EF3))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6D703EF3))<<int32(14) | (c+(d|^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6D703EF3))<<int32(13) | (b+(c|^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6D703EF3))<<int32(13) | (a+(b|^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6D703EF3))<<int32(7) | (e+(a|^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6D703EF3))<<int32(5) | (d+(e|^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x6D703EF3))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #47 */
	/* Parallel round 4 */
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x7A6D76E9))<<int32(15) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x7A6D76E9))<<int32(5) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x7A6D76E9))<<int32(8) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x7A6D76E9))<<int32(11) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x7A6D76E9))<<int32(14) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x7A6D76E9))<<int32(14) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x7A6D76E9))<<int32(6) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x7A6D76E9))<<int32(14) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x7A6D76E9))<<int32(6) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x7A6D76E9))<<int32(9) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x7A6D76E9))<<int32(12) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x7A6D76E9))<<int32(9) | (b+(c&d|^c&e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x7A6D76E9))<<int32(12) | (a+(b&c|^b&d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x7A6D76E9))<<int32(5) | (e+(a&b|^a&c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x7A6D76E9))<<int32(15) | (d+(e&a|^e&b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x7A6D76E9))<<int32(8) | (c+(d&e|^d&a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+uint32(0x7A6D76E9))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #63 */
	/* Parallel round 5 */
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)<<int32(8) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(12)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)<<int32(5) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(15)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)<<int32(12) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(10)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)<<int32(9) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(4)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(9)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)<<int32(12) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(1)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(12)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)<<int32(5) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(5)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)<<int32(14) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(8)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(14)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)<<int32(6) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(7)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)<<int32(8) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(6)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)<<int32(13) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(2)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)<<int32(6) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(13)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	a = (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)<<int32(5) | (a+(b^c^d)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(14)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)) + e
	c = c<<libc.Int32FromInt32(10) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	e = (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)<<int32(15) | (e+(a^b^c)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[0]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(15)) + d
	b = b<<libc.Int32FromInt32(10) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	d = (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)<<int32(13) | (d+(e^a^b)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(3)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(13)) + c
	a = a<<libc.Int32FromInt32(10) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	c = (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)<<int32(11) | (c+(d^e^a)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(9)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + b
	e = e<<libc.Int32FromInt32(10) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10))
	b = (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)<<int32(11) | (b+(c^d^e)+(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[int32(11)]+0x00000000)>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(11)) + a
	d = d<<libc.Int32FromInt32(10) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(10)) /* #79 */
	t = *(*Tuint32_t)(unsafe.Pointer(state + 1*4)) + cc + d
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 2*4)) + dd + e
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 3*4)) + ee + a
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) = *(*Tuint32_t)(unsafe.Pointer(state + 4*4)) + aa + b
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) = *(*Tuint32_t)(unsafe.Pointer(state)) + bb + c
	*(*Tuint32_t)(unsafe.Pointer(state)) = t
}

const m_SHA1_BLOCK_LENGTH = 64
const m_SHA1_DIGEST_LENGTH = 20

type TSHA1_CTX = struct {
	Fstate  [5]Tuint32_t
	Fcount  Tuint64_t
	Fbuffer [64]Tuint8_t
}

/*
 * blk0() and blk() perform the initial expand.
 * I got the idea of expanding during the round function from SSLeay
 */

/*
 * (R0+R1), R2, R3, R4 are the different operations (rounds) used in SHA1
 */

type TCHAR64LONG16 = struct {
	Fl [0][16]Tuint32_t
	Fc [64]Tuint8_t
}

// C documentation
//
//	/*
//	 * Hash a single 512-bit block. This is the core of the algorithm.
//	 */
func XSHA1Transform(tls *libc.TLS, state uintptr, buffer uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var a, b, c, d, e, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v6, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v7, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v8, v80, v81, v82, v83, v84, v9 Tuint32_t
	var block uintptr
	var _ /* workspace at bp+0 */ [64]Tuint8_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = a, b, block, c, d, e, v1, v10, v11, v12, v13, v14, v15, v16, v17, v18, v19, v2, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v3, v30, v31, v32, v33, v34, v35, v36, v37, v38, v39, v4, v40, v41, v42, v43, v44, v45, v46, v47, v48, v49, v5, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v6, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v7, v70, v71, v72, v73, v74, v75, v76, v77, v78, v79, v8, v80, v81, v82, v83, v84, v9
	block = bp
	libc.X__builtin___memcpy_chk(tls, block, buffer, uint64(m_SHA1_BLOCK_LENGTH), ^t__predefined_size_t(0))
	/* Copy context->state[] to working vars */
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	/* 4 rounds of 20 operations each. Loop unrolled. */
	v1 = (*(*Tuint32_t)(unsafe.Pointer(block))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block)) = v1
	e += b&(c^d) ^ d + v1 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v2 = (*(*Tuint32_t)(unsafe.Pointer(block + 1*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 1*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 1*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 1*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 1*4)) = v2
	d += a&(b^c) ^ c + v2 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v3 = (*(*Tuint32_t)(unsafe.Pointer(block + 2*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 2*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 2*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 2*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 2*4)) = v3
	c += e&(a^b) ^ b + v3 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v4 = (*(*Tuint32_t)(unsafe.Pointer(block + 3*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 3*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 3*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 3*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 3*4)) = v4
	b += d&(e^a) ^ a + v4 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v5 = (*(*Tuint32_t)(unsafe.Pointer(block + 4*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 4*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 4*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 4*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 4*4)) = v5
	a += c&(d^e) ^ e + v5 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v6 = (*(*Tuint32_t)(unsafe.Pointer(block + 5*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 5*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 5*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 5*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 5*4)) = v6
	e += b&(c^d) ^ d + v6 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v7 = (*(*Tuint32_t)(unsafe.Pointer(block + 6*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 6*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 6*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 6*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 6*4)) = v7
	d += a&(b^c) ^ c + v7 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v8 = (*(*Tuint32_t)(unsafe.Pointer(block + 7*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 7*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 7*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 7*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 7*4)) = v8
	c += e&(a^b) ^ b + v8 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v9 = (*(*Tuint32_t)(unsafe.Pointer(block + 8*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 8*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 8*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 8*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 8*4)) = v9
	b += d&(e^a) ^ a + v9 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v10 = (*(*Tuint32_t)(unsafe.Pointer(block + 9*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 9*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 9*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 9*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 9*4)) = v10
	a += c&(d^e) ^ e + v10 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v11 = (*(*Tuint32_t)(unsafe.Pointer(block + 10*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 10*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 10*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 10*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 10*4)) = v11
	e += b&(c^d) ^ d + v11 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v12 = (*(*Tuint32_t)(unsafe.Pointer(block + 11*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 11*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 11*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 11*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 11*4)) = v12
	d += a&(b^c) ^ c + v12 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v13 = (*(*Tuint32_t)(unsafe.Pointer(block + 12*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 12*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 12*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 12*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 12*4)) = v13
	c += e&(a^b) ^ b + v13 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v14 = (*(*Tuint32_t)(unsafe.Pointer(block + 13*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 13*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 13*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 13*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 13*4)) = v14
	b += d&(e^a) ^ a + v14 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v15 = (*(*Tuint32_t)(unsafe.Pointer(block + 14*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 14*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 14*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 14*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 14*4)) = v15
	a += c&(d^e) ^ e + v15 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v16 = (*(*Tuint32_t)(unsafe.Pointer(block + 15*4))<<libc.Int32FromInt32(24)|*(*Tuint32_t)(unsafe.Pointer(block + 15*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(24)))&libc.Uint32FromUint32(0xFF00FF00) | (*(*Tuint32_t)(unsafe.Pointer(block + 15*4))<<libc.Int32FromInt32(8)|*(*Tuint32_t)(unsafe.Pointer(block + 15*4))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(8)))&libc.Uint32FromInt32(0x00FF00FF)
	*(*Tuint32_t)(unsafe.Pointer(block + 15*4)) = v16
	e += b&(c^d) ^ d + v16 + uint32(0x5A827999) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v17 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(16)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(16)&libc.Int32FromInt32(15))*4)) = v17
	d += a&(b^c) ^ c + v17 + uint32(0x5A827999) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v18 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(17)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(17)&libc.Int32FromInt32(15))*4)) = v18
	c += e&(a^b) ^ b + v18 + uint32(0x5A827999) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v19 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(18)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(18)&libc.Int32FromInt32(15))*4)) = v19
	b += d&(e^a) ^ a + v19 + uint32(0x5A827999) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v20 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(19)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(19)&libc.Int32FromInt32(15))*4)) = v20
	a += c&(d^e) ^ e + v20 + uint32(0x5A827999) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v21 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(20)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(20)&libc.Int32FromInt32(15))*4)) = v21
	e += b ^ c ^ d + v21 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v22 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(21)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(21)&libc.Int32FromInt32(15))*4)) = v22
	d += a ^ b ^ c + v22 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v23 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(22)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(22)&libc.Int32FromInt32(15))*4)) = v23
	c += e ^ a ^ b + v23 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v24 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(23)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(23)&libc.Int32FromInt32(15))*4)) = v24
	b += d ^ e ^ a + v24 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v25 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(24)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(24)&libc.Int32FromInt32(15))*4)) = v25
	a += c ^ d ^ e + v25 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v26 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(25)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(25)&libc.Int32FromInt32(15))*4)) = v26
	e += b ^ c ^ d + v26 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v27 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(26)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(26)&libc.Int32FromInt32(15))*4)) = v27
	d += a ^ b ^ c + v27 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v28 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(27)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(27)&libc.Int32FromInt32(15))*4)) = v28
	c += e ^ a ^ b + v28 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v29 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(28)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(28)&libc.Int32FromInt32(15))*4)) = v29
	b += d ^ e ^ a + v29 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v30 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(29)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(29)&libc.Int32FromInt32(15))*4)) = v30
	a += c ^ d ^ e + v30 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v31 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(30)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(30)&libc.Int32FromInt32(15))*4)) = v31
	e += b ^ c ^ d + v31 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v32 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(31)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(31)&libc.Int32FromInt32(15))*4)) = v32
	d += a ^ b ^ c + v32 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v33 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(32)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(32)&libc.Int32FromInt32(15))*4)) = v33
	c += e ^ a ^ b + v33 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v34 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(33)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(33)&libc.Int32FromInt32(15))*4)) = v34
	b += d ^ e ^ a + v34 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v35 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(34)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(34)&libc.Int32FromInt32(15))*4)) = v35
	a += c ^ d ^ e + v35 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v36 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(35)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(35)&libc.Int32FromInt32(15))*4)) = v36
	e += b ^ c ^ d + v36 + uint32(0x6ED9EBA1) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v37 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(36)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(36)&libc.Int32FromInt32(15))*4)) = v37
	d += a ^ b ^ c + v37 + uint32(0x6ED9EBA1) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v38 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(37)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(37)&libc.Int32FromInt32(15))*4)) = v38
	c += e ^ a ^ b + v38 + uint32(0x6ED9EBA1) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v39 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(38)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(38)&libc.Int32FromInt32(15))*4)) = v39
	b += d ^ e ^ a + v39 + uint32(0x6ED9EBA1) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v40 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(39)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(39)&libc.Int32FromInt32(15))*4)) = v40
	a += c ^ d ^ e + v40 + uint32(0x6ED9EBA1) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v41 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(40)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(40)&libc.Int32FromInt32(15))*4)) = v41
	e += (b|c)&d | b&c + v41 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v42 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(41)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(41)&libc.Int32FromInt32(15))*4)) = v42
	d += (a|b)&c | a&b + v42 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v43 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(42)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(42)&libc.Int32FromInt32(15))*4)) = v43
	c += (e|a)&b | e&a + v43 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v44 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(43)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(43)&libc.Int32FromInt32(15))*4)) = v44
	b += (d|e)&a | d&e + v44 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v45 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(44)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(44)&libc.Int32FromInt32(15))*4)) = v45
	a += (c|d)&e | c&d + v45 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v46 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(45)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(45)&libc.Int32FromInt32(15))*4)) = v46
	e += (b|c)&d | b&c + v46 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v47 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(46)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(46)&libc.Int32FromInt32(15))*4)) = v47
	d += (a|b)&c | a&b + v47 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v48 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(47)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(47)&libc.Int32FromInt32(15))*4)) = v48
	c += (e|a)&b | e&a + v48 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v49 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(48)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(48)&libc.Int32FromInt32(15))*4)) = v49
	b += (d|e)&a | d&e + v49 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v50 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(49)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(49)&libc.Int32FromInt32(15))*4)) = v50
	a += (c|d)&e | c&d + v50 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v51 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(50)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(50)&libc.Int32FromInt32(15))*4)) = v51
	e += (b|c)&d | b&c + v51 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v52 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(51)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(51)&libc.Int32FromInt32(15))*4)) = v52
	d += (a|b)&c | a&b + v52 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v53 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(52)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(52)&libc.Int32FromInt32(15))*4)) = v53
	c += (e|a)&b | e&a + v53 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v54 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(53)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(53)&libc.Int32FromInt32(15))*4)) = v54
	b += (d|e)&a | d&e + v54 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v55 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(54)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(54)&libc.Int32FromInt32(15))*4)) = v55
	a += (c|d)&e | c&d + v55 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v56 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(55)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(55)&libc.Int32FromInt32(15))*4)) = v56
	e += (b|c)&d | b&c + v56 + uint32(0x8F1BBCDC) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v57 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(56)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(56)&libc.Int32FromInt32(15))*4)) = v57
	d += (a|b)&c | a&b + v57 + uint32(0x8F1BBCDC) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v58 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(57)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(57)&libc.Int32FromInt32(15))*4)) = v58
	c += (e|a)&b | e&a + v58 + uint32(0x8F1BBCDC) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v59 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(58)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(58)&libc.Int32FromInt32(15))*4)) = v59
	b += (d|e)&a | d&e + v59 + uint32(0x8F1BBCDC) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v60 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(59)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(59)&libc.Int32FromInt32(15))*4)) = v60
	a += (c|d)&e | c&d + v60 + uint32(0x8F1BBCDC) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v61 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(60)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(60)&libc.Int32FromInt32(15))*4)) = v61
	e += b ^ c ^ d + v61 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v62 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(61)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(61)&libc.Int32FromInt32(15))*4)) = v62
	d += a ^ b ^ c + v62 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v63 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(62)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(62)&libc.Int32FromInt32(15))*4)) = v63
	c += e ^ a ^ b + v63 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v64 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(63)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(63)&libc.Int32FromInt32(15))*4)) = v64
	b += d ^ e ^ a + v64 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v65 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(64)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(64)&libc.Int32FromInt32(15))*4)) = v65
	a += c ^ d ^ e + v65 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v66 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(65)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(65)&libc.Int32FromInt32(15))*4)) = v66
	e += b ^ c ^ d + v66 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v67 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(66)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(66)&libc.Int32FromInt32(15))*4)) = v67
	d += a ^ b ^ c + v67 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v68 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(67)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(67)&libc.Int32FromInt32(15))*4)) = v68
	c += e ^ a ^ b + v68 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v69 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(68)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(68)&libc.Int32FromInt32(15))*4)) = v69
	b += d ^ e ^ a + v69 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v70 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(69)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(69)&libc.Int32FromInt32(15))*4)) = v70
	a += c ^ d ^ e + v70 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v71 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(70)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(70)&libc.Int32FromInt32(15))*4)) = v71
	e += b ^ c ^ d + v71 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v72 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(71)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(71)&libc.Int32FromInt32(15))*4)) = v72
	d += a ^ b ^ c + v72 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v73 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(72)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(72)&libc.Int32FromInt32(15))*4)) = v73
	c += e ^ a ^ b + v73 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v74 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(73)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(73)&libc.Int32FromInt32(15))*4)) = v74
	b += d ^ e ^ a + v74 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v75 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(74)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(74)&libc.Int32FromInt32(15))*4)) = v75
	a += c ^ d ^ e + v75 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v76 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(75)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(75)&libc.Int32FromInt32(15))*4)) = v76
	e += b ^ c ^ d + v76 + uint32(0xCA62C1D6) + (a<<libc.Int32FromInt32(5) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	b = b<<libc.Int32FromInt32(30) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v77 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(76)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(76)&libc.Int32FromInt32(15))*4)) = v77
	d += a ^ b ^ c + v77 + uint32(0xCA62C1D6) + (e<<libc.Int32FromInt32(5) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	a = a<<libc.Int32FromInt32(30) | a>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v78 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(77)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(77)&libc.Int32FromInt32(15))*4)) = v78
	c += e ^ a ^ b + v78 + uint32(0xCA62C1D6) + (d<<libc.Int32FromInt32(5) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	e = e<<libc.Int32FromInt32(30) | e>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v79 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(78)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(78)&libc.Int32FromInt32(15))*4)) = v79
	b += d ^ e ^ a + v79 + uint32(0xCA62C1D6) + (c<<libc.Int32FromInt32(5) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	d = d<<libc.Int32FromInt32(30) | d>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	v80 = (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)))<<libc.Int32FromInt32(1) | (*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(13))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(8))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr((libc.Int32FromInt32(79)+libc.Int32FromInt32(2))&libc.Int32FromInt32(15))*4))^*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)))>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(1))
	*(*Tuint32_t)(unsafe.Pointer(block + uintptr(libc.Int32FromInt32(79)&libc.Int32FromInt32(15))*4)) = v80
	a += c ^ d ^ e + v80 + uint32(0xCA62C1D6) + (b<<libc.Int32FromInt32(5) | b>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(5)))
	c = c<<libc.Int32FromInt32(30) | c>>(libc.Int32FromInt32(32)-libc.Int32FromInt32(30))
	/* Add the working vars back into context.state[] */
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) += e
	/* Wipe variables */
	v84 = libc.Uint32FromInt32(0)
	e = v84
	v83 = v84
	d = v83
	v82 = v83
	c = v82
	v81 = v82
	b = v81
	a = v81
}

// C documentation
//
//	/*
//	 * SHA1Init - Initialize new context
//	 */
func XSHA1Init(tls *libc.TLS, context uintptr) {
	/* SHA1 initialization constants */
	(*TSHA1_CTX)(unsafe.Pointer(context)).Fcount = uint64(0)
	*(*Tuint32_t)(unsafe.Pointer(context)) = uint32(0x67452301)
	*(*Tuint32_t)(unsafe.Pointer(context + 1*4)) = uint32(0xEFCDAB89)
	*(*Tuint32_t)(unsafe.Pointer(context + 2*4)) = uint32(0x98BADCFE)
	*(*Tuint32_t)(unsafe.Pointer(context + 3*4)) = uint32(0x10325476)
	*(*Tuint32_t)(unsafe.Pointer(context + 4*4)) = uint32(0xC3D2E1F0)
}

// C documentation
//
//	/*
//	 * Run your data through this.
//	 */
func XSHA1Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var i, j, v1 Tsize_t
	_, _, _ = i, j, v1
	j = (*TSHA1_CTX)(unsafe.Pointer(context)).Fcount >> libc.Int32FromInt32(3) & libc.Uint64FromInt32(63)
	*(*Tuint64_t)(unsafe.Pointer(context + 24)) += len1 << libc.Int32FromInt32(3)
	if j+len1 > uint64(63) {
		v1 = libc.Uint64FromInt32(64) - j
		i = v1
		libc.X__builtin___memcpy_chk(tls, context+32+uintptr(j), data, v1, ^t__predefined_size_t(0))
		XSHA1Transform(tls, context, context+32)
		for {
			if !(i+uint64(63) < len1) {
				break
			}
			XSHA1Transform(tls, context, data+uintptr(i))
			goto _2
		_2:
			;
			i += uint64(64)
		}
		j = uint64(0)
	} else {
		i = uint64(0)
	}
	libc.X__builtin___memcpy_chk(tls, context+32+uintptr(j), data+uintptr(i), len1-i, ^t__predefined_size_t(0))
}

// C documentation
//
//	/*
//	 * Add padding and return the message digest.
//	 */
func XSHA1Pad(tls *libc.TLS, context uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i uint32
	var _ /* finalcount at bp+0 */ [8]Tuint8_t
	_ = i
	i = uint32(0)
	for {
		if !(i < uint32(8)) {
			break
		}
		(*(*[8]Tuint8_t)(unsafe.Pointer(bp)))[i] = uint8((*TSHA1_CTX)(unsafe.Pointer(context)).Fcount >> ((libc.Uint32FromInt32(7) - i&libc.Uint32FromInt32(7)) * libc.Uint32FromInt32(8)) & libc.Uint64FromInt32(255)) /* Endian independent */
		goto _1
	_1:
		;
		i++
	}
	XSHA1Update(tls, context, __ccgo_ts+153, uint64(1))
	for (*TSHA1_CTX)(unsafe.Pointer(context)).Fcount&uint64(504) != uint64(448) {
		XSHA1Update(tls, context, __ccgo_ts+155, uint64(1))
	}
	XSHA1Update(tls, context, bp, uint64(8)) /* Should cause a SHA1Transform() */
}

func XSHA1Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i uint32
	_ = i
	XSHA1Pad(tls, context)
	i = uint32(0)
	for {
		if !(i < uint32(m_SHA1_DIGEST_LENGTH)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i>>int32(2))*4)) >> ((libc.Uint32FromInt32(3) - i&libc.Uint32FromInt32(3)) * libc.Uint32FromInt32(8)) & libc.Uint32FromInt32(255))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(96), ^t__predefined_size_t(0))
}

const m_SHA224_BLOCK_LENGTH = 64
const m_SHA224_DIGEST_LENGTH = 28
const m_SHA256_BLOCK_LENGTH = 64
const m_SHA256_DIGEST_LENGTH = 32
const m_SHA384_BLOCK_LENGTH = 128
const m_SHA384_DIGEST_LENGTH = 48
const m_SHA512_256_BLOCK_LENGTH = 128
const m_SHA512_256_DIGEST_LENGTH = 32
const m_SHA512_BLOCK_LENGTH = 128
const m_SHA512_DIGEST_LENGTH = 64

type TSHA2_CTX = struct {
	Fstate struct {
		Fst64        [0][8]Tuint64_t
		Fst32        [8]Tuint32_t
		F__ccgo_pad2 [32]byte
	}
	Fbitcount [2]Tuint64_t
	Fbuffer   [128]Tuint8_t
}

type T_SHA2_CTX = TSHA2_CTX

/*
 * UNROLLED TRANSFORM LOOP NOTE:
 * You can define SHA2_UNROLL_TRANSFORM to use the unrolled transform
 * loop version for the hash transform rounds (defined using macros
 * later in this file).  Either define on the command line, for example:
 *
 *   cc -DSHA2_UNROLL_TRANSFORM -o sha2 sha2.c sha2prog.c
 *
 * or define below:
 *
 *   #define SHA2_UNROLL_TRANSFORM
 *
 */

/*** SHA-224/256/384/512 Various Length Definitions ***********************/
/* NOTE: Most of these are in sha2.h */

/*** ENDIAN SPECIFIC COPY MACROS **************************************/

/*
 * Macro for incrementally adding the unsigned 64-bit integer n to the
 * unsigned 128-bit integer (represented using a two-element array of
 * 64-bit words):
 */

/*** THE SIX LOGICAL FUNCTIONS ****************************************/
/*
 * Bit shifting and rotation (used by the six SHA-XYZ logical functions:
 *
 *   NOTE:  The naming of R and S appears backwards here (R is a SHIFT and
 *   S is a ROTATION) because the SHA-224/256/384/512 description document
 *   (see http://csrc.nist.gov/cryptval/shs/sha256-384-512.pdf) uses this
 *   same "backwards" definition.
 */
/* Shift-right (used in SHA-224, SHA-256, SHA-384, and SHA-512): */
/* 32-bit Rotate-right (used in SHA-224 and SHA-256): */
/* 64-bit Rotate-right (used in SHA-384 and SHA-512): */

/* Two of six logical functions used in SHA-224, SHA-256, SHA-384, and SHA-512: */

/* Four of six logical functions used in SHA-224 and SHA-256: */

/* Four of six logical functions used in SHA-384 and SHA-512: */

// C documentation
//
//	/*** SHA-XYZ INITIAL HASH VALUES AND CONSTANTS ************************/
//	/* Hash constant words K for SHA-224 and SHA-256: */
var _K256 = [64]Tuint32_t{
	0:  uint32(0x428a2f98),
	1:  uint32(0x71374491),
	2:  uint32(0xb5c0fbcf),
	3:  uint32(0xe9b5dba5),
	4:  uint32(0x3956c25b),
	5:  uint32(0x59f111f1),
	6:  uint32(0x923f82a4),
	7:  uint32(0xab1c5ed5),
	8:  uint32(0xd807aa98),
	9:  uint32(0x12835b01),
	10: uint32(0x243185be),
	11: uint32(0x550c7dc3),
	12: uint32(0x72be5d74),
	13: uint32(0x80deb1fe),
	14: uint32(0x9bdc06a7),
	15: uint32(0xc19bf174),
	16: uint32(0xe49b69c1),
	17: uint32(0xefbe4786),
	18: uint32(0x0fc19dc6),
	19: uint32(0x240ca1cc),
	20: uint32(0x2de92c6f),
	21: uint32(0x4a7484aa),
	22: uint32(0x5cb0a9dc),
	23: uint32(0x76f988da),
	24: uint32(0x983e5152),
	25: uint32(0xa831c66d),
	26: uint32(0xb00327c8),
	27: uint32(0xbf597fc7),
	28: uint32(0xc6e00bf3),
	29: uint32(0xd5a79147),
	30: uint32(0x06ca6351),
	31: uint32(0x14292967),
	32: uint32(0x27b70a85),
	33: uint32(0x2e1b2138),
	34: uint32(0x4d2c6dfc),
	35: uint32(0x53380d13),
	36: uint32(0x650a7354),
	37: uint32(0x766a0abb),
	38: uint32(0x81c2c92e),
	39: uint32(0x92722c85),
	40: uint32(0xa2bfe8a1),
	41: uint32(0xa81a664b),
	42: uint32(0xc24b8b70),
	43: uint32(0xc76c51a3),
	44: uint32(0xd192e819),
	45: uint32(0xd6990624),
	46: uint32(0xf40e3585),
	47: uint32(0x106aa070),
	48: uint32(0x19a4c116),
	49: uint32(0x1e376c08),
	50: uint32(0x2748774c),
	51: uint32(0x34b0bcb5),
	52: uint32(0x391c0cb3),
	53: uint32(0x4ed8aa4a),
	54: uint32(0x5b9cca4f),
	55: uint32(0x682e6ff3),
	56: uint32(0x748f82ee),
	57: uint32(0x78a5636f),
	58: uint32(0x84c87814),
	59: uint32(0x8cc70208),
	60: uint32(0x90befffa),
	61: uint32(0xa4506ceb),
	62: uint32(0xbef9a3f7),
	63: uint32(0xc67178f2),
}

// C documentation
//
//	/* Initial hash value H for SHA-256: */
var _sha256_initial_hash_value = [8]Tuint32_t{
	0: uint32(0x6a09e667),
	1: uint32(0xbb67ae85),
	2: uint32(0x3c6ef372),
	3: uint32(0xa54ff53a),
	4: uint32(0x510e527f),
	5: uint32(0x9b05688c),
	6: uint32(0x1f83d9ab),
	7: uint32(0x5be0cd19),
}

// C documentation
//
//	/* Hash constant words K for SHA-384 and SHA-512: */
var _K512 = [80]Tuint64_t{
	0:  uint64(0x428a2f98d728ae22),
	1:  uint64(0x7137449123ef65cd),
	2:  uint64(0xb5c0fbcfec4d3b2f),
	3:  uint64(0xe9b5dba58189dbbc),
	4:  uint64(0x3956c25bf348b538),
	5:  uint64(0x59f111f1b605d019),
	6:  uint64(0x923f82a4af194f9b),
	7:  uint64(0xab1c5ed5da6d8118),
	8:  uint64(0xd807aa98a3030242),
	9:  uint64(0x12835b0145706fbe),
	10: uint64(0x243185be4ee4b28c),
	11: uint64(0x550c7dc3d5ffb4e2),
	12: uint64(0x72be5d74f27b896f),
	13: uint64(0x80deb1fe3b1696b1),
	14: uint64(0x9bdc06a725c71235),
	15: uint64(0xc19bf174cf692694),
	16: uint64(0xe49b69c19ef14ad2),
	17: uint64(0xefbe4786384f25e3),
	18: uint64(0x0fc19dc68b8cd5b5),
	19: uint64(0x240ca1cc77ac9c65),
	20: uint64(0x2de92c6f592b0275),
	21: uint64(0x4a7484aa6ea6e483),
	22: uint64(0x5cb0a9dcbd41fbd4),
	23: uint64(0x76f988da831153b5),
	24: uint64(0x983e5152ee66dfab),
	25: uint64(0xa831c66d2db43210),
	26: uint64(0xb00327c898fb213f),
	27: uint64(0xbf597fc7beef0ee4),
	28: uint64(0xc6e00bf33da88fc2),
	29: uint64(0xd5a79147930aa725),
	30: uint64(0x06ca6351e003826f),
	31: uint64(0x142929670a0e6e70),
	32: uint64(0x27b70a8546d22ffc),
	33: uint64(0x2e1b21385c26c926),
	34: uint64(0x4d2c6dfc5ac42aed),
	35: uint64(0x53380d139d95b3df),
	36: uint64(0x650a73548baf63de),
	37: uint64(0x766a0abb3c77b2a8),
	38: uint64(0x81c2c92e47edaee6),
	39: uint64(0x92722c851482353b),
	40: uint64(0xa2bfe8a14cf10364),
	41: uint64(0xa81a664bbc423001),
	42: uint64(0xc24b8b70d0f89791),
	43: uint64(0xc76c51a30654be30),
	44: uint64(0xd192e819d6ef5218),
	45: uint64(0xd69906245565a910),
	46: uint64(0xf40e35855771202a),
	47: uint64(0x106aa07032bbd1b8),
	48: uint64(0x19a4c116b8d2d0c8),
	49: uint64(0x1e376c085141ab53),
	50: uint64(0x2748774cdf8eeb99),
	51: uint64(0x34b0bcb5e19b48a8),
	52: uint64(0x391c0cb3c5c95a63),
	53: uint64(0x4ed8aa4ae3418acb),
	54: uint64(0x5b9cca4f7763e373),
	55: uint64(0x682e6ff3d6b2b8a3),
	56: uint64(0x748f82ee5defb2fc),
	57: uint64(0x78a5636f43172f60),
	58: uint64(0x84c87814a1f0ab72),
	59: uint64(0x8cc702081a6439ec),
	60: uint64(0x90befffa23631e28),
	61: uint64(0xa4506cebde82bde9),
	62: uint64(0xbef9a3f7b2c67915),
	63: uint64(0xc67178f2e372532b),
	64: uint64(0xca273eceea26619c),
	65: uint64(0xd186b8c721c0c207),
	66: uint64(0xeada7dd6cde0eb1e),
	67: uint64(0xf57d4f7fee6ed178),
	68: uint64(0x06f067aa72176fba),
	69: uint64(0x0a637dc5a2c898a6),
	70: uint64(0x113f9804bef90dae),
	71: uint64(0x1b710b35131c471b),
	72: uint64(0x28db77f523047d84),
	73: uint64(0x32caab7b40c72493),
	74: uint64(0x3c9ebe0a15c9bebc),
	75: uint64(0x431d67c49c100d4c),
	76: uint64(0x4cc5d4becb3e42b6),
	77: uint64(0x597f299cfc657e2a),
	78: uint64(0x5fcb6fab3ad6faec),
	79: uint64(0x6c44198c4a475817),
}

// C documentation
//
//	/* Initial hash value H for SHA-512 */
var _sha512_initial_hash_value = [8]Tuint64_t{
	0: uint64(0x6a09e667f3bcc908),
	1: uint64(0xbb67ae8584caa73b),
	2: uint64(0x3c6ef372fe94f82b),
	3: uint64(0xa54ff53a5f1d36f1),
	4: uint64(0x510e527fade682d1),
	5: uint64(0x9b05688c2b3e6c1f),
	6: uint64(0x1f83d9abfb41bd6b),
	7: uint64(0x5be0cd19137e2179),
}

// C documentation
//
//	/* Initial hash value H for SHA-224: */
var _sha224_initial_hash_value = [8]Tuint32_t{
	0: uint32(0xc1059ed8),
	1: uint32(0x367cd507),
	2: uint32(0x3070dd17),
	3: uint32(0xf70e5939),
	4: uint32(0xffc00b31),
	5: uint32(0x68581511),
	6: uint32(0x64f98fa7),
	7: uint32(0xbefa4fa4),
}

// C documentation
//
//	/* Initial hash value H for SHA-384 */
var _sha384_initial_hash_value = [8]Tuint64_t{
	0: uint64(0xcbbb9d5dc1059ed8),
	1: uint64(0x629a292a367cd507),
	2: uint64(0x9159015a3070dd17),
	3: uint64(0x152fecd8f70e5939),
	4: uint64(0x67332667ffc00b31),
	5: uint64(0x8eb44a8768581511),
	6: uint64(0xdb0c2e0d64f98fa7),
	7: uint64(0x47b5481dbefa4fa4),
}

// C documentation
//
//	/* Initial hash value H for SHA-512-256 */
var _sha512_256_initial_hash_value = [8]Tuint64_t{
	0: uint64(0x22312194fc2bf72c),
	1: uint64(0x9f555fa3c84c64c2),
	2: uint64(0x2393b86b6f53b151),
	3: uint64(0x963877195940eabd),
	4: uint64(0x96283ee2a88effe3),
	5: uint64(0xbe5e1e2553863992),
	6: uint64(0x2b0199fc2c85b8aa),
	7: uint64(0x0eb72ddc81c52ca2),
}

// C documentation
//
//	/*** SHA-224: *********************************************************/
func XSHA224Init(tls *libc.TLS, context uintptr) {
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha224_initial_hash_value)), uint64(32), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^t__predefined_size_t(0))
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = uint64(0)
}

func XSHA224Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	XSHA256Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(7)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^t__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-256: *********************************************************/
func XSHA256Init(tls *libc.TLS, context uintptr) {
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha256_initial_hash_value)), uint64(32), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^t__predefined_size_t(0))
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = uint64(0)
}

/* Unrolled SHA-256 round macros: */

func XSHA256Transform(tls *libc.TLS, state uintptr, data uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var T1, a, b, c, d, e, f, g, h, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9 Tuint32_t
	var j int32
	var p1, p2, p3, p4, p5, p6, p7, p8 uintptr
	var _ /* W256 at bp+0 */ [16]Tuint32_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = T1, a, b, c, d, e, f, g, h, j, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9, p1, p2, p3, p4, p5, p6, p7, p8
	/* Initialize registers with the prev. intermediate value */
	a = *(*Tuint32_t)(unsafe.Pointer(state))
	b = *(*Tuint32_t)(unsafe.Pointer(state + 1*4))
	c = *(*Tuint32_t)(unsafe.Pointer(state + 2*4))
	d = *(*Tuint32_t)(unsafe.Pointer(state + 3*4))
	e = *(*Tuint32_t)(unsafe.Pointer(state + 4*4))
	f = *(*Tuint32_t)(unsafe.Pointer(state + 5*4))
	g = *(*Tuint32_t)(unsafe.Pointer(state + 6*4))
	h = *(*Tuint32_t)(unsafe.Pointer(state + 7*4))
	j = 0
	for cond := true; cond; cond = j < int32(16) {
		/* Rounds 0 to 15 (unrolled): */
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = h + (e>>libc.Int32FromInt32(6) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (e>>libc.Int32FromInt32(11) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (e>>libc.Int32FromInt32(25) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (e&f ^ ^e&g) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(2) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (a>>libc.Int32FromInt32(13) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (a>>libc.Int32FromInt32(22) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (a&b ^ a&c ^ b&c)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = g + (d>>libc.Int32FromInt32(6) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (d>>libc.Int32FromInt32(11) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (d>>libc.Int32FromInt32(25) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (d&e ^ ^d&f) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(2) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (h>>libc.Int32FromInt32(13) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (h>>libc.Int32FromInt32(22) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (h&a ^ h&b ^ a&b)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = f + (c>>libc.Int32FromInt32(6) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (c>>libc.Int32FromInt32(11) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (c>>libc.Int32FromInt32(25) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (c&d ^ ^c&e) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(2) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (g>>libc.Int32FromInt32(13) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (g>>libc.Int32FromInt32(22) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (g&h ^ g&a ^ h&a)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = e + (b>>libc.Int32FromInt32(6) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (b>>libc.Int32FromInt32(11) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (b>>libc.Int32FromInt32(25) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (b&c ^ ^b&d) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(2) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (f>>libc.Int32FromInt32(13) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (f>>libc.Int32FromInt32(22) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (f&g ^ f&h ^ g&h)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = d + (a>>libc.Int32FromInt32(6) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (a>>libc.Int32FromInt32(11) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (a>>libc.Int32FromInt32(25) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (a&b ^ ^a&c) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(2) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (e>>libc.Int32FromInt32(13) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (e>>libc.Int32FromInt32(22) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (e&f ^ e&g ^ f&g)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = c + (h>>libc.Int32FromInt32(6) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (h>>libc.Int32FromInt32(11) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (h>>libc.Int32FromInt32(25) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (h&a ^ ^h&b) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(2) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (d>>libc.Int32FromInt32(13) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (d>>libc.Int32FromInt32(22) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (d&e ^ d&f ^ e&f)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = b + (g>>libc.Int32FromInt32(6) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (g>>libc.Int32FromInt32(11) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (g>>libc.Int32FromInt32(25) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (g&h ^ ^g&a) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(2) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (c>>libc.Int32FromInt32(13) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (c>>libc.Int32FromInt32(22) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (c&d ^ c&e ^ d&e)
		j++
		(*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j] = uint32(*(*Tuint8_t)(unsafe.Pointer(data + 3))) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(8) | uint32(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(16) | uint32(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(24)
		data += uintptr(4)
		T1 = a + (f>>libc.Int32FromInt32(6) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (f>>libc.Int32FromInt32(11) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (f>>libc.Int32FromInt32(25) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (f&g ^ ^f&h) + _K256[j] + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[j]
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(2) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (b>>libc.Int32FromInt32(13) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (b>>libc.Int32FromInt32(22) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Now for the remaining rounds up to 63: */
	for cond := true; cond; cond = j < int32(64) {
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p1 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p1)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = h + (e>>libc.Int32FromInt32(6) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (e>>libc.Int32FromInt32(11) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (e>>libc.Int32FromInt32(25) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (e&f ^ ^e&g) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p1))
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(2) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (a>>libc.Int32FromInt32(13) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (a>>libc.Int32FromInt32(22) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (a&b ^ a&c ^ b&c)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p2 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p2)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = g + (d>>libc.Int32FromInt32(6) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (d>>libc.Int32FromInt32(11) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (d>>libc.Int32FromInt32(25) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (d&e ^ ^d&f) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p2))
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(2) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (h>>libc.Int32FromInt32(13) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (h>>libc.Int32FromInt32(22) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (h&a ^ h&b ^ a&b)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p3 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p3)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = f + (c>>libc.Int32FromInt32(6) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (c>>libc.Int32FromInt32(11) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (c>>libc.Int32FromInt32(25) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (c&d ^ ^c&e) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p3))
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(2) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (g>>libc.Int32FromInt32(13) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (g>>libc.Int32FromInt32(22) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (g&h ^ g&a ^ h&a)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p4 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p4)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = e + (b>>libc.Int32FromInt32(6) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (b>>libc.Int32FromInt32(11) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (b>>libc.Int32FromInt32(25) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (b&c ^ ^b&d) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p4))
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(2) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (f>>libc.Int32FromInt32(13) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (f>>libc.Int32FromInt32(22) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (f&g ^ f&h ^ g&h)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p5 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p5)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = d + (a>>libc.Int32FromInt32(6) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (a>>libc.Int32FromInt32(11) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (a>>libc.Int32FromInt32(25) | a<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (a&b ^ ^a&c) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p5))
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(2) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (e>>libc.Int32FromInt32(13) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (e>>libc.Int32FromInt32(22) | e<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (e&f ^ e&g ^ f&g)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p6 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p6)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = c + (h>>libc.Int32FromInt32(6) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (h>>libc.Int32FromInt32(11) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (h>>libc.Int32FromInt32(25) | h<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (h&a ^ ^h&b) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p6))
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(2) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (d>>libc.Int32FromInt32(13) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (d>>libc.Int32FromInt32(22) | d<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (d&e ^ d&f ^ e&f)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p7 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p7)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = b + (g>>libc.Int32FromInt32(6) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (g>>libc.Int32FromInt32(11) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (g>>libc.Int32FromInt32(25) | g<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (g&h ^ ^g&a) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p7))
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(2) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (c>>libc.Int32FromInt32(13) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (c>>libc.Int32FromInt32(22) | c<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (c&d ^ c&e ^ d&e)
		j++
		s0 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(7) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(7)) ^ (s0>>libc.Int32FromInt32(18) | s0<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(18))) ^ s0>>libc.Int32FromInt32(3)
		s1 = (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(17) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(17)) ^ (s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(19))) ^ s1>>libc.Int32FromInt32(10)
		p8 = bp + uintptr(j&int32(0x0f))*4
		*(*Tuint32_t)(unsafe.Pointer(p8)) += s1 + (*(*[16]Tuint32_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = a + (f>>libc.Int32FromInt32(6) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(6)) ^ (f>>libc.Int32FromInt32(11) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(11))) ^ (f>>libc.Int32FromInt32(25) | f<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(25)))) + (f&g ^ ^f&h) + _K256[j] + *(*Tuint32_t)(unsafe.Pointer(p8))
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(2) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(2)) ^ (b>>libc.Int32FromInt32(13) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(13))) ^ (b>>libc.Int32FromInt32(22) | b<<(libc.Int32FromInt32(32)-libc.Int32FromInt32(22)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Compute the current intermediate hash value */
	*(*Tuint32_t)(unsafe.Pointer(state)) += a
	*(*Tuint32_t)(unsafe.Pointer(state + 1*4)) += b
	*(*Tuint32_t)(unsafe.Pointer(state + 2*4)) += c
	*(*Tuint32_t)(unsafe.Pointer(state + 3*4)) += d
	*(*Tuint32_t)(unsafe.Pointer(state + 4*4)) += e
	*(*Tuint32_t)(unsafe.Pointer(state + 5*4)) += f
	*(*Tuint32_t)(unsafe.Pointer(state + 6*4)) += g
	*(*Tuint32_t)(unsafe.Pointer(state + 7*4)) += h
	/* Clean up */
	v16 = libc.Uint32FromInt32(0)
	T1 = v16
	v15 = v16
	h = v15
	v14 = v15
	g = v14
	v13 = v14
	f = v13
	v12 = v13
	e = v12
	v11 = v12
	d = v11
	v10 = v11
	c = v10
	v9 = v10
	b = v9
	a = v9
}

func XSHA256Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var freespace, usedspace, v1, v2 Tsize_t
	_, _, _, _ = freespace, usedspace, v1, v2
	/* Calling with no data is valid (we do nothing) */
	if len1 == uint64(0) {
		return
	}
	usedspace = *(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA256_BLOCK_LENGTH)
	if usedspace > uint64(0) {
		/* Calculate how much free space is available in the buffer */
		freespace = uint64(m_SHA256_BLOCK_LENGTH) - usedspace
		if len1 >= freespace {
			/* Fill the buffer completely and process it */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, freespace, ^t__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += freespace << int32(3)
			len1 -= freespace
			data += uintptr(freespace)
			XSHA256Transform(tls, context, context+80)
		} else {
			/* The buffer is not yet full */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, len1, ^t__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << int32(3)
			/* Clean up: */
			v1 = libc.Uint64FromInt32(0)
			freespace = v1
			usedspace = v1
			return
		}
	}
	for len1 >= uint64(m_SHA256_BLOCK_LENGTH) {
		/* Process as many complete blocks as we can */
		XSHA256Transform(tls, context, data)
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH) << libc.Int32FromInt32(3))
		len1 -= uint64(m_SHA256_BLOCK_LENGTH)
		data += uintptr(m_SHA256_BLOCK_LENGTH)
	}
	if len1 > uint64(0) {
		/* There's left-overs, so save 'em */
		libc.X__builtin___memcpy_chk(tls, context+80, data, len1, ^t__predefined_size_t(0))
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << int32(3)
	}
	/* Clean up: */
	v2 = libc.Uint64FromInt32(0)
	freespace = v2
	usedspace = v2
}

func XSHA256Pad(tls *libc.TLS, context uintptr) {
	var usedspace, v1 uint32
	_, _ = usedspace, v1
	usedspace = uint32(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA256_BLOCK_LENGTH))
	if usedspace > uint32(0) {
		/* Begin padding with a 1 bit: */
		v1 = usedspace
		usedspace++
		*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(v1))) = uint8(0x80)
		if usedspace <= libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) {
			/* Set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8))-usedspace), ^t__predefined_size_t(0))
		} else {
			if usedspace < uint32(m_SHA256_BLOCK_LENGTH) {
				libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(uint32(m_SHA256_BLOCK_LENGTH)-usedspace), ^t__predefined_size_t(0))
			}
			/* Do second-to-last transform: */
			XSHA256Transform(tls, context, context+80)
			/* Prepare for last transform: */
			libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)), ^t__predefined_size_t(0))
		}
	} else {
		/* Set-up for the last transform: */
		libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)), ^t__predefined_size_t(0))
		/* Begin padding with a 1 bit: */
		*(*Tuint8_t)(unsafe.Pointer(context + 80)) = uint8(0x80)
	}
	/* Store the length of input data (in bits) in big endian format: */
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA256_BLOCK_LENGTH)-libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)))
	/* Final transform: */
	XSHA256Transform(tls, context, context+80)
	/* Clean up: */
	usedspace = uint32(0)
}

func XSHA256Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	XSHA256Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(8)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)))) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 1)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 2)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(4)) + 3)) = uint8(*(*Tuint32_t)(unsafe.Pointer(context + uintptr(i)*4)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^t__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-512: *********************************************************/
func XSHA512Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha512_initial_hash_value)), uint64(64), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^t__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

/* Unrolled SHA-512 round macros: */

func XSHA512Transform(tls *libc.TLS, state uintptr, data uintptr) {
	bp := tls.Alloc(128)
	defer tls.Free(128)
	var T1, a, b, c, d, e, f, g, h, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9 Tuint64_t
	var j int32
	var p1, p2, p3, p4, p5, p6, p7, p8 uintptr
	var _ /* W512 at bp+0 */ [16]Tuint64_t
	_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _ = T1, a, b, c, d, e, f, g, h, j, s0, s1, v10, v11, v12, v13, v14, v15, v16, v9, p1, p2, p3, p4, p5, p6, p7, p8
	/* Initialize registers with the prev. intermediate value */
	a = *(*Tuint64_t)(unsafe.Pointer(state))
	b = *(*Tuint64_t)(unsafe.Pointer(state + 1*8))
	c = *(*Tuint64_t)(unsafe.Pointer(state + 2*8))
	d = *(*Tuint64_t)(unsafe.Pointer(state + 3*8))
	e = *(*Tuint64_t)(unsafe.Pointer(state + 4*8))
	f = *(*Tuint64_t)(unsafe.Pointer(state + 5*8))
	g = *(*Tuint64_t)(unsafe.Pointer(state + 6*8))
	h = *(*Tuint64_t)(unsafe.Pointer(state + 7*8))
	j = 0
	for cond := true; cond; cond = j < int32(16) {
		/* Rounds 0 to 15 (unrolled): */
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = h + (e>>libc.Int32FromInt32(14) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (e>>libc.Int32FromInt32(18) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (e>>libc.Int32FromInt32(41) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (e&f ^ ^e&g) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(28) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (a>>libc.Int32FromInt32(34) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (a>>libc.Int32FromInt32(39) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (a&b ^ a&c ^ b&c)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = g + (d>>libc.Int32FromInt32(14) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (d>>libc.Int32FromInt32(18) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (d>>libc.Int32FromInt32(41) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (d&e ^ ^d&f) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(28) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (h>>libc.Int32FromInt32(34) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (h>>libc.Int32FromInt32(39) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (h&a ^ h&b ^ a&b)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = f + (c>>libc.Int32FromInt32(14) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (c>>libc.Int32FromInt32(18) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (c>>libc.Int32FromInt32(41) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (c&d ^ ^c&e) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(28) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (g>>libc.Int32FromInt32(34) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (g>>libc.Int32FromInt32(39) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (g&h ^ g&a ^ h&a)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = e + (b>>libc.Int32FromInt32(14) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (b>>libc.Int32FromInt32(18) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (b>>libc.Int32FromInt32(41) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (b&c ^ ^b&d) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(28) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (f>>libc.Int32FromInt32(34) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (f>>libc.Int32FromInt32(39) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (f&g ^ f&h ^ g&h)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = d + (a>>libc.Int32FromInt32(14) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (a>>libc.Int32FromInt32(18) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (a>>libc.Int32FromInt32(41) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (a&b ^ ^a&c) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(28) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (e>>libc.Int32FromInt32(34) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (e>>libc.Int32FromInt32(39) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (e&f ^ e&g ^ f&g)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = c + (h>>libc.Int32FromInt32(14) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (h>>libc.Int32FromInt32(18) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (h>>libc.Int32FromInt32(41) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (h&a ^ ^h&b) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(28) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (d>>libc.Int32FromInt32(34) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (d>>libc.Int32FromInt32(39) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (d&e ^ d&f ^ e&f)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = b + (g>>libc.Int32FromInt32(14) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (g>>libc.Int32FromInt32(18) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (g>>libc.Int32FromInt32(41) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (g&h ^ ^g&a) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(28) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (c>>libc.Int32FromInt32(34) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (c>>libc.Int32FromInt32(39) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (c&d ^ c&e ^ d&e)
		j++
		(*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j] = uint64(*(*Tuint8_t)(unsafe.Pointer(data + 7))) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 6)))<<libc.Int32FromInt32(8) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 5)))<<libc.Int32FromInt32(16) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 4)))<<libc.Int32FromInt32(24) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 3)))<<libc.Int32FromInt32(32) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 2)))<<libc.Int32FromInt32(40) | uint64(*(*Tuint8_t)(unsafe.Pointer(data + 1)))<<libc.Int32FromInt32(48) | uint64(*(*Tuint8_t)(unsafe.Pointer(data)))<<libc.Int32FromInt32(56)
		data += uintptr(8)
		T1 = a + (f>>libc.Int32FromInt32(14) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (f>>libc.Int32FromInt32(18) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (f>>libc.Int32FromInt32(41) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (f&g ^ ^f&h) + _K512[j] + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[j]
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(28) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (b>>libc.Int32FromInt32(34) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (b>>libc.Int32FromInt32(39) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Now for the remaining rounds up to 79: */
	for cond := true; cond; cond = j < int32(80) {
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p1 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p1)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = h + (e>>libc.Int32FromInt32(14) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (e>>libc.Int32FromInt32(18) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (e>>libc.Int32FromInt32(41) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (e&f ^ ^e&g) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p1))
		d += T1
		h = T1 + (a>>libc.Int32FromInt32(28) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (a>>libc.Int32FromInt32(34) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (a>>libc.Int32FromInt32(39) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (a&b ^ a&c ^ b&c)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p2 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p2)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = g + (d>>libc.Int32FromInt32(14) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (d>>libc.Int32FromInt32(18) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (d>>libc.Int32FromInt32(41) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (d&e ^ ^d&f) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p2))
		c += T1
		g = T1 + (h>>libc.Int32FromInt32(28) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (h>>libc.Int32FromInt32(34) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (h>>libc.Int32FromInt32(39) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (h&a ^ h&b ^ a&b)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p3 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p3)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = f + (c>>libc.Int32FromInt32(14) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (c>>libc.Int32FromInt32(18) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (c>>libc.Int32FromInt32(41) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (c&d ^ ^c&e) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p3))
		b += T1
		f = T1 + (g>>libc.Int32FromInt32(28) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (g>>libc.Int32FromInt32(34) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (g>>libc.Int32FromInt32(39) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (g&h ^ g&a ^ h&a)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p4 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p4)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = e + (b>>libc.Int32FromInt32(14) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (b>>libc.Int32FromInt32(18) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (b>>libc.Int32FromInt32(41) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (b&c ^ ^b&d) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p4))
		a += T1
		e = T1 + (f>>libc.Int32FromInt32(28) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (f>>libc.Int32FromInt32(34) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (f>>libc.Int32FromInt32(39) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (f&g ^ f&h ^ g&h)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p5 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p5)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = d + (a>>libc.Int32FromInt32(14) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (a>>libc.Int32FromInt32(18) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (a>>libc.Int32FromInt32(41) | a<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (a&b ^ ^a&c) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p5))
		h += T1
		d = T1 + (e>>libc.Int32FromInt32(28) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (e>>libc.Int32FromInt32(34) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (e>>libc.Int32FromInt32(39) | e<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (e&f ^ e&g ^ f&g)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p6 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p6)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = c + (h>>libc.Int32FromInt32(14) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (h>>libc.Int32FromInt32(18) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (h>>libc.Int32FromInt32(41) | h<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (h&a ^ ^h&b) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p6))
		g += T1
		c = T1 + (d>>libc.Int32FromInt32(28) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (d>>libc.Int32FromInt32(34) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (d>>libc.Int32FromInt32(39) | d<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (d&e ^ d&f ^ e&f)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p7 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p7)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = b + (g>>libc.Int32FromInt32(14) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (g>>libc.Int32FromInt32(18) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (g>>libc.Int32FromInt32(41) | g<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (g&h ^ ^g&a) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p7))
		f += T1
		b = T1 + (c>>libc.Int32FromInt32(28) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (c>>libc.Int32FromInt32(34) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (c>>libc.Int32FromInt32(39) | c<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (c&d ^ c&e ^ d&e)
		j++
		s0 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(1))&int32(0x0f)]
		s0 = s0>>libc.Int32FromInt32(1) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(1)) ^ (s0>>libc.Int32FromInt32(8) | s0<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(8))) ^ s0>>libc.Int32FromInt32(7)
		s1 = (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(14))&int32(0x0f)]
		s1 = s1>>libc.Int32FromInt32(19) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(19)) ^ (s1>>libc.Int32FromInt32(61) | s1<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(61))) ^ s1>>libc.Int32FromInt32(6)
		p8 = bp + uintptr(j&int32(0x0f))*8
		*(*Tuint64_t)(unsafe.Pointer(p8)) += s1 + (*(*[16]Tuint64_t)(unsafe.Pointer(bp)))[(j+int32(9))&int32(0x0f)] + s0
		T1 = a + (f>>libc.Int32FromInt32(14) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(14)) ^ (f>>libc.Int32FromInt32(18) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(18))) ^ (f>>libc.Int32FromInt32(41) | f<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(41)))) + (f&g ^ ^f&h) + _K512[j] + *(*Tuint64_t)(unsafe.Pointer(p8))
		e += T1
		a = T1 + (b>>libc.Int32FromInt32(28) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(28)) ^ (b>>libc.Int32FromInt32(34) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(34))) ^ (b>>libc.Int32FromInt32(39) | b<<(libc.Int32FromInt32(64)-libc.Int32FromInt32(39)))) + (b&c ^ b&d ^ c&d)
		j++
	}
	/* Compute the current intermediate hash value */
	*(*Tuint64_t)(unsafe.Pointer(state)) += a
	*(*Tuint64_t)(unsafe.Pointer(state + 1*8)) += b
	*(*Tuint64_t)(unsafe.Pointer(state + 2*8)) += c
	*(*Tuint64_t)(unsafe.Pointer(state + 3*8)) += d
	*(*Tuint64_t)(unsafe.Pointer(state + 4*8)) += e
	*(*Tuint64_t)(unsafe.Pointer(state + 5*8)) += f
	*(*Tuint64_t)(unsafe.Pointer(state + 6*8)) += g
	*(*Tuint64_t)(unsafe.Pointer(state + 7*8)) += h
	/* Clean up */
	v16 = libc.Uint64FromInt32(0)
	T1 = v16
	v15 = v16
	h = v15
	v14 = v15
	g = v14
	v13 = v14
	f = v13
	v12 = v13
	e = v12
	v11 = v12
	d = v11
	v10 = v11
	c = v10
	v9 = v10
	b = v9
	a = v9
}

func XSHA512Update(tls *libc.TLS, context uintptr, data uintptr, len1 Tsize_t) {
	var freespace, usedspace, v1, v2 Tsize_t
	_, _, _, _ = freespace, usedspace, v1, v2
	/* Calling with no data is valid (we do nothing) */
	if len1 == uint64(0) {
		return
	}
	usedspace = *(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA512_BLOCK_LENGTH)
	if usedspace > uint64(0) {
		/* Calculate how much free space is available in the buffer */
		freespace = uint64(m_SHA512_BLOCK_LENGTH) - usedspace
		if len1 >= freespace {
			/* Fill the buffer completely and process it */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, freespace, ^t__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += freespace << libc.Int32FromInt32(3)
			if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < freespace<<libc.Int32FromInt32(3) {
				*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
			}
			len1 -= freespace
			data += uintptr(freespace)
			XSHA512Transform(tls, context, context+80)
		} else {
			/* The buffer is not yet full */
			libc.X__builtin___memcpy_chk(tls, context+80+uintptr(usedspace), data, len1, ^t__predefined_size_t(0))
			*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << libc.Int32FromInt32(3)
			if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < len1<<libc.Int32FromInt32(3) {
				*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
			}
			/* Clean up: */
			v1 = libc.Uint64FromInt32(0)
			freespace = v1
			usedspace = v1
			return
		}
	}
	for len1 >= uint64(m_SHA512_BLOCK_LENGTH) {
		/* Process as many complete blocks as we can */
		XSHA512Transform(tls, context, data)
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH) << libc.Int32FromInt32(3))
		if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)<<libc.Int32FromInt32(3)) {
			*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
		}
		len1 -= uint64(m_SHA512_BLOCK_LENGTH)
		data += uintptr(m_SHA512_BLOCK_LENGTH)
	}
	if len1 > uint64(0) {
		/* There's left-overs, so save 'em */
		libc.X__builtin___memcpy_chk(tls, context+80, data, len1, ^t__predefined_size_t(0))
		*(*Tuint64_t)(unsafe.Pointer(context + 64)) += len1 << libc.Int32FromInt32(3)
		if *(*Tuint64_t)(unsafe.Pointer(context + 64)) < len1<<libc.Int32FromInt32(3) {
			*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8))++
		}
	}
	/* Clean up: */
	v2 = libc.Uint64FromInt32(0)
	freespace = v2
	usedspace = v2
}

func XSHA512Pad(tls *libc.TLS, context uintptr) {
	var usedspace, v1 uint32
	_, _ = usedspace, v1
	usedspace = uint32(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> libc.Int32FromInt32(3) % uint64(m_SHA512_BLOCK_LENGTH))
	if usedspace > uint32(0) {
		/* Begin padding with a 1 bit: */
		v1 = usedspace
		usedspace++
		*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(v1))) = uint8(0x80)
		if usedspace <= libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) {
			/* Set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(libc.Uint32FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16))-usedspace), ^t__predefined_size_t(0))
		} else {
			if usedspace < uint32(m_SHA512_BLOCK_LENGTH) {
				libc.X__builtin___memset_chk(tls, context+80+uintptr(usedspace), 0, uint64(uint32(m_SHA512_BLOCK_LENGTH)-usedspace), ^t__predefined_size_t(0))
			}
			/* Do second-to-last transform: */
			XSHA512Transform(tls, context, context+80)
			/* And set-up for the last transform: */
			libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(2)), ^t__predefined_size_t(0))
		}
	} else {
		/* Prepare for final transform: */
		libc.X__builtin___memset_chk(tls, context+80, 0, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)), ^t__predefined_size_t(0))
		/* Begin padding with a 1 bit: */
		*(*Tuint8_t)(unsafe.Pointer(context + 80)) = uint8(0x80)
	}
	/* Store the length of input data (in bits) in big endian format: */
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(56))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(48))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(40))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(32))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(24))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(16))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)) >> int32(8))
	*(*Tuint8_t)(unsafe.Pointer(context + 80 + uintptr(libc.Int32FromInt32(m_SHA512_BLOCK_LENGTH)-libc.Int32FromInt32(16)+libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + 64)))
	/* Final transform: */
	XSHA512Transform(tls, context, context+80)
	/* Clean up: */
	usedspace = uint32(0)
}

func XSHA512Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	XSHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(8)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^t__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-384: *********************************************************/
func XSHA384Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha384_initial_hash_value)), uint64(64), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^t__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

func XSHA384Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	XSHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(6)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	/* Zero out state data */
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^t__predefined_size_t(0))
}

// C documentation
//
//	/*** SHA-512/256: *********************************************************/
func XSHA512_256Init(tls *libc.TLS, context uintptr) {
	var v1 Tuint64_t
	_ = v1
	libc.X__builtin___memcpy_chk(tls, context, uintptr(unsafe.Pointer(&_sha512_256_initial_hash_value)), uint64(64), ^t__predefined_size_t(0))
	libc.X__builtin___memset_chk(tls, context+80, 0, uint64(128), ^t__predefined_size_t(0))
	v1 = libc.Uint64FromInt32(0)
	*(*Tuint64_t)(unsafe.Pointer(context + 64 + 1*8)) = v1
	*(*Tuint64_t)(unsafe.Pointer(context + 64)) = v1
}

func XSHA512_256Final(tls *libc.TLS, digest uintptr, context uintptr) {
	var i int32
	_ = i
	XSHA512Pad(tls, context)
	/* Convert TO host byte order */
	i = 0
	for {
		if !(i < int32(4)) {
			break
		}
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)))) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(56))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 1)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(48))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 2)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(40))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 3)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(32))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 4)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(24))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 5)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(16))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 6)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)) >> int32(8))
		*(*Tuint8_t)(unsafe.Pointer(digest + uintptr(i*libc.Int32FromInt32(8)) + 7)) = uint8(*(*Tuint64_t)(unsafe.Pointer(context + uintptr(i)*8)))
		goto _1
	_1:
		;
		i++
	}
	/* Zero out state data */
	libc.X__builtin___memset_chk(tls, context, 0, uint64(208), ^t__predefined_size_t(0))
}

const m_ACCESSX_MAX_DESCRIPTORS = 100
const m_ATTRIBUTION_NAME_MAX = 255
const m_AT_EACCESS = 0x0010
const m_AT_FDONLY = 0x0400
const m_AT_REALDEV = 0x0200
const m_AT_REMOVEDIR = 0x0080
const m_AT_SYMLINK_FOLLOW = 0x0040
const m_AT_SYMLINK_NOFOLLOW = 0x0020
const m_AT_SYMLINK_NOFOLLOW_ANY = 0x0800
const m_BADSIG = "SIG_ERR"
const m_BUFSIZ = 1024
const m_BUS_ADRALN = 1
const m_BUS_ADRERR = 2
const m_BUS_NOOP = 0
const m_BUS_OBJERR = 3
const m_CLD_CONTINUED = 6
const m_CLD_DUMPED = 3
const m_CLD_EXITED = 1
const m_CLD_KILLED = 2
const m_CLD_NOOP = 0
const m_CLD_STOPPED = 5
const m_CLD_TRAPPED = 4
const m_CPF_IGNORE_MODE = 0x0002
const m_CPF_OVERWRITE = 0x0001
const m_CPUMON_MAKE_FATAL = 0x1000
const m_E2BIG = 7
const m_EACCES = 13
const m_EADDRINUSE = 48
const m_EADDRNOTAVAIL = 49
const m_EAFNOSUPPORT = 47
const m_EAGAIN = 35
const m_EALREADY = 37
const m_EAUTH = 80
const m_EBADARCH = 86
const m_EBADEXEC = 85
const m_EBADF = 9
const m_EBADMACHO = 88
const m_EBADMSG = 94
const m_EBADRPC = 72
const m_EBUSY = 16
const m_ECANCELED = 89
const m_ECHILD = 10
const m_ECONNABORTED = 53
const m_ECONNREFUSED = 61
const m_ECONNRESET = 54
const m_EDEADLK = 11
const m_EDESTADDRREQ = 39
const m_EDEVERR = 83
const m_EDOM = 33
const m_EDQUOT = 69
const m_EEXIST = 17
const m_EFAULT = 14
const m_EFBIG = 27
const m_EFTYPE = 79
const m_EF_IS_PURGEABLE = 0x00000008
const m_EF_IS_SPARSE = 0x00000010
const m_EF_IS_SYNC_ROOT = 0x00000004
const m_EF_IS_SYNTHETIC = 0x00000020
const m_EF_MAY_SHARE_BLOCKS = 0x00000001
const m_EF_NO_XATTRS = 0x00000002
const m_EF_SHARES_ALL_BLOCKS = 0x00000040
const m_EHOSTDOWN = 64
const m_EHOSTUNREACH = 65
const m_EIDRM = 90
const m_EILSEQ = 92
const m_EINPROGRESS = 36
const m_EINTR = 4
const m_EINVAL = 22
const m_EIO = 5
const m_EISCONN = 56
const m_EISDIR = 21
const m_ELAST = 106
const m_ELOOP = 62
const m_EMFILE = 24
const m_EMLINK = 31
const m_EMSGSIZE = 40
const m_EMULTIHOP = 95
const m_ENAMETOOLONG = 63
const m_ENEEDAUTH = 81
const m_ENETDOWN = 50
const m_ENETRESET = 52
const m_ENETUNREACH = 51
const m_ENFILE = 23
const m_ENOATTR = 93
const m_ENOBUFS = 55
const m_ENODATA = 96
const m_ENODEV = 19
const m_ENOENT = 2
const m_ENOEXEC = 8
const m_ENOLCK = 77
const m_ENOLINK = 97
const m_ENOMEM = 12
const m_ENOMSG = 91
const m_ENOPOLICY = 103
const m_ENOPROTOOPT = 42
const m_ENOSPC = 28
const m_ENOSR = 98
const m_ENOSTR = 99
const m_ENOSYS = 78
const m_ENOTBLK = 15
const m_ENOTCONN = 57
const m_ENOTDIR = 20
const m_ENOTEMPTY = 66
const m_ENOTRECOVERABLE = 104
const m_ENOTSOCK = 38
const m_ENOTSUP = 45
const m_ENOTTY = 25
const m_ENXIO = 6
const m_EOPNOTSUPP = 102
const m_EOVERFLOW = 84
const m_EOWNERDEAD = 105
const m_EPERM = 1
const m_EPFNOSUPPORT = 46
const m_EPIPE = 32
const m_EPROCLIM = 67
const m_EPROCUNAVAIL = 76
const m_EPROGMISMATCH = 75
const m_EPROGUNAVAIL = 74
const m_EPROTO = 100
const m_EPROTONOSUPPORT = 43
const m_EPROTOTYPE = 41
const m_EPWROFF = 82
const m_EQFULL = 106
const m_ERANGE = 34
const m_EREMOTE = 71
const m_EROFS = 30
const m_ERPCMISMATCH = 73
const m_ESHLIBVERS = 87
const m_ESHUTDOWN = 58
const m_ESOCKTNOSUPPORT = 44
const m_ESPIPE = 29
const m_ESRCH = 3
const m_ESTALE = 70
const m_ETIME = 101
const m_ETIMEDOUT = 60
const m_ETOOMANYREFS = 59
const m_ETXTBSY = 26
const m_EUSERS = 68
const m_EWOULDBLOCK = "EAGAIN"
const m_EXDEV = 18
const m_EXIT_FAILURE = 1
const m_EXIT_SUCCESS = 0
const m_FAPPEND = "O_APPEND"
const m_FASYNC = "O_ASYNC"
const m_FCNTL_FS_SPECIFIC_BASE = 0x00010000
const m_FD_CLOEXEC = 1
const m_FFDSYNC = "O_DSYNC"
const m_FFSYNC = "O_FSYNC"
const m_FILENAME_MAX = 1024
const m_FILESEC_GUID = "FILESEC_UUID"
const m_FNDELAY = "O_NONBLOCK"
const m_FNONBLOCK = "O_NONBLOCK"
const m_FOOTPRINT_INTERVAL_RESET = 0x1
const m_FOPEN_MAX = 20
const m_FPE_FLTDIV = 1
const m_FPE_FLTINV = 5
const m_FPE_FLTOVF = 2
const m_FPE_FLTRES = 4
const m_FPE_FLTSUB = 6
const m_FPE_FLTUND = 3
const m_FPE_INTDIV = 7
const m_FPE_INTOVF = 8
const m_FPE_NOOP = 0
const m_FP_CHOP = 3
const m_FP_PREC_24B = 0
const m_FP_PREC_53B = 2
const m_FP_PREC_64B = 3
const m_FP_RND_DOWN = 1
const m_FP_RND_NEAR = 0
const m_FP_RND_UP = 2
const m_FP_STATE_BYTES = 512
const m_FREAD = 0x00000001
const m_FWRITE = 0x00000002
const m_F_ADDFILESIGS = 61
const m_F_ADDFILESIGS_FOR_DYLD_SIM = 83
const m_F_ADDFILESIGS_INFO = 103
const m_F_ADDFILESIGS_RETURN = 97
const m_F_ADDFILESUPPL = 104
const m_F_ADDSIGS = 59
const m_F_ADDSIGS_MAIN_BINARY = 113
const m_F_ALLOCATEALL = 0x00000004
const m_F_ALLOCATECONTIG = 0x00000002
const m_F_ALLOCATEPERSIST = 0x00000008
const m_F_ATTRIBUTION_TAG = 111
const m_F_BARRIERFSYNC = 85
const m_F_CHECK_LV = 98
const m_F_CHKCLEAN = 41
const m_F_CREATE_TAG = 0x00000001
const m_F_DELETE_TAG = 0x00000002
const m_F_DUPFD = 0
const m_F_DUPFD_CLOEXEC = 67
const m_F_FINDSIGS = 78
const m_F_FLUSH_DATA = 40
const m_F_FREEZE_FS = 53
const m_F_FULLFSYNC = 51
const m_F_GETCODEDIR = 72
const m_F_GETFD = 1
const m_F_GETFL = 3
const m_F_GETLEASE = 107
const m_F_GETLK = 7
const m_F_GETLKPID = 66
const m_F_GETNOSIGPIPE = 74
const m_F_GETOWN = 5
const m_F_GETPATH = 50
const m_F_GETPATH_MTMINFO = 71
const m_F_GETPATH_NOFIRMLINK = 102
const m_F_GETPROTECTIONCLASS = 63
const m_F_GETPROTECTIONLEVEL = 77
const m_F_GETSIGSINFO = 105
const m_F_GLOBAL_NOCACHE = 55
const m_F_LOCK = 1
const m_F_LOG2PHYS = 49
const m_F_LOG2PHYS_EXT = 65
const m_F_NOCACHE = 48
const m_F_NODIRECT = 62
const m_F_OFD_GETLK = 92
const m_F_OFD_SETLK = 90
const m_F_OFD_SETLKW = 91
const m_F_OFD_SETLKWTIMEOUT = 93
const m_F_OK = 0
const m_F_PATHPKG_CHECK = 52
const m_F_PEOFPOSMODE = 3
const m_F_PREALLOCATE = 42
const m_F_PUNCHHOLE = 99
const m_F_QUERY_TAG = 0x00000004
const m_F_RDADVISE = 44
const m_F_RDAHEAD = 45
const m_F_RDLCK = 1
const m_F_SETBACKINGSTORE = 70
const m_F_SETFD = 2
const m_F_SETFL = 4
const m_F_SETLEASE = 106
const m_F_SETLK = 8
const m_F_SETLKW = 9
const m_F_SETLKWTIMEOUT = 10
const m_F_SETNOSIGPIPE = 73
const m_F_SETOWN = 6
const m_F_SETPROTECTIONCLASS = 64
const m_F_SETSIZE = 43
const m_F_SINGLE_WRITER = 76
const m_F_SPECULATIVE_READ = 101
const m_F_TEST = 3
const m_F_THAW_FS = 54
const m_F_TLOCK = 2
const m_F_TRANSCODEKEY = 75
const m_F_TRANSFEREXTENTS = 110
const m_F_TRIM_ACTIVE_FILE = 100
const m_F_ULOCK = 0
const m_F_UNLCK = 2
const m_F_VOLPOSMODE = 4
const m_F_WRLCK = 3
const m_GETSIGSINFO_PLATFORM_BINARY = 1
const m_ILL_BADSTK = 8
const m_ILL_COPROC = 7
const m_ILL_ILLADR = 5
const m_ILL_ILLOPC = 1
const m_ILL_ILLOPN = 4
const m_ILL_ILLTRP = 2
const m_ILL_NOOP = 0
const m_ILL_PRVOPC = 3
const m_ILL_PRVREG = 6
const m_IOPOL_APPLICATION = "IOPOL_STANDARD"
const m_IOPOL_ATIME_UPDATES_DEFAULT = 0
const m_IOPOL_ATIME_UPDATES_OFF = 1
const m_IOPOL_DEFAULT = 0
const m_IOPOL_IMPORTANT = 1
const m_IOPOL_MATERIALIZE_DATALESS_FILES_DEFAULT = 0
const m_IOPOL_MATERIALIZE_DATALESS_FILES_OFF = 1
const m_IOPOL_MATERIALIZE_DATALESS_FILES_ON = 2
const m_IOPOL_NORMAL = "IOPOL_IMPORTANT"
const m_IOPOL_PASSIVE = 2
const m_IOPOL_SCOPE_DARWIN_BG = 2
const m_IOPOL_SCOPE_PROCESS = 0
const m_IOPOL_SCOPE_THREAD = 1
const m_IOPOL_STANDARD = 5
const m_IOPOL_THROTTLE = 3
const m_IOPOL_TYPE_DISK = 0
const m_IOPOL_TYPE_VFS_ALLOW_LOW_SPACE_WRITES = 9
const m_IOPOL_TYPE_VFS_ATIME_UPDATES = 2
const m_IOPOL_TYPE_VFS_DISALLOW_RW_FOR_O_EVTONLY = 10
const m_IOPOL_TYPE_VFS_IGNORE_CONTENT_PROTECTION = 6
const m_IOPOL_TYPE_VFS_IGNORE_PERMISSIONS = 7
const m_IOPOL_TYPE_VFS_MATERIALIZE_DATALESS_FILES = 3
const m_IOPOL_TYPE_VFS_SKIP_MTIME_UPDATE = 8
const m_IOPOL_TYPE_VFS_STATFS_NO_DATA_VOLUME = 4
const m_IOPOL_TYPE_VFS_TRIGGER_RESOLVE = 5
const m_IOPOL_UTILITY = 4
const m_IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_OFF = 0
const m_IOPOL_VFS_ALLOW_LOW_SPACE_WRITES_ON = 1
const m_IOPOL_VFS_CONTENT_PROTECTION_DEFAULT = 0
const m_IOPOL_VFS_CONTENT_PROTECTION_IGNORE = 1
const m_IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_DEFAULT = 0
const m_IOPOL_VFS_DISALLOW_RW_FOR_O_EVTONLY_ON = 1
const m_IOPOL_VFS_IGNORE_PERMISSIONS_OFF = 0
const m_IOPOL_VFS_IGNORE_PERMISSIONS_ON = 1
const m_IOPOL_VFS_NOCACHE_WRITE_FS_BLKSIZE_DEFAULT = 0
const m_IOPOL_VFS_NOCACHE_WRITE_FS_BLKSIZE_ON = 1
const m_IOPOL_VFS_SKIP_MTIME_UPDATE_OFF = 0
const m_IOPOL_VFS_SKIP_MTIME_UPDATE_ON = 1
const m_IOPOL_VFS_STATFS_FORCE_NO_DATA_VOLUME = 1
const m_IOPOL_VFS_STATFS_NO_DATA_VOLUME_DEFAULT = 0
const m_IOPOL_VFS_TRIGGER_RESOLVE_DEFAULT = 0
const m_IOPOL_VFS_TRIGGER_RESOLVE_OFF = 1
const m_LOCK_EX = 0x02
const m_LOCK_NB = 0x04
const m_LOCK_SH = 0x01
const m_LOCK_UN = 0x08
const m_L_INCR = "SEEK_CUR"
const m_L_SET = "SEEK_SET"
const m_L_XTND = "SEEK_END"
const m_L_ctermid = 1024
const m_L_tmpnam = 1024
const m_MB_CUR_MAX = "__mb_cur_max"
const m_MINSIGSTKSZ = 32768
const m_NSIG = "__DARWIN_NSIG"
const m_O_ACCMODE = 0x0003
const m_O_ALERT = 0x20000000
const m_O_APPEND = 0x00000008
const m_O_ASYNC = 0x00000040
const m_O_CLOEXEC = 0x01000000
const m_O_CREAT = 0x00000200
const m_O_DIRECTORY = 0x00100000
const m_O_DP_AUTHENTICATE = 0x0004
const m_O_DP_GETRAWENCRYPTED = 0x0001
const m_O_DP_GETRAWUNENCRYPTED = 0x0002
const m_O_DSYNC = 0x400000
const m_O_EVTONLY = 0x00008000
const m_O_EXCL = 0x00000800
const m_O_EXEC = 0x40000000
const m_O_EXLOCK = 0x00000020
const m_O_FSYNC = "O_SYNC"
const m_O_NDELAY = "O_NONBLOCK"
const m_O_NOCTTY = 0x00020000
const m_O_NOFOLLOW = 0x00000100
const m_O_NOFOLLOW_ANY = 0x20000000
const m_O_NONBLOCK = 0x00000004
const m_O_POPUP = 0x80000000
const m_O_RDONLY = 0
const m_O_RDWR = 0x0002
const m_O_SHLOCK = 0x00000010
const m_O_SYMLINK = 0x00200000
const m_O_SYNC = 0x0080
const m_O_TRUNC = 0x00000400
const m_O_WRONLY = 0x0001
const m_POLL_ERR = 4
const m_POLL_HUP = 6
const m_POLL_IN = 1
const m_POLL_MSG = 3
const m_POLL_OUT = 2
const m_POLL_PRI = 5
const m_PRIO_DARWIN_BG = 0x1000
const m_PRIO_DARWIN_NONUI = 0x1001
const m_PRIO_DARWIN_PROCESS = 4
const m_PRIO_DARWIN_THREAD = 3
const m_PRIO_MAX = 20
const m_PRIO_PGRP = 1
const m_PRIO_PROCESS = 0
const m_PRIO_USER = 2
const m_P_tmpdir = "/var/tmp/"
const m_RAND_MAX = 0x7fffffff
const m_RENAME_EXCL = 0x00000004
const m_RENAME_NOFOLLOW_ANY = 0x00000010
const m_RENAME_RESERVED1 = 0x00000008
const m_RENAME_SECLUDE = 0x00000001
const m_RENAME_SWAP = 0x00000002
const m_RLIMIT_AS = 5
const m_RLIMIT_CORE = 4
const m_RLIMIT_CPU = 0
const m_RLIMIT_CPU_USAGE_MONITOR = 0x2
const m_RLIMIT_DATA = 2
const m_RLIMIT_FOOTPRINT_INTERVAL = 0x4
const m_RLIMIT_FSIZE = 1
const m_RLIMIT_MEMLOCK = 6
const m_RLIMIT_NOFILE = 8
const m_RLIMIT_NPROC = 7
const m_RLIMIT_RSS = "RLIMIT_AS"
const m_RLIMIT_STACK = 3
const m_RLIMIT_THREAD_CPULIMITS = 0x3
const m_RLIMIT_WAKEUPS_MONITOR = 0x1
const m_RLIM_NLIMITS = 9
const m_RLIM_SAVED_CUR = "RLIM_INFINITY"
const m_RLIM_SAVED_MAX = "RLIM_INFINITY"
const m_RUSAGE_INFO_CURRENT = "RUSAGE_INFO_V6"
const m_RUSAGE_INFO_V0 = 0
const m_RUSAGE_INFO_V1 = 1
const m_RUSAGE_INFO_V2 = 2
const m_RUSAGE_INFO_V3 = 3
const m_RUSAGE_INFO_V4 = 4
const m_RUSAGE_INFO_V5 = 5
const m_RUSAGE_INFO_V6 = 6
const m_RUSAGE_SELF = 0
const m_RU_PROC_RUNS_RESLIDE = 0x00000001
const m_SA_64REGSET = 0x0200
const m_SA_NOCLDSTOP = 0x0008
const m_SA_NOCLDWAIT = 0x0020
const m_SA_NODEFER = 0x0010
const m_SA_ONSTACK = 0x0001
const m_SA_RESETHAND = 0x0004
const m_SA_RESTART = 0x0002
const m_SA_SIGINFO = 0x0040
const m_SA_USERTRAMP = 0x0100
const m_SEEK_CUR = 1
const m_SEEK_DATA = 4
const m_SEEK_END = 2
const m_SEEK_HOLE = 3
const m_SEEK_SET = 0
const m_SEGV_ACCERR = 2
const m_SEGV_MAPERR = 1
const m_SEGV_NOOP = 0
const m_SF_APPEND = 0x00040000
const m_SF_ARCHIVED = 0x00010000
const m_SF_DATALESS = 0x40000000
const m_SF_FIRMLINK = 0x00800000
const m_SF_IMMUTABLE = 0x00020000
const m_SF_NOUNLINK = 0x00100000
const m_SF_RESTRICTED = 0x00080000
const m_SF_SETTABLE = 0x3fff0000
const m_SF_SUPPORTED = 0x009f0000
const m_SF_SYNTHETIC = 0xc0000000
const m_SIGABRT = 6
const m_SIGALRM = 14
const m_SIGBUS = 10
const m_SIGCHLD = 20
const m_SIGCONT = 19
const m_SIGEMT = 7
const m_SIGEV_NONE = 0
const m_SIGEV_SIGNAL = 1
const m_SIGEV_THREAD = 3
const m_SIGFPE = 8
const m_SIGHUP = 1
const m_SIGILL = 4
const m_SIGINFO = 29
const m_SIGINT = 2
const m_SIGIO = 23
const m_SIGIOT = "SIGABRT"
const m_SIGKILL = 9
const m_SIGPIPE = 13
const m_SIGPROF = 27
const m_SIGQUIT = 3
const m_SIGSEGV = 11
const m_SIGSTKSZ = 131072
const m_SIGSTOP = 17
const m_SIGSYS = 12
const m_SIGTERM = 15
const m_SIGTRAP = 5
const m_SIGTSTP = 18
const m_SIGTTIN = 21
const m_SIGTTOU = 22
const m_SIGURG = 16
const m_SIGUSR1 = 30
const m_SIGUSR2 = 31
const m_SIGVTALRM = 26
const m_SIGWINCH = 28
const m_SIGXCPU = 24
const m_SIGXFSZ = 25
const m_SIG_BLOCK = 1
const m_SIG_SETMASK = 3
const m_SIG_UNBLOCK = 2
const m_SI_ASYNCIO = 0x10004
const m_SI_MESGQ = 0x10005
const m_SI_QUEUE = 0x10002
const m_SI_TIMER = 0x10003
const m_SI_USER = 0x10001
const m_SS_DISABLE = 0x0004
const m_SS_ONSTACK = 0x0001
const m_STDERR_FILENO = 2
const m_STDIN_FILENO = 0
const m_STDOUT_FILENO = 1
const m_SV_INTERRUPT = "SA_RESTART"
const m_SV_NOCLDSTOP = "SA_NOCLDSTOP"
const m_SV_NODEFER = "SA_NODEFER"
const m_SV_ONSTACK = "SA_ONSTACK"
const m_SV_RESETHAND = "SA_RESETHAND"
const m_SV_SIGINFO = "SA_SIGINFO"
const m_SYNC_VOLUME_FULLSYNC = 0x01
const m_SYNC_VOLUME_WAIT = 0x02
const m_S_BLKSIZE = 512
const m_S_IEXEC = "S_IXUSR"
const m_S_IFBLK = 0060000
const m_S_IFCHR = 0020000
const m_S_IFDIR = 0040000
const m_S_IFIFO = 0010000
const m_S_IFLNK = 0120000
const m_S_IFMT = 0170000
const m_S_IFREG = 0100000
const m_S_IFSOCK = 0140000
const m_S_IFWHT = 0160000
const m_S_IREAD = "S_IRUSR"
const m_S_IRGRP = 0000040
const m_S_IROTH = 0000004
const m_S_IRUSR = 0000400
const m_S_IRWXG = 0000070
const m_S_IRWXO = 0000007
const m_S_IRWXU = 0000700
const m_S_ISGID = 0002000
const m_S_ISTXT = "S_ISVTX"
const m_S_ISUID = 0004000
const m_S_ISVTX = 0001000
const m_S_IWGRP = 0000020
const m_S_IWOTH = 0000002
const m_S_IWRITE = "S_IWUSR"
const m_S_IWUSR = 0000200
const m_S_IXGRP = 0000010
const m_S_IXOTH = 0000001
const m_S_IXUSR = 0000100
const m_TMP_MAX = 308915776
const m_TRAP_BRKPT = 1
const m_TRAP_TRACE = 2
const m_UF_APPEND = 0x00000004
const m_UF_COMPRESSED = 0x00000020
const m_UF_DATAVAULT = 0x00000080
const m_UF_HIDDEN = 0x00008000
const m_UF_IMMUTABLE = 0x00000002
const m_UF_NODUMP = 0x00000001
const m_UF_OPAQUE = 0x00000008
const m_UF_SETTABLE = 0x0000ffff
const m_UF_TRACKED = 0x00000040
const m_USER_FSIGNATURES_CDHASH_LEN = 20
const m_WAIT_MYPGRP = 0
const m_WAKEMON_DISABLE = 0x02
const m_WAKEMON_ENABLE = 0x01
const m_WAKEMON_GET_PARAMS = 0x04
const m_WAKEMON_MAKE_FATAL = 0x10
const m_WAKEMON_SET_DEFAULTS = 0x08
const m_WCONTINUED = 0x00000010
const m_WCOREFLAG = 0200
const m_WEXITED = 0x00000004
const m_WNOHANG = 0x00000001
const m_WNOWAIT = 0x00000020
const m_WSTOPPED = 0x00000008
const m_WUNTRACED = 0x00000002
const m__CS_DARWIN_USER_CACHE_DIR = 65538
const m__CS_DARWIN_USER_DIR = 65536
const m__CS_DARWIN_USER_TEMP_DIR = 65537
const m__CS_PATH = 1
const m__CS_POSIX_V6_ILP32_OFF32_CFLAGS = 2
const m__CS_POSIX_V6_ILP32_OFF32_LDFLAGS = 3
const m__CS_POSIX_V6_ILP32_OFF32_LIBS = 4
const m__CS_POSIX_V6_ILP32_OFFBIG_CFLAGS = 5
const m__CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS = 6
const m__CS_POSIX_V6_ILP32_OFFBIG_LIBS = 7
const m__CS_POSIX_V6_LP64_OFF64_CFLAGS = 8
const m__CS_POSIX_V6_LP64_OFF64_LDFLAGS = 9
const m__CS_POSIX_V6_LP64_OFF64_LIBS = 10
const m__CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS = 11
const m__CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS = 12
const m__CS_POSIX_V6_LPBIG_OFFBIG_LIBS = 13
const m__CS_POSIX_V6_WIDTH_RESTRICTED_ENVS = 14
const m__CS_XBS5_ILP32_OFF32_CFLAGS = 20
const m__CS_XBS5_ILP32_OFF32_LDFLAGS = 21
const m__CS_XBS5_ILP32_OFF32_LIBS = 22
const m__CS_XBS5_ILP32_OFF32_LINTFLAGS = 23
const m__CS_XBS5_ILP32_OFFBIG_CFLAGS = 24
const m__CS_XBS5_ILP32_OFFBIG_LDFLAGS = 25
const m__CS_XBS5_ILP32_OFFBIG_LIBS = 26
const m__CS_XBS5_ILP32_OFFBIG_LINTFLAGS = 27
const m__CS_XBS5_LP64_OFF64_CFLAGS = 28
const m__CS_XBS5_LP64_OFF64_LDFLAGS = 29
const m__CS_XBS5_LP64_OFF64_LIBS = 30
const m__CS_XBS5_LP64_OFF64_LINTFLAGS = 31
const m__CS_XBS5_LPBIG_OFFBIG_CFLAGS = 32
const m__CS_XBS5_LPBIG_OFFBIG_LDFLAGS = 33
const m__CS_XBS5_LPBIG_OFFBIG_LIBS = 34
const m__CS_XBS5_LPBIG_OFFBIG_LINTFLAGS = 35
const m__I386_SIGNAL_H_ = 1
const m__IOFBF = 0
const m__IOLBF = 1
const m__IONBF = 2
const m__PC_2_SYMLINKS = 15
const m__PC_ALLOC_SIZE_MIN = 16
const m__PC_ASYNC_IO = 17
const m__PC_AUTH_OPAQUE_NP = 14
const m__PC_CASE_PRESERVING = 12
const m__PC_CASE_SENSITIVE = 11
const m__PC_CHOWN_RESTRICTED = 7
const m__PC_EXTENDED_SECURITY_NP = 13
const m__PC_FILESIZEBITS = 18
const m__PC_LINK_MAX = 1
const m__PC_MAX_CANON = 2
const m__PC_MAX_INPUT = 3
const m__PC_MIN_HOLE_SIZE = 27
const m__PC_NAME_CHARS_MAX = 10
const m__PC_NAME_MAX = 4
const m__PC_NO_TRUNC = 8
const m__PC_PATH_MAX = 5
const m__PC_PIPE_BUF = 6
const m__PC_PRIO_IO = 19
const m__PC_REC_INCR_XFER_SIZE = 20
const m__PC_REC_MAX_XFER_SIZE = 21
const m__PC_REC_MIN_XFER_SIZE = 22
const m__PC_REC_XFER_ALIGN = 23
const m__PC_SYMLINK_MAX = 24
const m__PC_SYNC_IO = 25
const m__PC_VDISABLE = 9
const m__PC_XATTR_SIZE_BITS = 26
const m__POSIX2_CHAR_TERM = 200112
const m__POSIX2_C_BIND = 200112
const m__POSIX2_C_DEV = 200112
const m__POSIX2_FORT_RUN = 200112
const m__POSIX2_LOCALEDEF = 200112
const m__POSIX2_SW_DEV = 200112
const m__POSIX2_UPE = 200112
const m__POSIX2_VERSION = 200112
const m__POSIX_CHOWN_RESTRICTED = 200112
const m__POSIX_FSYNC = 200112
const m__POSIX_IPV6 = 200112
const m__POSIX_JOB_CONTROL = 200112
const m__POSIX_MAPPED_FILES = 200112
const m__POSIX_MEMORY_PROTECTION = 200112
const m__POSIX_NO_TRUNC = 200112
const m__POSIX_READER_WRITER_LOCKS = 200112
const m__POSIX_REGEXP = 200112
const m__POSIX_SAVED_IDS = 200112
const m__POSIX_SHELL = 200112
const m__POSIX_SPAWN = 200112
const m__POSIX_THREADS = 200112
const m__POSIX_THREAD_ATTR_STACKADDR = 200112
const m__POSIX_THREAD_ATTR_STACKSIZE = 200112
const m__POSIX_THREAD_KEYS_MAX = 128
const m__POSIX_THREAD_PROCESS_SHARED = 200112
const m__POSIX_THREAD_SAFE_FUNCTIONS = 200112
const m__POSIX_V6_ILP32_OFF32 = "__ILP32_OFF32"
const m__POSIX_V6_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__POSIX_V6_LP64_OFF64 = "__LP64_OFF64"
const m__POSIX_V6_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__POSIX_V7_ILP32_OFF32 = "__ILP32_OFF32"
const m__POSIX_V7_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__POSIX_V7_LP64_OFF64 = "__LP64_OFF64"
const m__POSIX_V7_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__POSIX_VERSION = 200112
const m__RLIMIT_POSIX_FLAG = 0x1000
const m__SC_2_CHAR_TERM = 20
const m__SC_2_C_BIND = 18
const m__SC_2_C_DEV = 19
const m__SC_2_FORT_DEV = 21
const m__SC_2_FORT_RUN = 22
const m__SC_2_LOCALEDEF = 23
const m__SC_2_PBS = 59
const m__SC_2_PBS_ACCOUNTING = 60
const m__SC_2_PBS_CHECKPOINT = 61
const m__SC_2_PBS_LOCATE = 62
const m__SC_2_PBS_MESSAGE = 63
const m__SC_2_PBS_TRACK = 64
const m__SC_2_SW_DEV = 24
const m__SC_2_UPE = 25
const m__SC_2_VERSION = 17
const m__SC_ADVISORY_INFO = 65
const m__SC_AIO_LISTIO_MAX = 42
const m__SC_AIO_MAX = 43
const m__SC_AIO_PRIO_DELTA_MAX = 44
const m__SC_ARG_MAX = 1
const m__SC_ASYNCHRONOUS_IO = 28
const m__SC_ATEXIT_MAX = 107
const m__SC_BARRIERS = 66
const m__SC_BC_BASE_MAX = 9
const m__SC_BC_DIM_MAX = 10
const m__SC_BC_SCALE_MAX = 11
const m__SC_BC_STRING_MAX = 12
const m__SC_CHILD_MAX = 2
const m__SC_CLK_TCK = 3
const m__SC_CLOCK_SELECTION = 67
const m__SC_COLL_WEIGHTS_MAX = 13
const m__SC_CPUTIME = 68
const m__SC_DELAYTIMER_MAX = 45
const m__SC_EXPR_NEST_MAX = 14
const m__SC_FILE_LOCKING = 69
const m__SC_FSYNC = 38
const m__SC_GETGR_R_SIZE_MAX = 70
const m__SC_GETPW_R_SIZE_MAX = 71
const m__SC_HOST_NAME_MAX = 72
const m__SC_IOV_MAX = 56
const m__SC_IPV6 = 118
const m__SC_JOB_CONTROL = 6
const m__SC_LINE_MAX = 15
const m__SC_LOGIN_NAME_MAX = 73
const m__SC_MAPPED_FILES = 47
const m__SC_MEMLOCK = 30
const m__SC_MEMLOCK_RANGE = 31
const m__SC_MEMORY_PROTECTION = 32
const m__SC_MESSAGE_PASSING = 33
const m__SC_MONOTONIC_CLOCK = 74
const m__SC_MQ_OPEN_MAX = 46
const m__SC_MQ_PRIO_MAX = 75
const m__SC_NGROUPS_MAX = 4
const m__SC_NPROCESSORS_CONF = 57
const m__SC_NPROCESSORS_ONLN = 58
const m__SC_OPEN_MAX = 5
const m__SC_PAGESIZE = 29
const m__SC_PAGE_SIZE = "_SC_PAGESIZE"
const m__SC_PASS_MAX = 131
const m__SC_PHYS_PAGES = 200
const m__SC_PRIORITIZED_IO = 34
const m__SC_PRIORITY_SCHEDULING = 35
const m__SC_RAW_SOCKETS = 119
const m__SC_READER_WRITER_LOCKS = 76
const m__SC_REALTIME_SIGNALS = 36
const m__SC_REGEXP = 77
const m__SC_RE_DUP_MAX = 16
const m__SC_RTSIG_MAX = 48
const m__SC_SAVED_IDS = 7
const m__SC_SEMAPHORES = 37
const m__SC_SEM_NSEMS_MAX = 49
const m__SC_SEM_VALUE_MAX = 50
const m__SC_SHARED_MEMORY_OBJECTS = 39
const m__SC_SHELL = 78
const m__SC_SIGQUEUE_MAX = 51
const m__SC_SPAWN = 79
const m__SC_SPIN_LOCKS = 80
const m__SC_SPORADIC_SERVER = 81
const m__SC_SS_REPL_MAX = 126
const m__SC_STREAM_MAX = 26
const m__SC_SYMLOOP_MAX = 120
const m__SC_SYNCHRONIZED_IO = 40
const m__SC_THREADS = 96
const m__SC_THREAD_ATTR_STACKADDR = 82
const m__SC_THREAD_ATTR_STACKSIZE = 83
const m__SC_THREAD_CPUTIME = 84
const m__SC_THREAD_DESTRUCTOR_ITERATIONS = 85
const m__SC_THREAD_KEYS_MAX = 86
const m__SC_THREAD_PRIORITY_SCHEDULING = 89
const m__SC_THREAD_PRIO_INHERIT = 87
const m__SC_THREAD_PRIO_PROTECT = 88
const m__SC_THREAD_PROCESS_SHARED = 90
const m__SC_THREAD_SAFE_FUNCTIONS = 91
const m__SC_THREAD_SPORADIC_SERVER = 92
const m__SC_THREAD_STACK_MIN = 93
const m__SC_THREAD_THREADS_MAX = 94
const m__SC_TIMEOUTS = 95
const m__SC_TIMERS = 41
const m__SC_TIMER_MAX = 52
const m__SC_TRACE = 97
const m__SC_TRACE_EVENT_FILTER = 98
const m__SC_TRACE_EVENT_NAME_MAX = 127
const m__SC_TRACE_INHERIT = 99
const m__SC_TRACE_LOG = 100
const m__SC_TRACE_NAME_MAX = 128
const m__SC_TRACE_SYS_MAX = 129
const m__SC_TRACE_USER_EVENT_MAX = 130
const m__SC_TTY_NAME_MAX = 101
const m__SC_TYPED_MEMORY_OBJECTS = 102
const m__SC_TZNAME_MAX = 27
const m__SC_V6_ILP32_OFF32 = 103
const m__SC_V6_ILP32_OFFBIG = 104
const m__SC_V6_LP64_OFF64 = 105
const m__SC_V6_LPBIG_OFFBIG = 106
const m__SC_VERSION = 8
const m__SC_XBS5_ILP32_OFF32 = 122
const m__SC_XBS5_ILP32_OFFBIG = 123
const m__SC_XBS5_LP64_OFF64 = 124
const m__SC_XBS5_LPBIG_OFFBIG = 125
const m__SC_XOPEN_CRYPT = 108
const m__SC_XOPEN_ENH_I18N = 109
const m__SC_XOPEN_LEGACY = 110
const m__SC_XOPEN_REALTIME = 111
const m__SC_XOPEN_REALTIME_THREADS = 112
const m__SC_XOPEN_SHM = 113
const m__SC_XOPEN_STREAMS = 114
const m__SC_XOPEN_UNIX = 115
const m__SC_XOPEN_VERSION = 116
const m__SC_XOPEN_XCU_VERSION = 121
const m__STRUCT_MCONTEXT = "_STRUCT_MCONTEXT64"
const m__V6_ILP32_OFF32 = "__ILP32_OFF32"
const m__V6_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__V6_LP64_OFF64 = "__LP64_OFF64"
const m__V6_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__WSTOPPED = 0177
const m__X86_INSTRUCTION_STATE_CACHELINE_SIZE = 64
const m__XBS5_ILP32_OFF32 = "__ILP32_OFF32"
const m__XBS5_ILP32_OFFBIG = "__ILP32_OFFBIG"
const m__XBS5_LP64_OFF64 = "__LP64_OFF64"
const m__XBS5_LPBIG_OFFBIG = "__LPBIG_OFFBIG"
const m__XOPEN_CRYPT = 1
const m__XOPEN_ENH_I18N = 1
const m__XOPEN_SHM = 1
const m__XOPEN_UNIX = 1
const m__XOPEN_VERSION = 600
const m__XOPEN_XCU_VERSION = 4
const m___DARWIN_NSIG = 32
const m___LASTBRANCH_MAX = 32
const m___LP64_OFF64 = 1
const m___LPBIG_OFFBIG = 1
const m___SALC = 0x4000
const m___SAPP = 0x0100
const m___SEOF = 0x0020
const m___SERR = 0x0040
const m___SIGN = 0x8000
const m___SLBF = 0x0001
const m___SMBF = 0x0080
const m___SMOD = 0x2000
const m___SNBF = 0x0002
const m___SNPT = 0x0800
const m___SOFF = 0x1000
const m___SOPT = 0x0400
const m___SRD = 0x0004
const m___SRW = 0x0010
const m___SSTR = 0x0200
const m___SWR = 0x0008
const m_ru_first = "ru_ixrss"
const m_ru_last = "ru_nivcsw"
const m_stderr = "__stderrp"
const m_stdin = "__stdinp"
const m_stdout = "__stdoutp"
const m_sv_onstack = "sv_flags"

type Ttimespec = struct {
	Ftv_sec  t__darwin_time_t
	Ftv_nsec int64
}

type Tostat = struct {
	Fst_dev       t__uint16_t
	Fst_ino       Tino_t
	Fst_mode      Tmode_t
	Fst_nlink     Tnlink_t
	Fst_uid       t__uint16_t
	Fst_gid       t__uint16_t
	Fst_rdev      t__uint16_t
	Fst_size      t__int32_t
	Fst_atimespec Ttimespec
	Fst_mtimespec Ttimespec
	Fst_ctimespec Ttimespec
	Fst_blksize   t__int32_t
	Fst_blocks    t__int32_t
	Fst_flags     t__uint32_t
	Fst_gen       t__uint32_t
}

type Tstat = struct {
	Fst_dev           Tdev_t
	Fst_mode          Tmode_t
	Fst_nlink         Tnlink_t
	Fst_ino           t__darwin_ino64_t
	Fst_uid           Tuid_t
	Fst_gid           Tgid_t
	Fst_rdev          Tdev_t
	Fst_atimespec     Ttimespec
	Fst_mtimespec     Ttimespec
	Fst_ctimespec     Ttimespec
	Fst_birthtimespec Ttimespec
	Fst_size          Toff_t
	Fst_blocks        Tblkcnt_t
	Fst_blksize       Tblksize_t
	Fst_flags         t__uint32_t
	Fst_gen           t__uint32_t
	Fst_lspare        t__int32_t
	Fst_qspare        [2]t__int64_t
}

type Tstat64 = struct {
	Fst_dev           Tdev_t
	Fst_mode          Tmode_t
	Fst_nlink         Tnlink_t
	Fst_ino           t__darwin_ino64_t
	Fst_uid           Tuid_t
	Fst_gid           Tgid_t
	Fst_rdev          Tdev_t
	Fst_atimespec     Ttimespec
	Fst_mtimespec     Ttimespec
	Fst_ctimespec     Ttimespec
	Fst_birthtimespec Ttimespec
	Fst_size          Toff_t
	Fst_blocks        Tblkcnt_t
	Fst_blksize       Tblksize_t
	Fst_flags         t__uint32_t
	Fst_gen           t__uint32_t
	Fst_lspare        t__int32_t
	Fst_qspare        [2]t__int64_t
}

type Tfilesec_t = uintptr

type Tflock = struct {
	Fl_start  Toff_t
	Fl_len    Toff_t
	Fl_pid    Tpid_t
	Fl_type   int16
	Fl_whence int16
}

type Tflocktimeout = struct {
	Ffl      Tflock
	Ftimeout Ttimespec
}

type Tradvisory = struct {
	Fra_offset Toff_t
	Fra_count  int32
}

type Tfsignatures_t = struct {
	Ffs_file_start       Toff_t
	Ffs_blob_start       uintptr
	Ffs_blob_size        Tsize_t
	Ffs_fsignatures_size Tsize_t
	Ffs_cdhash           [20]int8
	Ffs_hash_type        int32
}

type Tfsignatures = Tfsignatures_t

type Tfsupplement_t = struct {
	Ffs_file_start Toff_t
	Ffs_blob_start Toff_t
	Ffs_blob_size  Tsize_t
	Ffs_orig_fd    int32
}

type Tfsupplement = Tfsupplement_t

type Tfchecklv_t = struct {
	Flv_file_start         Toff_t
	Flv_error_message_size Tsize_t
	Flv_error_message      uintptr
}

type Tfchecklv = Tfchecklv_t

type Tfgetsigsinfo_t = struct {
	Ffg_file_start      Toff_t
	Ffg_info_request    int32
	Ffg_sig_is_platform int32
}

type Tfgetsigsinfo = Tfgetsigsinfo_t

type Tfstore_t = struct {
	Ffst_flags      uint32
	Ffst_posmode    int32
	Ffst_offset     Toff_t
	Ffst_length     Toff_t
	Ffst_bytesalloc Toff_t
}

type Tfstore = Tfstore_t

type Tfpunchhole_t = struct {
	Ffp_flags  uint32
	Freserved  uint32
	Ffp_offset Toff_t
	Ffp_length Toff_t
}

type Tfpunchhole = Tfpunchhole_t

type Tftrimactivefile_t = struct {
	Ffta_offset Toff_t
	Ffta_length Toff_t
}

type Tftrimactivefile = Tftrimactivefile_t

type Tfspecread_t = struct {
	Ffsr_flags  uint32
	Freserved   uint32
	Ffsr_offset Toff_t
	Ffsr_length Toff_t
}

type Tfspecread = Tfspecread_t

type Tfattributiontag_t = struct {
	Fft_flags            uint32
	Fft_hash             uint64
	Fft_attribution_name [255]int8
}

type Tfattributiontag = Tfattributiontag_t

type Tlog2phys = struct {
	Fl2p_flags       uint32
	Fl2p_contigbytes Toff_t
	Fl2p_devoffset   Toff_t
}

type Tfilesec_property_t = int32

const _FILESEC_OWNER = 1
const _FILESEC_GROUP = 2
const _FILESEC_UUID = 3
const _FILESEC_MODE = 4
const _FILESEC_ACL = 5
const _FILESEC_GRPUUID = 6
const _FILESEC_ACL_RAW = 100
const _FILESEC_ACL_ALLOCSIZE = 101

type Tidtype_t = int32

const _P_ALL = 0
const _P_PID = 1
const _P_PGID = 2

type Tsig_atomic_t = int32

type t__darwin_i386_thread_state = struct {
	F__eax    uint32
	F__ebx    uint32
	F__ecx    uint32
	F__edx    uint32
	F__edi    uint32
	F__esi    uint32
	F__ebp    uint32
	F__esp    uint32
	F__ss     uint32
	F__eflags uint32
	F__eip    uint32
	F__cs     uint32
	F__ds     uint32
	F__es     uint32
	F__fs     uint32
	F__gs     uint32
}

type t__darwin_fp_control = struct {
	F__ccgo0 uint16
}

type t__darwin_fp_control_t = struct {
	F__ccgo0 uint16
}

type t__darwin_fp_status = struct {
	F__ccgo0 uint16
}

type t__darwin_fp_status_t = struct {
	F__ccgo0 uint16
}

type t__darwin_mmst_reg = struct {
	F__mmst_reg  [10]int8
	F__mmst_rsrv [6]int8
}

type t__darwin_xmm_reg = struct {
	F__xmm_reg [16]int8
}

type t__darwin_ymm_reg = struct {
	F__ymm_reg [32]int8
}

type t__darwin_zmm_reg = struct {
	F__zmm_reg [64]int8
}

type t__darwin_opmask_reg = struct {
	F__opmask_reg [8]int8
}

type t__darwin_i386_float_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
}

type t__darwin_i386_avx_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     t__darwin_xmm_reg
	F__fpu_ymmh1     t__darwin_xmm_reg
	F__fpu_ymmh2     t__darwin_xmm_reg
	F__fpu_ymmh3     t__darwin_xmm_reg
	F__fpu_ymmh4     t__darwin_xmm_reg
	F__fpu_ymmh5     t__darwin_xmm_reg
	F__fpu_ymmh6     t__darwin_xmm_reg
	F__fpu_ymmh7     t__darwin_xmm_reg
}

type t__darwin_i386_avx512_state = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_rsrv4     [224]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     t__darwin_xmm_reg
	F__fpu_ymmh1     t__darwin_xmm_reg
	F__fpu_ymmh2     t__darwin_xmm_reg
	F__fpu_ymmh3     t__darwin_xmm_reg
	F__fpu_ymmh4     t__darwin_xmm_reg
	F__fpu_ymmh5     t__darwin_xmm_reg
	F__fpu_ymmh6     t__darwin_xmm_reg
	F__fpu_ymmh7     t__darwin_xmm_reg
	F__fpu_k0        t__darwin_opmask_reg
	F__fpu_k1        t__darwin_opmask_reg
	F__fpu_k2        t__darwin_opmask_reg
	F__fpu_k3        t__darwin_opmask_reg
	F__fpu_k4        t__darwin_opmask_reg
	F__fpu_k5        t__darwin_opmask_reg
	F__fpu_k6        t__darwin_opmask_reg
	F__fpu_k7        t__darwin_opmask_reg
	F__fpu_zmmh0     t__darwin_ymm_reg
	F__fpu_zmmh1     t__darwin_ymm_reg
	F__fpu_zmmh2     t__darwin_ymm_reg
	F__fpu_zmmh3     t__darwin_ymm_reg
	F__fpu_zmmh4     t__darwin_ymm_reg
	F__fpu_zmmh5     t__darwin_ymm_reg
	F__fpu_zmmh6     t__darwin_ymm_reg
	F__fpu_zmmh7     t__darwin_ymm_reg
}

type t__darwin_i386_exception_state = struct {
	F__trapno     t__uint16_t
	F__cpu        t__uint16_t
	F__err        t__uint32_t
	F__faultvaddr t__uint32_t
}

type t__darwin_x86_debug_state32 = struct {
	F__dr0 uint32
	F__dr1 uint32
	F__dr2 uint32
	F__dr3 uint32
	F__dr4 uint32
	F__dr5 uint32
	F__dr6 uint32
	F__dr7 uint32
}

type t__x86_instruction_state = struct {
	F__insn_stream_valid_bytes int32
	F__insn_offset             int32
	F__out_of_synch            int32
	F__insn_bytes              [2380]t__uint8_t
	F__insn_cacheline          [64]t__uint8_t
}

type t__last_branch_record = struct {
	F__from_ip t__uint64_t
	F__to_ip   t__uint64_t
	F__ccgo16  uint32
}

type t__last_branch_state = struct {
	F__lbr_count int32
	F__ccgo4     uint32
	F__lbrs      [32]t__last_branch_record
}

type t__x86_pagein_state = struct {
	F__pagein_error int32
}

type t__darwin_x86_thread_state64 = struct {
	F__rax    t__uint64_t
	F__rbx    t__uint64_t
	F__rcx    t__uint64_t
	F__rdx    t__uint64_t
	F__rdi    t__uint64_t
	F__rsi    t__uint64_t
	F__rbp    t__uint64_t
	F__rsp    t__uint64_t
	F__r8     t__uint64_t
	F__r9     t__uint64_t
	F__r10    t__uint64_t
	F__r11    t__uint64_t
	F__r12    t__uint64_t
	F__r13    t__uint64_t
	F__r14    t__uint64_t
	F__r15    t__uint64_t
	F__rip    t__uint64_t
	F__rflags t__uint64_t
	F__cs     t__uint64_t
	F__fs     t__uint64_t
	F__gs     t__uint64_t
}

type t__darwin_x86_thread_full_state64 = struct {
	F__ss64   t__darwin_x86_thread_state64
	F__ds     t__uint64_t
	F__es     t__uint64_t
	F__ss     t__uint64_t
	F__gsbase t__uint64_t
}

type t__darwin_x86_float_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_xmm8      t__darwin_xmm_reg
	F__fpu_xmm9      t__darwin_xmm_reg
	F__fpu_xmm10     t__darwin_xmm_reg
	F__fpu_xmm11     t__darwin_xmm_reg
	F__fpu_xmm12     t__darwin_xmm_reg
	F__fpu_xmm13     t__darwin_xmm_reg
	F__fpu_xmm14     t__darwin_xmm_reg
	F__fpu_xmm15     t__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
}

type t__darwin_x86_avx_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_xmm8      t__darwin_xmm_reg
	F__fpu_xmm9      t__darwin_xmm_reg
	F__fpu_xmm10     t__darwin_xmm_reg
	F__fpu_xmm11     t__darwin_xmm_reg
	F__fpu_xmm12     t__darwin_xmm_reg
	F__fpu_xmm13     t__darwin_xmm_reg
	F__fpu_xmm14     t__darwin_xmm_reg
	F__fpu_xmm15     t__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     t__darwin_xmm_reg
	F__fpu_ymmh1     t__darwin_xmm_reg
	F__fpu_ymmh2     t__darwin_xmm_reg
	F__fpu_ymmh3     t__darwin_xmm_reg
	F__fpu_ymmh4     t__darwin_xmm_reg
	F__fpu_ymmh5     t__darwin_xmm_reg
	F__fpu_ymmh6     t__darwin_xmm_reg
	F__fpu_ymmh7     t__darwin_xmm_reg
	F__fpu_ymmh8     t__darwin_xmm_reg
	F__fpu_ymmh9     t__darwin_xmm_reg
	F__fpu_ymmh10    t__darwin_xmm_reg
	F__fpu_ymmh11    t__darwin_xmm_reg
	F__fpu_ymmh12    t__darwin_xmm_reg
	F__fpu_ymmh13    t__darwin_xmm_reg
	F__fpu_ymmh14    t__darwin_xmm_reg
	F__fpu_ymmh15    t__darwin_xmm_reg
}

type t__darwin_x86_avx512_state64 = struct {
	F__fpu_reserved  [2]int32
	F__fpu_fcw       t__darwin_fp_control
	F__fpu_fsw       t__darwin_fp_status
	F__fpu_ftw       t__uint8_t
	F__fpu_rsrv1     t__uint8_t
	F__fpu_fop       t__uint16_t
	F__fpu_ip        t__uint32_t
	F__fpu_cs        t__uint16_t
	F__fpu_rsrv2     t__uint16_t
	F__fpu_dp        t__uint32_t
	F__fpu_ds        t__uint16_t
	F__fpu_rsrv3     t__uint16_t
	F__fpu_mxcsr     t__uint32_t
	F__fpu_mxcsrmask t__uint32_t
	F__fpu_stmm0     t__darwin_mmst_reg
	F__fpu_stmm1     t__darwin_mmst_reg
	F__fpu_stmm2     t__darwin_mmst_reg
	F__fpu_stmm3     t__darwin_mmst_reg
	F__fpu_stmm4     t__darwin_mmst_reg
	F__fpu_stmm5     t__darwin_mmst_reg
	F__fpu_stmm6     t__darwin_mmst_reg
	F__fpu_stmm7     t__darwin_mmst_reg
	F__fpu_xmm0      t__darwin_xmm_reg
	F__fpu_xmm1      t__darwin_xmm_reg
	F__fpu_xmm2      t__darwin_xmm_reg
	F__fpu_xmm3      t__darwin_xmm_reg
	F__fpu_xmm4      t__darwin_xmm_reg
	F__fpu_xmm5      t__darwin_xmm_reg
	F__fpu_xmm6      t__darwin_xmm_reg
	F__fpu_xmm7      t__darwin_xmm_reg
	F__fpu_xmm8      t__darwin_xmm_reg
	F__fpu_xmm9      t__darwin_xmm_reg
	F__fpu_xmm10     t__darwin_xmm_reg
	F__fpu_xmm11     t__darwin_xmm_reg
	F__fpu_xmm12     t__darwin_xmm_reg
	F__fpu_xmm13     t__darwin_xmm_reg
	F__fpu_xmm14     t__darwin_xmm_reg
	F__fpu_xmm15     t__darwin_xmm_reg
	F__fpu_rsrv4     [96]int8
	F__fpu_reserved1 int32
	F__avx_reserved1 [64]int8
	F__fpu_ymmh0     t__darwin_xmm_reg
	F__fpu_ymmh1     t__darwin_xmm_reg
	F__fpu_ymmh2     t__darwin_xmm_reg
	F__fpu_ymmh3     t__darwin_xmm_reg
	F__fpu_ymmh4     t__darwin_xmm_reg
	F__fpu_ymmh5     t__darwin_xmm_reg
	F__fpu_ymmh6     t__darwin_xmm_reg
	F__fpu_ymmh7     t__darwin_xmm_reg
	F__fpu_ymmh8     t__darwin_xmm_reg
	F__fpu_ymmh9     t__darwin_xmm_reg
	F__fpu_ymmh10    t__darwin_xmm_reg
	F__fpu_ymmh11    t__darwin_xmm_reg
	F__fpu_ymmh12    t__darwin_xmm_reg
	F__fpu_ymmh13    t__darwin_xmm_reg
	F__fpu_ymmh14    t__darwin_xmm_reg
	F__fpu_ymmh15    t__darwin_xmm_reg
	F__fpu_k0        t__darwin_opmask_reg
	F__fpu_k1        t__darwin_opmask_reg
	F__fpu_k2        t__darwin_opmask_reg
	F__fpu_k3        t__darwin_opmask_reg
	F__fpu_k4        t__darwin_opmask_reg
	F__fpu_k5        t__darwin_opmask_reg
	F__fpu_k6        t__darwin_opmask_reg
	F__fpu_k7        t__darwin_opmask_reg
	F__fpu_zmmh0     t__darwin_ymm_reg
	F__fpu_zmmh1     t__darwin_ymm_reg
	F__fpu_zmmh2     t__darwin_ymm_reg
	F__fpu_zmmh3     t__darwin_ymm_reg
	F__fpu_zmmh4     t__darwin_ymm_reg
	F__fpu_zmmh5     t__darwin_ymm_reg
	F__fpu_zmmh6     t__darwin_ymm_reg
	F__fpu_zmmh7     t__darwin_ymm_reg
	F__fpu_zmmh8     t__darwin_ymm_reg
	F__fpu_zmmh9     t__darwin_ymm_reg
	F__fpu_zmmh10    t__darwin_ymm_reg
	F__fpu_zmmh11    t__darwin_ymm_reg
	F__fpu_zmmh12    t__darwin_ymm_reg
	F__fpu_zmmh13    t__darwin_ymm_reg
	F__fpu_zmmh14    t__darwin_ymm_reg
	F__fpu_zmmh15    t__darwin_ymm_reg
	F__fpu_zmm16     t__darwin_zmm_reg
	F__fpu_zmm17     t__darwin_zmm_reg
	F__fpu_zmm18     t__darwin_zmm_reg
	F__fpu_zmm19     t__darwin_zmm_reg
	F__fpu_zmm20     t__darwin_zmm_reg
	F__fpu_zmm21     t__darwin_zmm_reg
	F__fpu_zmm22     t__darwin_zmm_reg
	F__fpu_zmm23     t__darwin_zmm_reg
	F__fpu_zmm24     t__darwin_zmm_reg
	F__fpu_zmm25     t__darwin_zmm_reg
	F__fpu_zmm26     t__darwin_zmm_reg
	F__fpu_zmm27     t__darwin_zmm_reg
	F__fpu_zmm28     t__darwin_zmm_reg
	F__fpu_zmm29     t__darwin_zmm_reg
	F__fpu_zmm30     t__darwin_zmm_reg
	F__fpu_zmm31     t__darwin_zmm_reg
}

type t__darwin_x86_exception_state64 = struct {
	F__trapno     t__uint16_t
	F__cpu        t__uint16_t
	F__err        t__uint32_t
	F__faultvaddr t__uint64_t
}

type t__darwin_x86_debug_state64 = struct {
	F__dr0 t__uint64_t
	F__dr1 t__uint64_t
	F__dr2 t__uint64_t
	F__dr3 t__uint64_t
	F__dr4 t__uint64_t
	F__dr5 t__uint64_t
	F__dr6 t__uint64_t
	F__dr7 t__uint64_t
}

type t__darwin_x86_cpmu_state64 = struct {
	F__ctrs [16]t__uint64_t
}

type t__darwin_mcontext32 = struct {
	F__es t__darwin_i386_exception_state
	F__ss t__darwin_i386_thread_state
	F__fs t__darwin_i386_float_state
}

type t__darwin_mcontext_avx32 = struct {
	F__es t__darwin_i386_exception_state
	F__ss t__darwin_i386_thread_state
	F__fs t__darwin_i386_avx_state
}

type t__darwin_mcontext_avx512_32 = struct {
	F__es t__darwin_i386_exception_state
	F__ss t__darwin_i386_thread_state
	F__fs t__darwin_i386_avx512_state
}

type t__darwin_mcontext64 = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_state64
	F__fs t__darwin_x86_float_state64
}

type t__darwin_mcontext64_full = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_full_state64
	F__fs t__darwin_x86_float_state64
}

type t__darwin_mcontext_avx64 = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_state64
	F__fs t__darwin_x86_avx_state64
}

type t__darwin_mcontext_avx64_full = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_full_state64
	F__fs t__darwin_x86_avx_state64
}

type t__darwin_mcontext_avx512_64 = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_state64
	F__fs t__darwin_x86_avx512_state64
}

type t__darwin_mcontext_avx512_64_full = struct {
	F__es t__darwin_x86_exception_state64
	F__ss t__darwin_x86_thread_full_state64
	F__fs t__darwin_x86_avx512_state64
}

type Tmcontext_t = uintptr

type t__darwin_sigaltstack = struct {
	Fss_sp    uintptr
	Fss_size  t__darwin_size_t
	Fss_flags int32
}

type Tstack_t = struct {
	Fss_sp    uintptr
	Fss_size  t__darwin_size_t
	Fss_flags int32
}

type t__darwin_ucontext = struct {
	Fuc_onstack  int32
	Fuc_sigmask  t__darwin_sigset_t
	Fuc_stack    t__darwin_sigaltstack
	Fuc_link     uintptr
	Fuc_mcsize   t__darwin_size_t
	Fuc_mcontext uintptr
}

type Tucontext_t = struct {
	Fuc_onstack  int32
	Fuc_sigmask  t__darwin_sigset_t
	Fuc_stack    t__darwin_sigaltstack
	Fuc_link     uintptr
	Fuc_mcsize   t__darwin_size_t
	Fuc_mcontext uintptr
}

type Tsigset_t = uint32

type Tsigval = struct {
	Fsival_ptr   [0]uintptr
	Fsival_int   int32
	F__ccgo_pad2 [4]byte
}

type Tsigevent = struct {
	Fsigev_notify            int32
	Fsigev_signo             int32
	Fsigev_value             Tsigval
	Fsigev_notify_function   uintptr
	Fsigev_notify_attributes uintptr
}

type Tsiginfo_t = struct {
	Fsi_signo  int32
	Fsi_errno  int32
	Fsi_code   int32
	Fsi_pid    Tpid_t
	Fsi_uid    Tuid_t
	Fsi_status int32
	Fsi_addr   uintptr
	Fsi_value  Tsigval
	Fsi_band   int64
	F__pad     [7]uint64
}

type t__siginfo = Tsiginfo_t

type t__sigaction_u = struct {
	F__sa_sigaction [0]uintptr
	F__sa_handler   uintptr
}

type t__sigaction = struct {
	F__sigaction_u t__sigaction_u
	Fsa_tramp      uintptr
	Fsa_mask       Tsigset_t
	Fsa_flags      int32
}

type Tsigaction = struct {
	F__sigaction_u t__sigaction_u
	Fsa_mask       Tsigset_t
	Fsa_flags      int32
}

type Tsig_t = uintptr

type Tsigvec = struct {
	Fsv_handler uintptr
	Fsv_mask    int32
	Fsv_flags   int32
}

type Tsigstack = struct {
	Fss_sp      uintptr
	Fss_onstack int32
}

type Ttimeval = struct {
	Ftv_sec  t__darwin_time_t
	Ftv_usec t__darwin_suseconds_t
}

type Trlim_t = uint64

type Trusage = struct {
	Fru_utime    Ttimeval
	Fru_stime    Ttimeval
	Fru_maxrss   int64
	Fru_ixrss    int64
	Fru_idrss    int64
	Fru_isrss    int64
	Fru_minflt   int64
	Fru_majflt   int64
	Fru_nswap    int64
	Fru_inblock  int64
	Fru_oublock  int64
	Fru_msgsnd   int64
	Fru_msgrcv   int64
	Fru_nsignals int64
	Fru_nvcsw    int64
	Fru_nivcsw   int64
}

type Trusage_info_t = uintptr

type Trusage_info_v0 = struct {
	Fri_uuid               [16]Tuint8_t
	Fri_user_time          Tuint64_t
	Fri_system_time        Tuint64_t
	Fri_pkg_idle_wkups     Tuint64_t
	Fri_interrupt_wkups    Tuint64_t
	Fri_pageins            Tuint64_t
	Fri_wired_size         Tuint64_t
	Fri_resident_size      Tuint64_t
	Fri_phys_footprint     Tuint64_t
	Fri_proc_start_abstime Tuint64_t
	Fri_proc_exit_abstime  Tuint64_t
}

type Trusage_info_v1 = struct {
	Fri_uuid                  [16]Tuint8_t
	Fri_user_time             Tuint64_t
	Fri_system_time           Tuint64_t
	Fri_pkg_idle_wkups        Tuint64_t
	Fri_interrupt_wkups       Tuint64_t
	Fri_pageins               Tuint64_t
	Fri_wired_size            Tuint64_t
	Fri_resident_size         Tuint64_t
	Fri_phys_footprint        Tuint64_t
	Fri_proc_start_abstime    Tuint64_t
	Fri_proc_exit_abstime     Tuint64_t
	Fri_child_user_time       Tuint64_t
	Fri_child_system_time     Tuint64_t
	Fri_child_pkg_idle_wkups  Tuint64_t
	Fri_child_interrupt_wkups Tuint64_t
	Fri_child_pageins         Tuint64_t
	Fri_child_elapsed_abstime Tuint64_t
}

type Trusage_info_v2 = struct {
	Fri_uuid                  [16]Tuint8_t
	Fri_user_time             Tuint64_t
	Fri_system_time           Tuint64_t
	Fri_pkg_idle_wkups        Tuint64_t
	Fri_interrupt_wkups       Tuint64_t
	Fri_pageins               Tuint64_t
	Fri_wired_size            Tuint64_t
	Fri_resident_size         Tuint64_t
	Fri_phys_footprint        Tuint64_t
	Fri_proc_start_abstime    Tuint64_t
	Fri_proc_exit_abstime     Tuint64_t
	Fri_child_user_time       Tuint64_t
	Fri_child_system_time     Tuint64_t
	Fri_child_pkg_idle_wkups  Tuint64_t
	Fri_child_interrupt_wkups Tuint64_t
	Fri_child_pageins         Tuint64_t
	Fri_child_elapsed_abstime Tuint64_t
	Fri_diskio_bytesread      Tuint64_t
	Fri_diskio_byteswritten   Tuint64_t
}

type Trusage_info_v3 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
}

type Trusage_info_v4 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
}

type Trusage_info_v5 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
}

type Trusage_info_v6 = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
	Fri_user_ptime                    Tuint64_t
	Fri_system_ptime                  Tuint64_t
	Fri_pinstructions                 Tuint64_t
	Fri_pcycles                       Tuint64_t
	Fri_energy_nj                     Tuint64_t
	Fri_penergy_nj                    Tuint64_t
	Fri_secure_time_in_system         Tuint64_t
	Fri_secure_ptime_in_system        Tuint64_t
	Fri_neural_footprint              Tuint64_t
	Fri_lifetime_max_neural_footprint Tuint64_t
	Fri_interval_max_neural_footprint Tuint64_t
	Fri_reserved                      [9]Tuint64_t
}

type Trusage_info_current = struct {
	Fri_uuid                          [16]Tuint8_t
	Fri_user_time                     Tuint64_t
	Fri_system_time                   Tuint64_t
	Fri_pkg_idle_wkups                Tuint64_t
	Fri_interrupt_wkups               Tuint64_t
	Fri_pageins                       Tuint64_t
	Fri_wired_size                    Tuint64_t
	Fri_resident_size                 Tuint64_t
	Fri_phys_footprint                Tuint64_t
	Fri_proc_start_abstime            Tuint64_t
	Fri_proc_exit_abstime             Tuint64_t
	Fri_child_user_time               Tuint64_t
	Fri_child_system_time             Tuint64_t
	Fri_child_pkg_idle_wkups          Tuint64_t
	Fri_child_interrupt_wkups         Tuint64_t
	Fri_child_pageins                 Tuint64_t
	Fri_child_elapsed_abstime         Tuint64_t
	Fri_diskio_bytesread              Tuint64_t
	Fri_diskio_byteswritten           Tuint64_t
	Fri_cpu_time_qos_default          Tuint64_t
	Fri_cpu_time_qos_maintenance      Tuint64_t
	Fri_cpu_time_qos_background       Tuint64_t
	Fri_cpu_time_qos_utility          Tuint64_t
	Fri_cpu_time_qos_legacy           Tuint64_t
	Fri_cpu_time_qos_user_initiated   Tuint64_t
	Fri_cpu_time_qos_user_interactive Tuint64_t
	Fri_billed_system_time            Tuint64_t
	Fri_serviced_system_time          Tuint64_t
	Fri_logical_writes                Tuint64_t
	Fri_lifetime_max_phys_footprint   Tuint64_t
	Fri_instructions                  Tuint64_t
	Fri_cycles                        Tuint64_t
	Fri_billed_energy                 Tuint64_t
	Fri_serviced_energy               Tuint64_t
	Fri_interval_max_phys_footprint   Tuint64_t
	Fri_runnable_time                 Tuint64_t
	Fri_flags                         Tuint64_t
	Fri_user_ptime                    Tuint64_t
	Fri_system_ptime                  Tuint64_t
	Fri_pinstructions                 Tuint64_t
	Fri_pcycles                       Tuint64_t
	Fri_energy_nj                     Tuint64_t
	Fri_penergy_nj                    Tuint64_t
	Fri_secure_time_in_system         Tuint64_t
	Fri_secure_ptime_in_system        Tuint64_t
	Fri_neural_footprint              Tuint64_t
	Fri_lifetime_max_neural_footprint Tuint64_t
	Fri_interval_max_neural_footprint Tuint64_t
	Fri_reserved                      [9]Tuint64_t
}

type Trlimit = struct {
	Frlim_cur Trlim_t
	Frlim_max Trlim_t
}

type Tproc_rlimit_control_wakeupmon = struct {
	Fwm_flags Tuint32_t
	Fwm_rate  Tint32_t
}

type Twait = struct {
	Fw_T [0]struct {
		F__ccgo0 uint32
	}
	Fw_S [0]struct {
		F__ccgo0 uint32
	}
	Fw_status int32
}

type Tct_rune_t = int32

type Trune_t = int32

type Twchar_t = int32

type Tdiv_t = struct {
	Fquot int32
	Frem  int32
}

type Tldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tlldiv_t = struct {
	Fquot int64
	Frem  int64
}

type Tmalloc_type_id_t = uint64

type Tva_list = uintptr

type Tfpos_t = int64

type t__sbuf = struct {
	F_base uintptr
	F_size int32
}

type TFILE = struct {
	F_p       uintptr
	F_r       int32
	F_w       int32
	F_flags   int16
	F_file    int16
	F_bf      t__sbuf
	F_lbfsize int32
	F_cookie  uintptr
	F_close   uintptr
	F_read    uintptr
	F_seek    uintptr
	F_write   uintptr
	F_ub      t__sbuf
	F_extra   uintptr
	F_ur      int32
	F_ubuf    [3]uint8
	F_nbuf    [1]uint8
	F_lb      t__sbuf
	F_blksize int32
	F_offset  Tfpos_t
}

type t__sFILE = TFILE

type Taccessx_descriptor = struct {
	Fad_name_offset uint32
	Fad_flags       int32
	Fad_pad         [2]int32
}

type Tuuid_t = [16]uint8

// C documentation
//
//	/* ARGSUSED */
func XMD2End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, uint64(m_MD2_DIGEST_STRING_LENGTH))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XMD2Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD2_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(16), ^t__predefined_size_t(0))
	return buf
}

var _hex = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XMD2FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1248)
	defer tls.Free(1248)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TMD2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XMD2Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XMD2Update(tls, bp+1168, bp+144, uint32(libc.Uint64FromInt64(nr)))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XMD2End(tls, bp+1168, buf)
	}
	return v5
}

func XMD2File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XMD2FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XMD2Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(80)
	defer tls.Free(80)
	var _ /* ctx at bp+0 */ TMD2_CTX
	XMD2Init(tls, bp)
	XMD2Update(tls, bp, data, uint32(len1))
	return XMD2End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XMD4End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_MD4_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XMD4Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD4_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex1[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex1[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(16), ^t__predefined_size_t(0))
	return buf
}

var _hex1 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XMD4FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1264)
	defer tls.Free(1264)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TMD4_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XMD4Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XMD4Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XMD4End(tls, bp+1168, buf)
	}
	return v5
}

func XMD4File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XMD4FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XMD4Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TMD4_CTX
	XMD4Init(tls, bp)
	XMD4Update(tls, bp, data, len1)
	return XMD4End(tls, bp, buf)
}

/* Avoid polluting the namespace. Even though this makes this usage
 * implementation-specific, defining it unconditionally should not be
 * a problem, and better than possibly breaking unexpecting code. */

// C documentation
//
//	/* ARGSUSED */
func XMD5End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [16]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_MD5_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XMD5Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_MD5_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex2[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex2[libc.Int32FromUint8((*(*[16]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(16), ^t__predefined_size_t(0))
	return buf
}

var _hex2 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XMD5FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1264)
	defer tls.Free(1264)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TMD5_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XMD5Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XMD5Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XMD5End(tls, bp+1168, buf)
	}
	return v5
}

func XMD5File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XMD5FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XMD5Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TMD5_CTX
	XMD5Init(tls, bp)
	XMD5Update(tls, bp, data, len1)
	return XMD5End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XRMD160End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [20]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_RMD160_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XRMD160Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_RMD160_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex3[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex3[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(20), ^t__predefined_size_t(0))
	return buf
}

var _hex3 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XRMD160FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1264)
	defer tls.Free(1264)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TRMD160_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XRMD160Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XRMD160Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XRMD160End(tls, bp+1168, buf)
	}
	return v5
}

func XRMD160File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XRMD160FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XRMD160Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TRMD160_CTX
	XRMD160Init(tls, bp)
	XRMD160Update(tls, bp, data, len1)
	return XRMD160End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA1End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [20]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA1_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA1Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA1_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex4[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex4[libc.Int32FromUint8((*(*[20]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(20), ^t__predefined_size_t(0))
	return buf
}

var _hex4 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA1FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1264)
	defer tls.Free(1264)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA1_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA1Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA1Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA1End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA1File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA1FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA1Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(96)
	defer tls.Free(96)
	var _ /* ctx at bp+0 */ TSHA1_CTX
	XSHA1Init(tls, bp)
	XSHA1Update(tls, bp, data, len1)
	return XSHA1End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA224End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [28]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA224_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA224Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA224_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex5[libc.Int32FromUint8((*(*[28]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex5[libc.Int32FromUint8((*(*[28]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(28), ^t__predefined_size_t(0))
	return buf
}

var _hex5 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA224FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1376)
	defer tls.Free(1376)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA224Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA256Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA224End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA224File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA224FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA224Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	XSHA224Init(tls, bp)
	XSHA256Update(tls, bp, data, len1)
	return XSHA224End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA256End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [32]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA256_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA256Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA256_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex6[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex6[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(32), ^t__predefined_size_t(0))
	return buf
}

var _hex6 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA256FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1376)
	defer tls.Free(1376)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA256Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA256Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA256End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA256File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA256FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA256Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	XSHA256Init(tls, bp)
	XSHA256Update(tls, bp, data, len1)
	return XSHA256End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA384End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(48)
	defer tls.Free(48)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [48]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA384_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA384Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA384_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex7[libc.Int32FromUint8((*(*[48]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex7[libc.Int32FromUint8((*(*[48]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(48), ^t__predefined_size_t(0))
	return buf
}

var _hex7 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA384FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1376)
	defer tls.Free(1376)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA384Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA512Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA384End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA384File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA384FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA384Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	XSHA384Init(tls, bp)
	XSHA512Update(tls, bp, data, len1)
	return XSHA384End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA512End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [64]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA512Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA512_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex8[libc.Int32FromUint8((*(*[64]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex8[libc.Int32FromUint8((*(*[64]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(64), ^t__predefined_size_t(0))
	return buf
}

var _hex8 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA512FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1376)
	defer tls.Free(1376)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA512Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA512Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA512End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA512File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA512FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA512Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	XSHA512Init(tls, bp)
	XSHA512Update(tls, bp, data, len1)
	return XSHA512End(tls, bp, buf)
}

// C documentation
//
//	/* ARGSUSED */
func XSHA512_256End(tls *libc.TLS, ctx uintptr, buf uintptr) (r uintptr) {
	bp := tls.Alloc(32)
	defer tls.Free(32)
	var i int32
	var v1 uintptr
	var v2 bool
	var _ /* digest at bp+0 */ [32]Tuint8_t
	_, _, _ = i, v1, v2
	if v2 = buf == libc.UintptrFromInt32(0); v2 {
		v1 = libc.Xmalloc(tls, libc.Uint64FromInt32(libc.Int32FromInt32(m_SHA512_256_DIGEST_LENGTH)*libc.Int32FromInt32(2)+libc.Int32FromInt32(1)))
		buf = v1
	}
	if v2 && v1 == libc.UintptrFromInt32(0) {
		return libc.UintptrFromInt32(0)
	}
	XSHA512_256Final(tls, bp, ctx)
	i = 0
	for {
		if !(i < int32(m_SHA512_256_DIGEST_LENGTH)) {
			break
		}
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = _hex9[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])>>int32(4)]
		*(*int8)(unsafe.Pointer(buf + uintptr(i+i+int32(1)))) = _hex9[libc.Int32FromUint8((*(*[32]Tuint8_t)(unsafe.Pointer(bp)))[i])&int32(0x0f)]
		goto _3
	_3:
		;
		i++
	}
	*(*int8)(unsafe.Pointer(buf + uintptr(i+i))) = int8('\000')
	libc.X__builtin___memset_chk(tls, bp, 0, uint64(32), ^t__predefined_size_t(0))
	return buf
}

var _hex9 = [17]int8{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'}

func XSHA512_256FileChunk(tls *libc.TLS, filename uintptr, buf uintptr, off Toff_t, len1 Toff_t) (r uintptr) {
	bp := tls.Alloc(1376)
	defer tls.Free(1376)
	var fd, save_errno, v1 int32
	var nr, v2 Tssize_t
	var v3 int64
	var v4 bool
	var v5 uintptr
	var _ /* buffer at bp+144 */ [1024]Tuint8_t
	var _ /* ctx at bp+1168 */ TSHA2_CTX
	var _ /* sb at bp+0 */ Tstat
	_, _, _, _, _, _, _, _ = fd, nr, save_errno, v1, v2, v3, v4, v5
	XSHA512_256Init(tls, bp+1168)
	v1 = libc.Xopen(tls, filename, m_O_RDONLY, 0)
	fd = v1
	if v1 < 0 {
		return libc.UintptrFromInt32(0)
	}
	if len1 == 0 {
		if libc.Xfstat(tls, fd, bp) == -int32(1) {
			libc.Xclose(tls, fd)
			return libc.UintptrFromInt32(0)
		}
		len1 = (*(*Tstat)(unsafe.Pointer(bp))).Fst_size
	}
	if off > 0 && libc.Xlseek(tls, fd, off, m_SEEK_SET) < 0 {
		libc.Xclose(tls, fd)
		return libc.UintptrFromInt32(0)
	}
	for {
		if libc.Int64FromInt64(1024) < len1 {
			v3 = libc.Int64FromInt64(1024)
		} else {
			v3 = len1
		}
		v2 = libc.Xread(tls, fd, bp+144, libc.Uint64FromInt64(v3))
		nr = v2
		if !(v2 > 0) {
			break
		}
		XSHA512Update(tls, bp+1168, bp+144, libc.Uint64FromInt64(nr))
		if v4 = len1 > 0; v4 {
			len1 -= nr
		}
		if v4 && len1 == 0 {
			break
		}
	}
	save_errno = *(*int32)(unsafe.Pointer(libc.X__error(tls)))
	libc.Xclose(tls, fd)
	*(*int32)(unsafe.Pointer(libc.X__error(tls))) = save_errno
	if nr < 0 {
		v5 = libc.UintptrFromInt32(0)
	} else {
		v5 = XSHA512_256End(tls, bp+1168, buf)
	}
	return v5
}

func XSHA512_256File(tls *libc.TLS, filename uintptr, buf uintptr) (r uintptr) {
	return XSHA512_256FileChunk(tls, filename, buf, libc.Int64FromInt32(0), libc.Int64FromInt32(0))
}

func XSHA512_256Data(tls *libc.TLS, data uintptr, len1 Tsize_t, buf uintptr) (r uintptr) {
	bp := tls.Alloc(208)
	defer tls.Free(208)
	var _ /* ctx at bp+0 */ TSHA2_CTX
	XSHA512_256Init(tls, bp)
	XSHA512Update(tls, bp, data, len1)
	return XSHA512_256End(tls, bp, buf)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "\x00\x01\x00\x02\x02\x00\x03\x03\x03\x00\x04\x04\x04\x04\x00\x05\x05\x05\x05\x05\x00\x06\x06\x06\x06\x06\x06\x00\a\a\a\a\a\a\a\x00\b\b\b\b\b\b\b\b\x00\t\t\t\t\t\t\t\t\t\x00\n\n\n\n\n\n\n\n\n\n\x00\v\v\v\v\v\v\v\v\v\v\v\x00\f\f\f\f\f\f\f\f\f\f\f\f\x00\r\r\r\r\r\r\r\r\r\r\r\r\r\x00\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x0e\x00\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x0f\x00\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x10\x00\x80\x00\x00\x00"
