// Copyright 2023 The libmd-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libmd is a ccgo/v4 version of libmd.a, the BSD Message Digest
// library.
package libmd // import "modernc.org/libmd"
